<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <h2>Upcoming Events</h2>

      <table class="item-list" summary="Sortable Table (Click a column header to sort)">
        <thead>
          <tr>
            <th><a class="sortable desc" href="#">Date <abbr style="border-style: none;" class="sort-icon" title="(sorted descending)"><span class="caret"></span></abbr></a></th>
            <th><a class="sortable" href="#">Event / Location <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
            <th><a class="sortable" href="#">Speakers <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <td width="12%">
              <strong>May 5<br>16:30 ET</strong>
            </td>
            <td width="53%">
              GenMark Diagnostics First Quarter Earnings Conference Call
            </td>
            <td>
              TBC
            </td>
            <td>
              <a style="margin-bottom: 5px;" class="btn btn-default fa-icon fa-icon--headphones" href="#">Listen</a><br>
              <a class="btn btn-default fa-icon fa-icon--clock" href="#">Remind Me</a>
            </td>
          </tr>

          <tr>
            <td>
              <strong>May 13<br>15:00 PT</strong>
            </td>
            <td>
              Bank of America Merrill Lynch 2015 Health Care Conference <br>
              <span class="item-list__location fa-icon fa-icon--map-marker">Encore Hotel in Las Vegas, NV</span>
            </td>
            <td>
              Hany Massarany, President and Chief Executive Officer
            </td>
            <td>
              <a style="margin-bottom: 5px;" class="btn btn-default fa-icon fa-icon--headphones" href="#">Listen</a><br>
              <a class="btn btn-default fa-icon fa-icon--clock" href="#">Remind Me</a>
            </td>
          </tr>

        </tbody>
      </table>

      <h2>Archived Events</h2>

      <div class="row">

        <div class="col-xs-12">
          <form action="" class="form-inline">
            <div class="form-group">
              <label for="id2">Year: </label>
              <select class="form-control">
                <option value="">All Years</option>
              </select>
            </div>
            <div class="form-group">
              <input class="btn btn-default reversed" type="submit" value="Search">&nbsp;
              <a href="#" class="btn btn-default">Reset</a>
            </div>
          </form>
        </div>

      </div>

      <br>

      <table class="item-list" summary="Sortable Table (Click a column header to sort)">
        <thead>
          <tr>
            <th><a class="sortable desc" href="#">Date <abbr style="border-style: none;" class="sort-icon" title="(sorted descending)"><span class="caret"></span></abbr></a></th>
            <th><a class="sortable" href="#">Event / Location <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
            <th><a class="sortable" href="#">Speakers <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <td width="12%">
              <strong>Mar 4<br>09:20 ET</strong>
            </td>
            <td width="53%">
              Cowen and Company's 35th Annual Health Care Conference<br>
              <span class="item-list__location fa-icon fa-icon--map-marker">Boston Marriott Copley Place in Boston, Massachusetts</span>
            </td>
            <td>
              Hany Massarany, President and Chief Executive Officer
            </td>
            <td>
              <!-- <a style="margin-bottom: 5px;" class="btn btn-default fa-icon fa-icon--headphones" href="#">Listen</a><br> -->
            </td>
          </tr>

          <tr>
            <td>
              <strong>Mar 2<br>14:50 ET</strong>
            </td>
            <td>
              Raymond James' 36th Annual Institutional Investors Conference <br>
              <span class="item-list__location fa-icon fa-icon--map-marker">JW Marriott Grande Lakes in Orlando, Florida</span>
            </td>
            <td>
              Hany Massarany, President and Chief Executive Officer
            </td>
            <td>
              <!-- <a style="margin-bottom: 5px;" class="btn btn-default fa-icon fa-icon--headphones" href="#">Listen</a><br> -->
            </td>
          </tr>

          <tr>
            <td>
              <strong>Feb 24<br>16:30 ET</strong>
            </td>
            <td>
              GenMark Diagnostics Fourth Quarter Earnings
            </td>
            <td>
              Hany Massarany, President and Chief Executive Officer
            </td>
            <td>
              <a style="margin-bottom: 5px;" class="btn btn-default fa-icon fa-icon--headphones" href="#">Listen</a><br>
            </td>
          </tr>

          <tr>
            <td>
              <strong>Jan 13<br>09:30 PT</strong>
            </td>
            <td>
              2015 J.P. Morgan Healthcare Conference  <br>
              <span class="item-list__location fa-icon fa-icon--map-marker">Westin St. Francis Hotel in San Francisco, CA</span>
            </td>
            <td>
              Hany Massarany, President and Chief Executive Officer
            </td>
            <td>
              <a style="margin-bottom: 5px;" class="btn btn-default fa-icon fa-icon--headphones" href="#">Listen</a><br>
            </td>
          </tr>

        </tbody>
      </table>

      <ul class="pagination">
        <li class="pagination__item"><a href="#" class="btn btn-default">1</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">2</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">3</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">4</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">></a></li>
      </ul>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>