
<div class="row">
  <div class="col-xs-12 col-sm-6">
    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo get_home_url(); ?>">
      <div class="form-group">
        <label class="screen-reader-text" for="s">Search for:</label>
        <input class="form-control" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>" name="s" id="s" type="text">
      </div>
      <input class="btn btn-default" id="searchsubmit" value="Search" type="submit">
    </form>
  </div>
</div>