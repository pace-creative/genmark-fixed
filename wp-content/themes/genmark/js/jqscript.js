$(document).ready(function() {

  //customer resources page
$('.results-container').children('div').children('div').children('div').hide();
$('.results-heading').click(function() {
  $(this).next('.results').toggle();
  $(this).toggleClass('results-heading--active');
});

  $('.crc-category').first().addClass('active');

    $('.content-panel > .content-panel__row:first-child .crc-cat').addClass('active');
  $('.crc-cat').click(function(e) {
      e.preventDefault();

      if(!$(this).hasClass('active')){
        $(this).parent('.content-panel__row').siblings().each(function(){
          $(this).find('.crc-cat').each(function(){
            $(this).removeClass('active');
          })
        })
        $(this).addClass('active');
        $target = $(this).attr('data-href');
        $(""+$target+"").siblings().each(function(){
          $(this).removeClass('active');
        })
        $(""+$target+"").addClass('active');
      }
    });

  $("video").bind("contextmenu",function(){
    return false;
  });

  if( typeof ga !== 'undefined' ) {

    // track events through google analytics
    $('.crc-cat').click(function(e) {
      e.preventDefault();
      var location = $(this).data('location'),
          name = $(this).data('name');

      ga('send', 'event', 'CRC Category - ' + location, 'click', name);

      if(!$(this).hasClass('active')){
        $(this).siblings().each(function(){
          $(this).removeClass('active');
        })
        $(this).addClass('active');
        $target = $(this).attr('href');
        $(""+$target+"").siblings().each(function(){
          $(this).removeClass('active');
        })
        $(""+$target+"").addClass('active');
      }
    });

    $('.crc-download').click(function() {

      var name = $(this).data('name');

      ga('send', 'event', 'CRC Download', 'click', name);

    });
  }

  $('.fakelink').fakeLink();

  // on window resize stuff
  function window_resize() {
    // $('.normalize-height').normalizeHeight();
    $('.content-widget--panels .content-widget__title, .fixheight').normalizeHeight();
    $('.content-widget--pipeline, .content-widget--panels').normalizeHeight({by_row:true});
  }

  $(window).load(function() {
    window_resize();
  });

  $(window).resize(function() {
    window_resize();
  });

  $('.table--comparison, .table--diagram').wrap('<div class="table-responsive"></div>');

  var lightbox_count = 0;

  // add _blank target on images linked to themselves
  $('a > img').each( function() {

    if( this.src == $(this).parent().attr('href') ) {
      $(this).parent().attr( 'target','_blank' );
    }

  });

  // check if preferred language is set
  pref_lang = readCookie('pref_lang');
  last_lang = readCookie('last_lang');

  if( !pref_lang && !last_lang ) {

      // set up ajax request
    var data = {
      action: 'ajax_geo_lang_match',
    }

    // send request
    jQuery.post(php_vals.ajax_url, data, function(response) {
      if( response == 1 ) {

        // geo lang matches site lang, set "last lang"
        set_last_lang();

      } else {

        // geo lang does not match site lang, launch popup
        $('#wrong-region-modal').modal();

      }
    });

  }

  // some extra actions when main nav is toggled
  $('.main-navigation a[data-toggle=dropdown]').click(function(e) {

    $megamenu = $('.megamenu');
    $dropdown = $(this).siblings('.dropdown-menu');

    // $dropdowns.hide();

    if( ! $(this).parent().hasClass('open') ) {

      // show mega menu background when main nav is toggled
      $megamenu.show();

      // set size of mega menu
      mmbackground();

      // find right edge of mega menu and dropdown
      mm_right = right_edge($megamenu);

      // toggle dropdown without repaint to get offset before it's "shown"
      $(this).parent().addClass('open');
      dd_right = right_edge($dropdown);
      $(this).parent().removeClass('open');

      // check if edge is over, and we're not mobile
      if( $('.is_sm:visible').length == 0 && dd_right > mm_right ) {

        // find difference
        offset = dd_right - mm_right;

        $dropdown.css({
          left: '-='+offset
        })
      }

    } else {

      $megamenu.hide();

    }

  });

  // hide mega menu or crc popover background when clicked off of
  $(document).on('click', hideMenu);

  function hideMenu() {
    $('.megamenu').hide();
    $('#crc-popover').hide();
  }

  // prevent megamenu from hiding when clicked
  $('.megamenu').click( function(e) {
    e.stopPropagation();
  });


  // timeout IDs for adding mouseout menu closing
  var megatimeoutID = 0;
  var crctimeoutID = 0;

  // clear timeouts if we hover back onto menu
  $('.megamenu').on('mouseover', function(e) {

    clearTimeout(megatimeoutID);

  });
  $('#crc-popover').on('mouseover', function(e) {

    clearTimeout(crctimeoutID);

  });

  // hide megamenu on mouseout
  $('.megamenu, .main-navigation > .main-navigation__item').on('mouseout', function(e) {

    if( $(e.relatedTarget).parents('.main-navigation').length > 0 ) {
      return;
    }

    megatimeoutID = setTimeout( function() {
      $('.megamenu').hide();
      $('.main-navigation .open').removeClass('open');
    }, 200 );

  });

  // hide CRC popover on mouseout
  $('#crc-popover').on('mouseout', function(e) {

    crctimeoutID = setTimeout( function() {
      $(this).hide();
    }.bind(this), 200 );

  });



  // toggle button for mobile nav
  $('.menu-toggle').click(function(e) {
    e.preventDefault();

    $('.main-navigation').slideToggle();
  });

  // first subnav level doesn't link, toggles instead
  $('.subnav.depth0 > .has_children > a').click( function(e) {

    e.preventDefault();
    e.stopPropagation();

    $(this).siblings('.subnav__collapse-toggle')[0].click();

  });

  // handle subnav toggling
  $('.subnav__collapse-toggle').click(function(e) {

    $(this).siblings( '.subnav' ).slideToggle().promise().done(function() {
      $(this).toggleClass('active');
    });

    $(this).toggleClass('active');

  });

  // apply hover intent to mega-menu hovers
  $('.main-navigation__item > .dropdown-menu').each(function() {

    var nav = $(this);

    nav
      .mouseover(function(e){
        nav.doTimeout( 'main-nav', 200, over, e.target );
      }).mouseout(function(e){
        nav.doTimeout( 'main-nav', 200, out, e.target );
      });

    function over( elem ) {

      var parent = $(elem).parents( 'li.dropdown' );
      var parents = $('li.dropdown');

      if( $('.hover_click').length == 0 ) {
        parents.removeClass( 'hover' );
        parent.addClass( 'hover' );
      }

      mmbackground();

    };

    function out( elem ) {

      var parents = nav.children();

      if( $('.hover_click').length == 0 ) {
        parents.removeClass( 'hover' );
      }

      mmbackground();

    };

  });

  // add hover class to dropdowns
  $('.dropdown-menu a').hover(function(e) {
    if( $('.hover_click').length == 0 ) {
      $(this).toggleClass('hover');
    }
  });

  // handle responsive menu top-level toggling
  $('.main-navigation > li.dropdown > a').click(function(e) {

    $('.main-navigation .hover').removeClass('hover');
    $('.hover_click').removeClass('hover_click');

    if( $('.is_sm:visible').length > 0 ) {
      e.stopPropagation();
      e.preventDefault();

      $(this).siblings( '.dropdown-menu' ).slideToggle().promise().done(function() {
        $(this).toggleClass('active');
      });

      $(this).toggleClass('active');
    }

  });

  // 2nd level responsive menu parent items don't link, click toggle instead
  $('.main-navigation__item > .dropdown-menu > li.dropdown > a').click( function(e) {
    e.preventDefault();
    e.stopPropagation();

    // remove hover class from nav items
    $('.main-navigation .hover').removeClass('hover');

    // clear "sticky" hover
    $('.hover_click').removeClass('hover_click');

    // add sticky hover to clicked item
    $(this).toggleClass('hover hover_click');

    // add hover class to parent
    $(this).parent().toggleClass('hover');

    if( $('.is_sm:visible').length > 0 ) {
      $(this).find('.fa-angle-down')[0].click();
    }

  });

  $('.crc-popover-toggle').click(function(e) {

    e.preventDefault();
    e.stopPropagation();

    // get popover element
    $popover = $('#crc-popover');

    // find necessary measurements
    position = $(this).position();
    height = $(this).outerHeight(true);
    width = $(this).outerWidth(true);
    pwidth = $popover.outerWidth(true);

    // compute popover position
    t = position.top + height + 5;
    l = position.left - (pwidth/2) + (width/2);

    // show popover and position it
    $popover.toggle().css({
      top: t,
      left: l
    });

    // make sure we aren't overshooting the edge, plus padding
    rightedge = $popover.offset().left +  pwidth + 10;
    overshoot = rightedge - $(window).width();
    if( overshoot > 0 ) {
      $popover.css( 'left', l - overshoot )
    }

  });


  $('#crc-popover').children().click(function(e) {
    e.stopPropagation();
  });

  // handle responsive menu toggling
  $('.main-navigation__item > .dropdown-menu > li.dropdown > a > .fa-angle-down').click( function(e) {

    if( $('.is_sm:visible').length > 0 ) {
      e.stopPropagation();
      e.preventDefault();

      $(this).parent().siblings( '.dropdown-menu' ).slideToggle().promise().done(function() {
        $(this).toggleClass('active');
      });

      $(this).toggleClass('active');
    }

  });
  $('.main-navigation__item > .dropdown-menu > li.dropdown > .dropdown-menu > li.dropdown > a').click( function(e) {
    window.location = $(this).attr('href');
  });


  // handle language switcher
  $('.lang-switcher').click(function(e) {

    e.preventDefault();

    // get current, and target lang
    cur_lang = readCookie('_icl_current_language');
    target_lang = $(this).data('lang');

    // if current lang is same as target lang, don't do anything
    if( cur_lang == target_lang ) {
      return;

    // else launch switcher popup
    } else {

      $('#language-confirm-modal').modal();
      $('.lang-switch-trigger')
        .attr('href', $(this).attr('href') )
        .data('lang', target_lang);
      return;

    }

    // fall back to link if we don't do anything else
    window.location = this.href;

  });

  $('.lang-switch-trigger').click(function(e) {

    e.preventDefault();

    lang = $(this).data('lang');

    set_pref_lang( lang );

    window.location = this.href;

  });

  $('.last-lang-trigger').click(function() {
    set_last_lang();
  });

  $('#alertbar-close, #alertbar-hide').click(function(e){
    e.preventDefault();
    clear_alert_param();
    $('.alertbar').slideUp();
  });

  $('#alertbar-close-icon, #alertbar-hide').click(function(e){
    e.preventDefault();
    clear_alert_param();
    $('.alertbar').slideUp();
  });

  function clear_alert_param() {
    // try to remove parameter from URL bar
    if (history.state !== undefined) {

      // build URL
      url = window.location.href;
      url = url.replace( /[\?&]alert=[^&]*/, '' );
      window.history.pushState({}, "", url);

    }
  }

  function set_pref_lang( lang ) {
    createCookie('pref_lang', lang, php_vals.DEFAULT_PREF_LANG_TIMEOUT);
  }
  function set_last_lang() {
    jQuery.post(php_vals.ajax_url, {action: 'set_last_lang'});
  }

  // check for visible dropdowns in the main nav, and adjust megamenu background height
  function mmbackground() {
    var intervalID = setTimeout( function() {

    ddheight = tallest('.main-navigation .dropdown-menu:visible');

    if( ddheight > 0 ) {
      ddheight += 30;

      $('.megamenu')
        .show()
        .css('height',ddheight+'px');
    } else {
      $('.megamenu').hide();
    }

    }, 1 );
  }

  // find the tallest of a set of elements
  function tallest(elements) {

    var return_height = 0;

    $(elements).each(function() {

      var height = $(this).outerHeight();

      if( height > return_height ) {
        return_height = height;
      }

    });

    return return_height;

  }

  /** cookie handling functions **/
  function createCookie(name, value, days) {
    if(days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
  }

  function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while(c.charAt(0) == ' ') c = c.substring(1, c.length);
      if(c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }

  function eraseCookie(name) {
    createCookie(name, "", -1);
  }


  $( ".btn-launch-video" ).click(function() {
    var post_id = this.id;
    if($(this).hasClass('webinar'))
      launchModalVideo( post_id, 'webinar-video' );
    if($(this).hasClass('widget'))
      launchModalVideo( post_id, 'widget-video' );
    return false;
  });

  $('.launch-system-video').click(function(e) {

    e.preventDefault();

    var $this = $(this);
    var system_id = $(this).data('system-id');

    if( system_id ) {
      launchModalVideo(system_id, 'system-video');
    }

  });

  $('.gallery-thumbs__thumb').click(function() {

    show_image(this);
    clearInterval(galleryIntervalID);

  });

  $('.gallery-thumbs__thumb').hover(function() {

    show_image(this);
    rotating = false;

  }, function() {
    rotating = true;
  });

  function show_image(obj) {

    var $this = $(obj);
    var item = $this.data('item');

    var $images = $('.gallery-img');
    var $img_target = $('#gallery-img-' + item);

    var $text = $('.gallery-text');
    var $text_target = $('#gallery-text-' + item);

    var $thumbs = $('.gallery-thumbs__thumb');

    $thumbs.removeClass('active');
    $this.addClass('active');

    $images.removeClass('active');
    $img_target.addClass('active');

    $text.removeClass('active');
    $text_target.addClass('active');

  }

  if( $('.gallery-thumbs__thumb').length ) {

    var rotating = true;

    var galleryIntervalID = setInterval(function() {

      if( !rotating ) {
        return false;
      }

      var $active = $('.gallery-thumbs__thumb.active');
      var $thumbs = $('.gallery-thumbs__thumb');

      if( !$active.length ) {

        $active = $thumbs.first();

      }

      $next = $active.parent().next().find('.gallery-thumbs__thumb');

      if( !$next.length ) {

        $next = $thumbs.first();

      }

      show_image($next);

    }, 5000);

  }

  $('.crc a').click(function(e) {

    var href = $(this).attr('href');
    if( href.charAt(0) == '#' ){
      e.preventDefault();
      if( href == '#')
        href = '#header';
      // console.log(href);
      $('html, body').animate({ scrollTop: $( href ).offset().top }, 200);
    }
  });

  var wpcf7Elm = document.querySelector( '.wpcf7' );

  if( wpcf7Elm && !$(wpcf7Elm).hasClass('haspopup') ) {

    wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {

      switch( event.detail.contactFormId ) {
        case '4259':
          $('#lab-auto-pdf-popup').modal('show');
          break;
        default:
          $('#reqInfoSent').modal("show");
          break;
      }

    }, false );
  }

  $('.tab-list__item').click(function(e) {
    var $this = $(this);
    var $siblings = $this.siblings();

    var target = $this.data('target');
    var $target = $('#tab-list-content-' + target);
    var $allContent = $target.siblings();

    $siblings.add($allContent).removeClass('active');
    $this.add($target).addClass('active');
  });

  $('.tab-accordion-title').click(function() {

    var $this = $(this);

    var target = $this.data('target');
    var $target = $('#tab-list-content-' + target);

    $this.toggleClass('active');
    $target.slideToggle();

  });

});

function launchModalVideo(post_id, field_name){
  ajaxurl = window.ajaxurl;
  $.ajax({
    url: ajaxurl,
    data: {
      'action':'update_modal_video',
      'post_id' : post_id,
      'field_name' : field_name
    },
    success:function(data) {
      //console.log(data);

      var video = JSON.parse(data);
      //console.log(video);

      var video_modal =
      '<div class="modal-dialog modal-video" role="document">'+
        '<div class="modal-content">'+
          '<div class="modal-header clearfix">'+
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="pauseModalVideo(\''+video.id+'\')"><span aria-hidden="true">Close x</span></button>'+
          '</div>'+
          '<div class="modal-body clearfix">'+
            '<video id="video_'+video.id+'" class="video-js vjs-default-skin vjs-big-play-centered"'+
              'controls preload="auto" width="100%" height="450"'+
              'poster="'+video.poster+'"'+
              'data-setup="{}">'+
              '<source src="'+video.url+'" type="video/mp4" />'+
              '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>'+
            '</video>'+
          '</div>'+
        '</div>'+
      '</div>';

      $( '#video-modal-ajax' ).html( video_modal );
      $( '#video-modal-ajax' ).modal("show");

    },
    error: function(errorThrown){  console.log(errorThrown); }
  });
}

function pauseModalVideo( video_id ){

  if( $( '#video_'+video_id ).is('video') ){
    if( !$( '#video_'+video_id )[0].paused ) {
      $( '#video_'+video_id )[0].pause();
    }
  }
  if( $( '#'+video_id ).is('div') ){
    if( $( '#'+video_id+' video' ) && !$( '#'+video_id+' video' )[0].paused ) {
      $( '#'+video_id+' video' )[0].pause();
    }
  }
}


// find right edge of element
function right_edge( elem ) {

  $elem = $(elem);

  // find attributes
  pos = $elem.offset();
  left = pos.left;
  width = $elem.width();

  // calculate
  right = left+width;

  return right;

}

// Called by the Requestion Information form after the message is sent
function req_info_ok(){
  // $('#reqInfoSent').modal("show");
}
