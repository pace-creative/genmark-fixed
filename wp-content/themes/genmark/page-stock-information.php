<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <div class="row stock-info">
        <div class="col-xs-12">
          <h2 class="stock-info__title">GenMark Diagnostics (NASDAQ: GNMK)
          <br><small class="stock-info__date">2:55 PM ET on May 5, 2015</small></h2>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-4">

          <div class="stock-info__value">$9.78</div>
          <div class="stock-info__change up">
            <span>+$0.41</span>&nbsp;&nbsp;
            <span>+4.024%</span>
          </div>

        </div>
        <div class="col-xs-12 col-sm-4">

          <table class="table table-striped table--stock-info">
            <tr>
              <th>Previous Close</th>
              <td>10.19</td>
            </tr>
            <tr>
              <th>Open</th>
              <td>10.12</td>
            </tr>
            <tr>
              <th>Volume</th>
              <td>132,500</td>
            </tr>
            <tr>
              <th>Exchange</th>
              <td>NASDAQ</td>
            </tr>
          </table>

        </div>
        <div class="col-xs-12 col-sm-4">

          <table class="table table-striped table--stock-info">
            <tr>
              <th>Day High</th>
              <td>10.19</td>
            </tr>
            <tr>
              <th>Day Low</th>
              <td>9.75</td>
            </tr>
            <tr>
              <th>52 Week High</th>
              <td>14.40</td>
            </tr>
            <tr>
              <th>52 Week Low</th>
              <td>8.48</td>
            </tr>
          </table>

        </div>
      </div>
<br>
<br>
      <div class="row">

        <div class="col-xs-12">
          <form action="" class="form-inline">
            <div class="form-group">
              <label for="id1">Compare: </label>
              <select class="form-control">
                <option value="">Benchmark</option>
                <option value="">NASDAQ</option>
                <option value="">NYSE</option>
                <option value="">S&amp;P 500</option>
                <option value="">AMEX</option>
              </select>
            </div>
            <div class="form-group">
              <label for="id2">Options: </label>
              <select class="form-control">
                <option value="">Area Graph</option>
                <option value="">OHLC</option>
                <option value="">Line Graph</option>
                <option value="">Candlestick</option>
              </select>
            </div>
            <div class="form-group">
              <label for="id3">Time: </label>
              <select class="form-control">
                <option value="">Option</option>
                <option value="">Option</option>
                <option value="">Option</option>
              </select>
            </div>
            <div class="form-group">
              <input class="btn btn-default reversed" type="submit" value="Update">
            </div>
          </form>
        </div>

      </div>

      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/stockinfo_placeholder.jpg" alt="">

      <p>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-excel.jpg" alt=""> <a href="#">Convert chart to Excel</a> &nbsp;&nbsp;
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-email.jpg" alt=""> <a href="#">Sign up for email alerts</a>
      </p>
      <p>
        <em>The stock information provided is for informational purposes only and is not intended for trading purposes. The stock information is provided by eSignal, stock charts are provided by EDGAR Online, both third party services, and GenMark Dx does not maintain or provide information directly to this service. Stock information is delayed approximately 20 minutes.</em>
      </p>

      <?php echo get_template_part('content','footnote'); ?>

    </div><!-- .col-xs-12 -->
  </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>