<?php

$title = get_the_title();
$date = get_field('date') ? DateTime::createFromFormat('d/m/Y', get_field('date')) : false;
$customer = get_field('customer');
$pdf = get_field('pdf');
?>

<tr>
  <td width="20%">
    <p class="upper-blue"><?php if($date) echo $date->format('M d, Y'); else echo 'n.d.'; ?></p>
  </td>
  <td>
    <p><?php echo $title; ?></p>
    <br>
    <?php if( !empty( $pdf )): ?>
    <p>
      <a class="btn btn-default" target="_blank" href="<?php echo $pdf['url']; ?>"><?php echo strtoupper( __('Read More','genmark') ); ?></a>

    </p>
  <?php endif; ?>
  </td>
  <td>
    <p><?php echo $customer; ?></p>
  </td>
</tr>
