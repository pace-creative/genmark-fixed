<?php
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
  $query_split = explode("=", $string);

  if( !isset( $query_split[1] )) {
    continue;
  }
  $search_query[$query_split[0]] = urldecode($query_split[1]);
} // foreach

if( !isset( $search_query['s'] )) {
  $search_query['s'] = '';
}

$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

$search_query['paged'] = $paged;
$search_query['post_type'] = array(
  'page',
  'Webinars',
  'Publications',
  // 'Conferences',
  'people',
);

$search = new WP_Query($search_query);
relevanssi_do_query($search);

$start_results = (( $paged-1 ) * $search->query_vars['posts_per_page'] ) + 1;
$end_results = $paged * $search->query_vars['posts_per_page'];

if( $end_results > $search->found_posts ) {
  $end_results = $search->found_posts;
}

?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <div class="col-xs-12 col-md-9">

    <h1>Search Results for: "<?php echo $search_query['s']; ?>"</h1>

    <?php get_search_form(); ?>

    <p>
      <?php if( $search->found_posts > 0 ): ?>
        Showing results <?php echo $start_results; ?> - <?php echo $end_results; ?> of <?php echo $search->found_posts; ?>
      <?php else: ?>
        No results found for this search.
      <?php endif;?>
    </p>

    <hr>

    <?php

      if( $search->have_posts() ) {
        while( $search->have_posts() ):
          $search->the_post();

        ?>
          <h2 class="search-result__title"><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h2>

            <ul class="breadcrumbs__list breadcrumbs__list--search">

              <?php

                if( $post->post_parent ){

                    // If child page, get parents
                    $anc = get_post_ancestors( $post->ID );

                    // Get parents in the right order
                    $anc = array_reverse($anc);

                    // Parent page loop
                    $count = 0;
                    foreach ( $anc as $ancestor ) {

                      ?>
                        <li class="breadcrumbs__item">
                          <?php the_alt_title($ancestor); ?>
                        </li>
                      <?php
                    }

                  // only show if there were ancestors
                  if( !empty( $anc )) {
                    echo '<li class="breadcrumbs__item">' . get_the_alt_title() . '</li>';
                  }

                }

              ?>

            </ul>

          <?php the_excerpt(); ?>
          <hr>
        <?php
        endwhile;
      }
      if ( $search->max_num_pages > 1 ) :

        $args = array(
          'format' => '?paged=%#%', //'page/%#%'
          'current' => max( 1, get_query_var('paged') ),
          'prev_next' => false,
          'show_all' => true,
          'total' => $search->max_num_pages,
          'type' => 'array'
        );

        $pages = paginate_links( $args );
        if( is_array( $pages ) ) {
          echo '<ul class="pagination">';
          foreach ( $pages as $page ) {
            echo '<li class="pagination__item">'.str_replace("page-numbers", "btn btn-default", $page).'</li>';
          }
         echo '</ul>';
        }
      endif;
    ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>