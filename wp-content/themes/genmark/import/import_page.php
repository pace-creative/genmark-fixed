<?php


if( !empty( $_FILES )) {

  $csv = $_FILES['csv']['tmp_name'] ? $_FILES['csv']['tmp_name'] : false;

  if( false !== $csv ) {

    $records = csv_to_array($csv);
    // $records = array_slice( $records, 0, 5 );

    $items = array();

    // parse out rows
    foreach( $records as $row ) {
      $items[] = parse_row($row);
    }

    global $wpdb;
    $table_name = $wpdb->prefix . "crc_oldaccounts";

    // loop through to insert posts
    foreach( $items as $item ) {

      $wpdb->insert(
        $table_name,
        array(
          'name' => $item['name'],
          'region' => $item['region'],
          'account' => $item['account'],
          'oldpass' => $item['oldpass'],
          'serials' => serialize($item['serials']),
        )
      );

    }

  }


}


  ?>

  <div class="wrap">
    <h2>Import</h2>
    <h3>Upload CSV</h3>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING']; ?>" enctype="multipart/form-data">

      <label for="csv"><strong>CSV File</strong></label><br />
      <input type="file" name="csv" id="csv" />


      <?php submit_button('Upload'); ?>
    </form>
  </div>
