<?php

require_once 'vendor/autoload.php';

function csv_import_menu() {
  add_management_page( 'Import CSV', 'Import CSV', 'manage_options', 'csv-import', 'csv_import' );
}
add_action( 'admin_menu', 'csv_import_menu' );

function csv_import() {

  include 'import_page.php';

}


// parser function
function csv_to_array($filename='', $delimiter=',') {
    if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
        {
            if(!$header){
              $header = $row;

              // make sure cols with blank headings don't overwrite each other
              foreach($header as $key => $value) {
                if( empty( $value )) {
                  $header[$key] = '_blank'.$key.'_';
                } else {
                  $header[$key] = $value . ' ' . $key;
                }
              }

            } else {
              $data[] = array_combine($header, $row);
            }
        }

        fclose($handle);
    }
    return $data;
}

function parse_row( $row ) {

  $values = array();

  $row = array_values($row);

  $values['name'] = $row[0];
  $values['region'] = $row[1];
  $values['account'] = $row[2];
  $values['oldpass'] = ''; //$row[3];
  $values['serials'] = array_filter( array_slice( $row, 3 ));

  return $values;

}
