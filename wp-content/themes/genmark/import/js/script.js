$ = ((typeof $ == 'undefined') ? jQuery : $);

$(document).ready(function() {


var map;
var mc;
var prevInfo = null;
var markers = [];
var vancouver = new google.maps.LatLng(37.6, -95.665);
var default_zoom = 4;

function initialize() {

  var mapOptions = {
    center: vancouver,
    zoom: default_zoom
  };
  map = new google.maps.Map(document.getElementById('map_container'), mapOptions);

  for (var x = 0; x < init_markers.length; x++) {
    addMarker(init_markers[x]);
  }

  styles = [
    {
      url: php_vals.stylesheet_url+'/phys_cent/img/marker_blank.png',
      height: 22,
      width: 22,
      anchor: [0,0],
      textColor: '#000000',
      textSize: 11,
    }
  ];

  mc_options = {
    maxZoom: 10,
    styles: styles,
    gridSize: 30
  };

  mc = new MarkerClusterer(map, markers, mc_options);

  fitBounds();

}

google.maps.event.addDomListener(window, 'load', initialize);

// Add a marker to the map and push to the array.
function addMarker(marker) {

  var contentString = '';

  for( x in marker.locations ) {
    var contentString = contentString + '<h2 class="h2-list"><a href="' + marker.locations[x].extra.permalink + '">' + marker.locations[x].extra.title + '</a></h2>';
  }


  var infoWindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    position: marker.position,
    map: map,
    title: marker.locations[0].extra.title
  });

  google.maps.event.addListener(marker, 'click', function() {

    if (prevInfo) {
      prevInfo.close();
    }
    infoWindow.open(map, marker);

    prevInfo = infoWindow;
  });

  markers.push(marker);
}

// Add array of markers
function addMarkers(markers) {
  for (var i = 0; i < markers.length; i++) {
    addMarker(markers[i]);
  }
}

// Sets the map on all markers in the array.
function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setAllMap(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setAllMap(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}

// fit map to markers
function fitBounds() {

  if (markers.length == 0) {
    map.panTo(vancouver);
    map.setZoom(default_zoom);
    return;
  }

  //  Create a new viewpoint bound
  var bounds = new google.maps.LatLngBounds();

  //  Go through each...
  for (var i = 0; i < markers.length; i++) {
    //  And increase the bounds to take this point
    bounds.extend(markers[i].position);
  }

  // Don't zoom in too far
  //if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
  var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
  var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01);
  bounds.extend(extendPoint1);
  bounds.extend(extendPoint2);
  //}

  //  Fit these bounds to the map
  map.fitBounds(bounds);
}


});

function parse_marker(json_marker) {
  return {
    position: new google.maps.LatLng(json_marker.pos.lat, json_marker.pos.lng),
    locations: json_marker.locations
  }
}

function condense_makers( markers ) {

  condensed = {};

  for( x in markers ) {

    poskey = markers[x]['position']['lat'] + '_' + markers[x]['position']['lng'];

    if( !( poskey in condensed ) ) {
      condensed[poskey] = {};
      condensed[poskey]['pos'] = {};
      condensed[poskey]['pos']['lat'] = markers[x]['position']['lat'];
      condensed[poskey]['pos']['lng'] = markers[x]['position']['lng'];
      condensed[poskey]['locations'] = [];
    }

    condensed[poskey]['locations'].push(markers[x]);

  }

  return condensed;

}