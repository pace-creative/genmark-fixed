<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <?php
    if ( WP_ENV == 'staging' ) {
      echo '<meta name="robots" content="noindex, nofollow" />';
    }
  ?>

  <title><?php wp_title(''); ?></title>
  <link href='//fonts.googleapis.com/css?family=Roboto:200,400,300,300italic,400italic,500,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <script> window.ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>"; </script>

  <?php wp_head(); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <?php
    if ( WP_ENV == 'production' ) {
      ?>
      <!-- Google Analytics -->
      <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-23971049-1', 'auto');
      ga('send', 'pageview');
      </script>
      <!-- End Google Analytics -->
    <?php
  }
?>

</head>

<body class="<?php echo is_front_page() ? 'front-page' : ''; ?>">
<span class="is_xs visible-xs"></span>
<span class="is_sm visible-xs visible-sm"></span>
<span class="is_md visible-xs visible-sm visible-md"></span>
<span id="top"></span>
<?php if ( !is_page_template('templates/landing-page-bcid-testing.php') ) : ?>

<header id="header" class="noprint header">
  <div class="container-fluid">
    <div class="row toolbar">

      <div class="container">
        <div class="row">

          <div class="col-xs-12 col-sm-4">

            <?php
              $hide_login = get_field( 'hide_footer_login' );
              if( empty( $hide_login ) || 'true' !== array_pop( $hide_login )):
            ?>

            <label>Select Your Region:</label>

            <div class="dropdown dropdown--toolbar" style="display: inline-block;">

              <a class="btn btn-default btn-default--toolbar btn--small dropdown-toggle" id="region" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo ICL_LANGUAGE_NAME; ?>
                <span class="caret"></span>
              </a>

              <?php
                $tl = icl_get_languages('skip_missing=1&orderby=name&order=desc');
              ?>
              <ul class="dropdown-menu dropdown-menu--toolbar" aria-labelledby="region">
                <?php
                  foreach( $tl as $language ) {
                  ?>
                  <li><a class="lang-switcher" data-lang="<?php echo $language['code']; ?>" href="<?php echo $language['url']; ?>"><?php echo $language['translated_name']; ?></a></li>
                  <?php
                  }
                ?>
              </ul>
              <?php
                $target_code = country_code_to_lang( get_geo_language(), 'code' );
                $target_name = country_code_to_lang( get_geo_language(), 'name' );
              ?>

              <div id="language-confirm-modal" class="modal fade modal--lang-switcher" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header clearfix">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close &times;</span></button>
                    </div>
                    <div class="modal-body clearfix">
                      <p>
                        <span class="upper-blue">You are about to enter another country or region-specific website</span>
                        <br>
                        Please be aware that the website you have requested is intended for the residents of a particular country or countries, as noted on that site. As a result, the site may contain information on products or uses of those products that are not approved in other countries or regions.
                      </p>
                      <p>
                        <span class="upper-blue">Do you wish to continue and exit this website?</span>
                      </p>
                      <p>
                        <a href="<?php echo get_current_url_by_language($target_code); ?>" class="btn btn-default reversed lang-switch-trigger"><?php echo strtoupper( __('Yes','genmark')); ?></a>
                        <button type="button" class="btn btn-default reversed last-lang-trigger" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><?php echo strtoupper( __('No','genmark')); ?></span></button>
                      </p>

                    </div>
                  </div>
                </div>
              </div>

              <div id="wrong-region-modal" class="modal fade modal--lang-switcher" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header clearfix">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close &times;</span></button>
                    </div>
                    <div class="modal-body clearfix">
                      <p>
                        <span class="upper-blue">Region Detection</span>
                        <br>
                        We have detected that you may be viewing the site from the <strong><?php echo $target_name; ?></strong> region. Site content may differ by region.
                      </p>
                      <p>
                        <span class="upper-blue">Do you wish to stay on the current site, or be redirected to the <?php echo $target_name; ?> site?</span>
                      </p>
                      <p>
                        <a href="<?php echo get_current_url_by_language($target_code); ?>" class="btn btn-default reversed lang-switch-trigger" data-lang="<?php echo $target_code; ?>"><?php echo strtoupper( sprintf( __("Redirect to %s site"), $target_name )); ?></a>
                        <button type="button" class="btn btn-default last-lang-trigger" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><?php echo strtoupper( __('Stay on current site','genmark')); ?></span></button>
                      </p>

                    </div>
                  </div>
                </div>
              </div>

            </div>

            <?php
              endif;
            ?>

          </div>

          <div class="col-xs-12 col-sm-8 text-right hidden-xs">
          <?php
            $contact_page_id = apply_filters( 'wpml_object_id', 100, 'page' );
          ?>
            <a href="<?php echo get_the_permalink($contact_page_id); ?>"><i class="fa fa-phone"></i> Contact Us</a>

            <?php
              $hide_login = get_field( 'hide_footer_login' );
              if( empty( $hide_login ) || 'true' !== array_pop( $hide_login )):
            ?>
             &nbsp;|&nbsp;


            <?php if( is_user_logged_in() ): ?>
              <a href="<?php echo get_home_url(); ?>/customer-resource-center/">Customer Resource Center</a> | <a href="<?echo wp_logout_url(get_current_url_by_language()); ?>">Sign Out</a>
            <?php else: ?>
              <a class="crc-popover-toggle" href="<?php echo get_home_url(); ?>/customer-resource-center/">Customer Resource Center <span class="caret"></span></a>
            <?php endif; ?>

            <div id="crc-popover" class="popover popover--crc bottom">
              <div class="arrow"></div>
              <div class="popover-content">

              <?php

              if( is_user_logged_in() ):

                $user = wp_get_current_user();
                ?>

                <p>
                  Logged in as: <strong><?php echo $user->display_name; ?></strong>
                </p>

                  <p>(<a href="<?echo wp_logout_url(get_current_url_by_language()); ?>">Sign Out</a>)</p>

                </div>

                <?php else: ?>

                <h3 class="popover--crc__title">Log In</h3>

                  <form action="<?php echo WP_SITEURL; ?>/wp-login.php" method="post">

                    <div class="form-group">
                      <label for="user_login--popover">Username</label>
                      <input name="log" id="user_login--popover" class="form-control" value="" size="20" type="text">
                    </div>

                    <div class="form-group">
                      <label for="user_pass--popover">Password</label>
                      <input name="pwd" id="user_pass--popover" class="form-control" value="" size="20" type="password">
                    </div>

                    <div class="checkbox">
                      <label for="rememberme"><input name="rememberme" id="rememberme" value="forever" type="checkbox"> Remember Me</label>
                    </div>

                    <input name="wp-submit" id="wp-submit--popover" class="btn btn-default" value="<?php echo strtoupper( __('Log In','genmark')); ?>" type="submit">
                    <input name="redirect_to" value="<?php echo get_home_url(); ?>/customer-resource-center/" type="hidden">

                    <p>
                      <a href="<?php echo get_home_url(); ?>/account/forgot-password/">Forgot Password?</a>
                    </p>

                  </form>

                </div>

                <div class="popover__footer">
                  <h3 class="popover__footer__title">Don't have an account?</h3>
                  <a href="<?php echo get_home_url(); ?>/account/create-account/" class="btn btn-default reversed"><?php echo strtoupper( __('Create Account','genmark')); ?></a>
                </div>

              <?php endif; ?>
            </div>

              <?php
              endif;
            ?>
          </div>

        </div>
      </div>

    </div>

    <?php

      $alert = isset( $_REQUEST['alert'] ) ? $_REQUEST['alert'] : false;

      if( $alert !== false ):
        ?>
        <div class="row alertbar" id="alertbar--regionchanged">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 clearfix">

                <div class="media">

                  <div class="media-left media-middle">
                    <i class="alertbar__icon fa fa-exclamation"></i>
                  </div>

                  <div class="media-body">
                    <p class="alertbar__text">

                      <?php
                        switch( $alert ) {
                          case 'regionchanged':
                            ?>
                              You have been redirected to the <strong><?php echo ICL_LANGUAGE_NAME; ?></strong> region due to your detected region settings.
                              If this redirection was made in error, you may use the Region Selector above to navigate to your preferred region.
                            <?php
                            break;
                          case 'lastlang':
                            ?>
                              You have been redirected to the <strong><?php echo ICL_LANGUAGE_NAME; ?></strong> region due to your previous browsing choices.
                              If this redirection was made in error, you may use the Region Selector above to navigate to your preferred region.
                            <?php
                            break;
                        }
                      ?>

                      <a href="<?php get_the_permalink(); ?>" class="alertbar__close" id="alertbar-close">Dismiss this message</a>
                    </p>
                  </div>

                  <div class="media-right media-middle" style="min-width: 75px";>
                    <a href="<?php get_the_permalink(); ?>" class="alertbar__close" id="alertbar-close-icon">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/btn-close_360.png" alt="close">
                    </a>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <?php
      endif;
    ?>

    <div class="row navbar clearfix">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 clearfix">

              <a href="<?php echo get_home_url(); ?>" class="header__logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.jpg" alt=""></a>

              <a href="#" class="menu-toggle btn btn-default"><i class="fa fa-bars"></i> Menu</a>

              <div class="pull-right clearfix main-navigation__container">

              <div class="site-search site-search--navbar pull-right text-right hidden-xs hidden-sm">

                <label for="nav-search" class="sr-only">Search GenMark</label>
                <form action="<?php echo get_home_url(); ?>" method="get">
                  <input type="text" id="nav-search" name="s" placeholder="Search GenMark" />

                  <input class="site-search--navbar__submit" type="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/btn-search.png" alt="Search">
                </form>
              </div>

              <div class="hidden-lg" style="clear: both;"></div>

                <?php

                  wp_nav_menu( array(
                      'theme_location'  => 'main_nav',
                      'container'       => false,
                      'menu_class'      => 'main-navigation',//  navbar-right
                      'walker'          => new Pace_Nav_Walker(),
                    )
                  );

                ?>


                <div class="col-xs-12">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="megamenu"></div>
                    </div>
                  </div>
                </div>
              </div>

           </div>
        </div>
      </div>

    </div>

    <?php if( (is_page() && !is_front_page()) || is_single()): ?>
    <div class="row breadcrumbs hidden-xs">
      <div class="container">
        <div class="row">

          <div class="col-xs-12">
            <ul class="breadcrumbs__list">

              <?php

                global $post;
                $separator = '>';

                // Single's parent is set manually on the single page. Treat the single page as its parent
                // temporarily so that the breadcrumbs and subnav will be created.
                if( is_single() ) {
                  $single_post = $post;
                  $post = get_post( $post->post_parent );
                }

                if( $post->post_parent ){

                    // If child page, get parents
                    $anc = get_post_ancestors( $post->ID );

                    // Get parents in the right order
                    $anc = array_reverse($anc);

                    // Parent page loop
                    $count = 0;
                    foreach ( $anc as $ancestor ) {

                      ?>
                        <li class="breadcrumbs__item">
                          <?php if( $count > 1 ): ?><a class="breadcrumbs__link" href="<?php echo get_permalink($ancestor); ?>"><?php endif; ?>
                          <?php
                            $title = get_the_alt_title($ancestor);
                            $title = genmark_upper( $title );

                            echo $title;
                          ?>
                          <?php if( $count++ > 1 ): ?></a><?php endif; ?>
                        </li>
                      <?php
                    }

                }

                $title = get_the_alt_title();
                $title = genmark_upper( $title );

                echo '<li class="breadcrumbs__item"><a class="breadcrumbs__link" href="'. get_permalink($post->ID) . '">' . $title . '</a></li>';

                // Reset the $post back to single_post after the breadcrumbs and subnav are created
                if( is_single() ) {
                  $post = $single_post;
                }

              ?>
            </ul>
          </div>

        </div>
      </div>
    </div>
    <?php elseif( is_search() ): ?>
    <div class="row breadcrumbs hidden-xs">
      <div class="container">
        <div class="row">

          <div class="col-xs-12">
            <ul class="breadcrumbs__list">
              <li class="breadcrumbs__item">SEARCH RESULTS</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>

  </div>


  <?php if( is_front_page() ): ?>

  <?php
    $eplex_page_id = apply_filters( 'wpml_object_id', 5, 'page' );
  ?>

  <div class="container">
    <?php if( ICL_LANGUAGE_CODE == 'int' ): ?>
        <div class="hero hero--eplex-int hero--<?php echo ICL_LANGUAGE_CODE; ?>">
            <a href="/int/solutions/panels/eplex-panels/blood-culture-identification-panels/" class="hero__img-int"></a>
          <?php else: ?>
            <div class="hero--eplex hero--eplex?>">
            <!-- <a href="<?php echo get_the_permalink($eplex_page_id); ?>?gallery=0" class="hero__img"></a> -->
          <?php endif; ?>

          <?php if( ICL_LANGUAGE_CODE == 'int' ): ?>
            <div class="row">
              <div class="col-xs-12 col-sm-5" style="z-index: 10;">
                <div class="hero__content-<?php echo ICL_LANGUAGE_CODE; ?>">
          <?php else: ?>
            <div class="row hero--eplex-container">
              <a href="/solutions/panels/eplex-panels/blood-culture-identification-panels/" class="hero__img-us">
              <div class="col-xs-12 col-sm-9">
                <div class="hero__content">
          <?php endif; ?>


        <?php if( ICL_LANGUAGE_CODE == 'int' ): ?>
          <h1 class="hero__content-int__title">
            <span class="bigger">The Only Test</span> <br>for Rapid, Routine Blood<br>Culture Identification
          </h1>
          <p>
            The broadest inclusivity of <br>organisms that cause sepsis <br>and their resistance genes
          </p>
          <?php else: ?>
            <h1 class="hero__title">
              ePlex<sup>®</sup>
            </h1>
              <h2 class="hero-sub-title">Blood Culture Identification Panels</h2>
              <div class="hero-title-divider"></div>
              <span class="hero-blurb"><em>The only test for rapid routine blood culture identification</em></span>
          <?php endif; ?>
          </div>
        </div><!-- /.col-xs-12 col-lg- -->
        <?php if( ICL_LANGUAGE_CODE != 'int' ): ?>
          <div class="col-xs-12 col-lg-3 hero-side-info-container">
            <div class="hero-side-info">
              <em>
                BCID-GP<br>
                & BCID-FP<br>
                Panels<br>
                <strong>FDA Cleared</strong>
              </em>
            </div>
          </div>
        </a>
        <?php endif; ?>
      </div><!-- /.row -->

    </div><!-- /.container -->
  </div>

  <?php /*
  <div class="hero">
    <div class="container">

      <div class="hero__content">
          <h1><?php echo get_field('hero_tagline'); ?></h1>
          <?php
            $acute_page_id = apply_filters( 'wpml_object_id', 54, 'page' );
            $gastro_page_id = apply_filters( 'wpml_object_id', 60, 'page' );
            $respiratory_page_id = apply_filters( 'wpml_object_id', 58, 'page' );
            $sepsis_page_id = apply_filters( 'wpml_object_id', 62, 'page' );
          ?>

          <p>
            <a href="<?php echo get_the_permalink( $respiratory_page_id ); ?>" class="btn btn-default hero__btn">
              <img class="hero__btn__icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-respir.png" alt=""><img class="hero__btn__icon--hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-respir_hover.png" alt=""><br>
              <?php echo strtoupper( __('Respiratory Infections','genmark')); ?>
            </a>&nbsp;
            <a href="<?php echo get_the_permalink( $sepsis_page_id ); ?>" class="btn btn-default hero__btn">
              <img class="hero__btn__icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-sepsis.png" alt=""><img class="hero__btn__icon--hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-sepsis_hover.png" alt=""><br>
              <?php echo strtoupper( __('Sepsis','genmark')); ?>
            </a>&nbsp;
            <a href="<?php echo get_the_permalink( $gastro_page_id ); ?>" class="btn btn-default hero__btn">
              <img class="hero__btn__icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-gastro.png" alt=""><img class="hero__btn__icon--hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-gastro_hover.png" alt=""><br>
              <?php echo strtoupper( __('GI Infections','genmark')); ?>
            </a>&nbsp;
            <a href="<?php echo get_the_permalink( $acute_page_id ); ?>" class="btn btn-default hero__btn">
              <img class="hero__btn__icon" width="29" height="34" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon_cns_infections_x2.png" alt=""><img class="hero__btn__icon--hover" width="29" height="34" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon_cns_infections_hover_x2.png" alt=""><br>
              <?php echo strtoupper( __('CNS Infections','genmark')); ?>
            </a>
          </p>
        </div>
      </div><!-- .hero__content -->
    </div><!-- .container -->
  </div><!-- .hero -->
  */ ?>

  <?php endif; ?>

</header>
<?php endif; ?>

<section id="body">
