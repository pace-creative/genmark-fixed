  </section><!-- #body -->
<?php if ( !is_page_template('templates/landing-page-bcid-testing.php') ) : ?>
  <footer id="footer" class="container-fluid noprint">

    <div class="row prefooter">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">

            <?php

              if( is_user_logged_in() ):

                $user = wp_get_current_user();
              ?>

              <div class="prefooter__left col-sm-12 col-md-6">
                <h3><span class="thin">You are currently logged in  as: </span><?php echo $user->display_name; ?></h3>
              </div>

              <div class="prefooter__right col-sm-12 col-md-6">
                <h3>
                  <a class="btn btn-default btn-go-to-crc" href="<?echo wp_logout_url(get_current_url_by_language()); ?>"><?php echo strtoupper( __( 'Go to Customer Resource Center','genmark')); ?></a>
                  <span>OR</span>
                  <a class="btn btn-default" href="<?echo wp_logout_url(get_current_url_by_language()); ?>"><?php echo strtoupper( __('Sign Out','genmark')); ?></a>
                </h3>
              </div>
              <?php else: ?>
              <?php
                $hide_login = get_field( 'hide_footer_login' );
                if( empty( $hide_login ) || 'true' !== array_pop( $hide_login )):
              ?>
                <form class="form-inline form-inline--login-widget" action="<?php echo WP_SITEURL; ?>/wp-login.php" method="post">

                  <div class="form-group form-group--title">
                    <h2>Customer Resource Center Login:</h2>
                  </div>

                  <div class="form-group">
                    <label for="user_login">Username</label>
                    <input name="log" id="user_login" class="form-control" value="" size="20" type="text">
                  </div>

                  <div class="form-group">
                    <label for="user_pass">Password</label>
                    <input name="pwd" id="user_pass" class="form-control" value="" size="20" type="password">
                  </div>

                  <input name="wp-submit" id="wp-submit" class="btn btn-default" value="<?php echo strtoupper( __('Log In','genmark')); ?>" type="submit">
                  <input name="redirect_to" value="<?php echo get_home_url(); ?>/customer-resource-center/" type="hidden">

                  &nbsp;&nbsp; <a href="<?php echo get_home_url(); ?>/account/create-account/">Create Account</a>

                </form>
              <?php endif; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row mainfooter">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-3 col-md-2">
            <a href="<?php echo get_home_url(); ?>" class="footer__logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.jpg" alt=""></a>

          </div>
          <div class="col-xs-12 col-sm-9 col-md-7 col-lg-6">
            <div class="row">
              <div class="col-xs-12 col-sm-4">
                <p class="footer__contact first">
                  <strong>GenMark Diagnostics, Inc.</strong><br>
                  5964 La Place Court <br>
                  Carlsbad, CA 92008, USA
                </p>
              </div>
              <div class="col-xs-12 col-sm-4">
                <p class="footer__contact">
                  <strong>Toll-free</strong>: 1.800.373.6767 <br>
                  <strong>Phone</strong>: +1 760.448.4300
                </p>
              </div>
              <div class="col-xs-12 col-sm-4">
                <p class="footer__contact">
                  <strong>Fax</strong>: +1 760.448.4301 <br>
                  <strong>Email</strong>: <a href="mailto:info@genmarkdx.com">info@genmarkdx.com</a>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-lg-4">

            <div class="footer__controls clearfix">
              <a href="#" data-toggle="modal" data-target="#sitemap-modal" class="btn btn--small pull-left btn-default fa-icon fa-icon--link"><?php echo strtoupper( __('Open Site Map','genmark')); ?></a>

              <div class="site-search site-search--footer">

                <label for="nav-search--footer" class="sr-only">Search GenMark</label>
                <form action="<?php echo get_home_url(); ?>" method="get">
                  <input type="text" id="nav-search--footer" class="site-search--footer__input" name="s" placeholder="Search GenMark" />

                  <input class="site-search--navbar__submit" type="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/btn-search.png" alt="Search">
                </form>

              </div>

            </div>

            <div id="sitemap-modal" class="modal--site-map modal fade" tabindex="-1" role="dialog">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close x</span></button>
                  </div>
                  <div class="modal-body clearfix">
                <?php

                    $args = array(
                      'post_type' => 'page',
                      'meta_key' => 'hide_from_sitemap',
                      'meta_value' => 1,
                    );

                    $excluded_pages = get_pages($args);
                    $excluded_ids = array();

                    foreach( $excluded_pages as $page ) {
                      $excluded_ids[] = $page->ID;
                    }

                    // get children
                    $args = array(
                      'post_parent' => 0,
                      'post_type' => 'page',
                      'numberposts' => -1,
                      'post_status' => 'publish',
                      'orderby' => 'menu_order',
                      'order' => 'ASC',
                      'post__not_in' => $excluded_ids, //array(102, 372, $frontpage_id),
                    );

                    $children = get_children_recursive( $args );

                  ?>
                  <?php

                    $args = array(
                      'carets' => false,
                      'list_class' => 'sitemap',
                    );

                    echo children2list( $children, $args );
                  ?>
                  </div>
                </div>
              </div>
            </div>

            <p class="colophon visible-lg">
              <?php the_colophon(); ?>
            </p>

          </div>


        </div>

        <div class="row">
          <div class="col-xs-12 text-right">
            <p class="colophon hidden-lg">
              <?php the_colophon(); ?>
            </p>
            <a href="http://creativepace.com/" class="created-by__btn">
              <img class="created-by__btn__icon--hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer-site-by-pace-creative_hover.png" alt=""><img class="created-by__btn__icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer-site-by-pace-creative.png" alt=""><br>
            </a>
          </div>
        </div>

      </div>
    </div>

  </footer>
<?php endif; ?>

  <!-- video-modal content will be populated -->
  <div id="video-modal-ajax" class="modal fade" tabindex="-1" role="dialog"></div>

  <div id="reqInfoSent" class="modal--req-info modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
      <div class="modal-header clearfix"><button class="close" type="button" data-dismiss="modal">Close ×</button></div>
        <div class="modal-body">
          <p><span class="upper-blue">Your message was sent successfully. Thank you.</span></p>
        </div>
      </div>
    </div>
  </div>
  <?php wp_footer(); ?>
  <!-- ends page -->

  <?php
    if ( WP_ENV == 'production' ) {
      ?>
        <script type="text/javascript" language="javascript">
          var sf14gv = 22309;
          (function() {
          var sf14g = document.createElement('script'); sf14g.type = 'text/javascript'; sf14g.async = true;
          sf14g.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 't.sf14g.com/sf14g.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sf14g, s);
          })();
        </script>
      <?php
    }
  ?>
  </body>
</html>
