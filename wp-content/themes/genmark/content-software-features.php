<div class="row tabs">
  <div class="col-xs-12 col-sm-3">
    <ul class="tab-list">
      <li data-target="1" class="tab-list__item active">Templated Comments</li>
      <li data-target="2" class="tab-list__item">Report Scheduler</li>
      <li data-target="3" class="tab-list__item">Custom Epidemiology <br>Reports</li>
      <li data-target="4" class="tab-list__item">Laboratory Information <br>System (LIS)</li>
      <li data-target="5" class="tab-list__item">External Control</li>
      <li data-target="6" class="tab-list__item">Security and User <br>Management</li>
    </ul>
  </div>

  <div class="col-xs-12 col-sm-9">
    <div class="tab-list-content">

      <h3 data-target="1" class="tab-accordion-title active">Templated Comments</h3>
      <div class="tab-list-content__item active" id="tab-list-content-1">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-sm-push-6">

            <h3 class="tab-list-content__title">Templated Comments</h3>
            <p><strong>User-defined comments on results reports to help guide treatment
                decisions</strong></p>

            <ul>
              <li>Automate the interpretation of the local antibiogram to guide antibiotic therapy selection</li>
              <li>Fast-track treatment intervention improving Antimicrobial Stewardship Programs and Infection Control</li>
            </ul>

          </div>

          <div class="col-xs-12 col-sm-6 col-sm-pull-6 text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/software-features/templated-comments.png" alt="" />
          </div>
        </div><!-- /.row -->
      </div>

      <h3 data-target="2" class="tab-accordion-title">Report Scheduler</h3>
      <div class="tab-list-content__item" id="tab-list-content-2">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-sm-push-6">

            <h3 class="tab-list-content__title">Report Scheduler</h3>
            <p><strong>Integrated scheduling tool that automates the generation and distribution of report results</strong></p>

            <ul>
              <li>Customized epidemiology reports with automated distribution scheduled daily, weekly or monthly</li>
              <li>Reports are distributed via email to minimize the administrative overhead of gathering data</li>
            </ul>

          </div>

          <div class="col-xs-12 col-sm-6 col-sm-pull-6 text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/software-features/report-scheduler.png" alt="" />
          </div>
        </div><!-- /.row -->
      </div>

      <h3 data-target="3" class="tab-accordion-title">Custom Epidemiology Reports</h3>
      <div class="tab-list-content__item" id="tab-list-content-3">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-sm-push-6">

            <h3 class="tab-list-content__title">Custom Epidemiology Reports</h3>

            <p><strong>Integrated epidemiology reporting to help analyze onboard prevalence data</strong></p>

            <ul>
              <li>Create customized reports for surveillance and monitoring of epidemiology data</li>
              <li>Export custom epidemiology datasets to excel compatible formats to create multi-series prevalence charts</li>
            </ul>

          </div>

          <div class="col-xs-12 col-sm-6 col-sm-pull-6 text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/software-features/custom-reports.png" alt="" />
          </div>
        </div><!-- /.row -->
      </div>

      <h3 data-target="4" class="tab-accordion-title">Laboratory Information System (LIS)</h3>
      <div class="tab-list-content__item" id="tab-list-content-4">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-sm-push-6">

            <h3 class="tab-list-content__title">Laboratory Information System (LIS)</h3>
            <p><strong>Bi-directional LIS integration improves result turnaround time and reduces risk of transcription errors</strong></p>

            <ul>
              <li>Pending test orders dashboard enables immediate visibility of incoming samples to determine daily workload</li>
              <li>Integrated auto-validation control to manage automatic result release to LIS</li>
              <li>Drivers available for all major LIS vendors</li>
            </ul>

          </div>

          <div class="col-xs-12 col-sm-6 col-sm-pull-6 text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/software-features/lis.png" alt="" />
          </div>
        </div><!-- /.row -->
      </div>

      <h3 data-target="5" class="tab-accordion-title">External Control</h3>
      <div class="tab-list-content__item" id="tab-list-content-5">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-sm-push-6">

            <h3 class="tab-list-content__title">External Control</h3>
            <p><strong>Automated QC tracking to ensure accreditation compliance</strong></p>

            <ul>
              <li>Easily demonstrate QC compliance for lab accreditation with onboard reporting</li>
              <li>Predefine and track external quality control by assay</li>
              <li>Automatically monitor QC frequency and lot changes</li>
            </ul>

          </div>

          <div class="col-xs-12 col-sm-6 col-sm-pull-6 text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/software-features/external-control.png" alt="" />
          </div>
        </div><!-- /.row -->
      </div>

      <h3 data-target="6" class="tab-accordion-title">Security and User Management</h3>
      <div class="tab-list-content__item" id="tab-list-content-6">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-sm-push-6">

            <h3 class="tab-list-content__title">Security and User Management</h3>
            <p><strong>Integrated HIPAA compliant safeguards to manage security risks and vulnerabilities</strong></p>

            <ul>
              <li>User access management to the ePlex system is simplified with active directory integration</li>
              <li>Strong password requirements are enforced to help protect electronic protected health information (ePHI)</li>
            </ul>

          </div>

          <div class="col-xs-12 col-sm-6 col-sm-pull-6 text-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/software-features/security.png" alt="" />
          </div>
        </div><!-- /.row -->
      </div>

    </div>
  </div>
</div><!-- /.row -->
