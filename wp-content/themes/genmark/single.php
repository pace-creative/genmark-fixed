<?php get_header(); ?>
<?php
  if( have_posts() ) {
    the_post();
  }
?>
<div class="page-section page-section--wide">
  <div class="container">

    <div class="row">

      <div class="col-xs-12 col-md-9">
        <div class="page-section__content">
          <h1><?php the_title(); ?></h1>
          <?php the_content(); ?>
        </div>
      </div>

    </div>

  </div>
</div>

<?php get_footer(); ?>