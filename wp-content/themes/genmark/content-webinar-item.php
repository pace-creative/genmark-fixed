<?php

$title = get_the_title();
$date = get_field('webinar-date') ? DateTime::createFromFormat('d/m/Y', get_field('webinar-date')) : false;
$presenters = get_field('webinar-presenters');
$url = esc_url( get_permalink( get_page_by_title( $title ) ) );
?>

<tr>
  <td width="20%">
    <p class="upper-blue"><?php if($date) echo $date->format('M, Y'); else echo 'n.d.'; ?></p>
  </td>
  <td>
    <p><?php echo $title; ?></p>
    <br>
    <p>
      <a class="btn btn-default" href="<?php echo $url; ?>"><?php echo strtoupper( __('Read More','genmark') ); ?></a>

      <?php if( get_field('webinar-video') ): ?>

      <a class="btn btn-default btn-launch-video webinar fa-icon fa-icon--play" id="<?php the_ID(); ?>" href="#" >
        <?php /*
        <!-- <img class="btn__icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-play.png" alt=""> -->
        <!-- <img class="btn__icon--hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-play_hover.png" alt="">  -->
        */ ?>
        <span><?php echo strtoupper( __('Watch Video','genmark') ); ?></span>
      </a>

      <?php endif; ?>

    </p>
  </td>
  <td>
    <p><?php echo $presenters; ?></p>
  </td>
</tr>
