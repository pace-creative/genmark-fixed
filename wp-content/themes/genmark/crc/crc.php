<?php

global $crc_db_version;
$crc_db_version = '1.1';

// init hook
add_action( 'init', 'crc_init' );
function crc_init() {

  global $wpdb, $crc_db_version;
  $installed_ver = get_option( "crc_db_version" );

  if ( $installed_ver != $crc_db_version ) {
    crc_install();
  }

}

function crc_install() {

  global $wpdb, $crc_db_version;

  $table_name = $wpdb->prefix . "crc_oldaccounts";

  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    name tinytext NOT NULL,
    name2 tinytext NOT NULL,
    region varchar(10) DEFAULT '' NOT NULL,
    account varchar(10) DEFAULT '' NOT NULL,
    oldpass varchar(50) DEFAULT '' NOT NULL,
    serials tinytext NOT NULL,
    UNIQUE KEY id (id)
  ) $charset_collate;";

  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

  dbDelta( $sql );

  update_option( 'crc_db_version', $crc_db_version );

}
