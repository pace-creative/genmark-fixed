<?php
  if( have_posts() ) {
    the_post();
  }
  global $post;
  $webinar_id = apply_filters( 'wpml_object_id', 88, 'page' );
  $post->post_parent =  $webinar_id;// Set the parent to the Webinars page manually
  $webinar_footnotes = '<p class="footnote">Webinar footnote</p>';
  $webinar_footnotes = get_field( 'footnotes', $webinar_id );
  $show_disclaimer = get_field( 'show_disclaimer' );

  if( $show_disclaimer === null ) {
    $show_disclaimer = true;
  }

?>
<?php get_header(); ?>

<?php
  $date = get_field('webinar-date') ? DateTime::createFromFormat('Ymd', get_field('webinar-date')) : false;
  $presenters = get_field('webinar-presenters');
  $video = get_field('webinar-video');
  $pdf = get_field('webinar-pdf');

?>
<div class="page-section page-section--wide">
  <div class="container">

    <div class="row">

      <?php get_sidebar(); ?>

      <div class="col-xs-12 col-md-9">


        <div class="page-section__content">


          <h1>Webinars</h1>
          <h2><?php the_title(); ?></h2>

          <?php if ($date): ?>
          <h5 class="upper-blue"><?php echo $date->format('M, Y'); ?></h5>
          <?php endif; ?>

          <?php if ($presenters): ?>
          <p>Presented by: <?php echo $presenters; ?></p>
          <?php endif; ?>

          <!-- video -->
          <?php /* if( !empty( $video ) || !empty( $pdf ) ): */ ?>
          <?php if( !empty( $video ) ): ?>
          <video id="webinar_video_<?php the_ID(); ?>" class="video-js vjs-default-skin vjs-big-play-centered"
            controls preload="auto" width="100%" height="450"
            poster="<?php echo get_stylesheet_directory_uri(); ?>/img/video-poster.jpg"
            data-setup='{}'>
            <source src="<?php echo $video['url']; ?>" type='video/mp4' />
            <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
          </video>
          <br>
          <?php endif; ?>
          <!-- end video -->

          <?php if (!empty( $pdf ) ): ?>
            <a href="<?php echo $pdf['url']; ?>" class="btn btn-default fa-icon fa-icon--pdf"><?php echo strtoupper(__('View PDF','')); ?></a>
            <br>
          <?php endif; ?>

          <hr>

          <?php the_content(); ?>

          <?php
          // echo get_template_part('content','footnote');
          ?>

          <?php if( $show_disclaimer ): ?>
            <p class="footnote">
              <?php echo $webinar_footnotes; ?>
            </p>
          <?php endif; ?>
        </div><!-- .page-section__content -->

      </div><!-- .col-xs-12 -->

    </div><!-- .row -->

  </div><!-- .container -->
</div><!-- .page-section -->

<?php get_footer(); ?>



   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->
