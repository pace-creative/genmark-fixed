<?php
  $debug = false;
  if( isset( $_POST['username'] )):

    if ( $debug ): ?>
  <div class="container">
    <div class="row">
      <div class="col-xs-12" style="border:1px solid red;">
  <?php endif;

        $username  = ( isset($_POST['username']) ) ? $_POST['username'] : '';
        $email     = ( isset($_POST['email']) )    ? $_POST['email']    : '';
        $pass1     = ( isset($_POST['pass1']) )    ? $_POST['pass1']    : '';
        $pass2     = ( isset($_POST['pass2']) )    ? $_POST['pass2']    : '';
        $acc       = ( isset($_POST['acc']) )      ? $_POST['acc']      : '';
        $serial    = ( isset($_POST['serial']) )   ? $_POST['serial']   : '';
        $country    = ( isset($_POST['country']) )   ? $_POST['country']   : '';

        $errors = ["", "", "", "", "", ""];
        $serial_error = "";

      if ( $debug ):
        ?><?php
        /*
        <h1>create-account-process.php</h1>
        <table style="width:50%;">
          <tr>
            <td>username</td>
            <td><?php echo $username; ?></td>
          </tr>
          <tr>
            <td>email</td>
            <td><?php echo $email; ?></td>
          </tr>
          <tr>
            <td>pass1</td>
            <td><?php echo $pass1; ?></td>
          </tr>
          <tr>
            <td>pass2</td>
            <td><?php echo $pass2; ?></td>
          </tr>
          <tr>
            <td>acc</td>
            <td><?php echo $acc; ?></td>
          </tr>
          <tr>
            <td>serial</td>
            <td><?php echo $serial; ?></td>
          </tr>
        </table>
        */
       ?><?php
        endif;

        // Username check
        if( sanitize_user( $username, true ) == '' ){
          $errors[0] = 'Username is required.';
        } else {
          // TODO: Length check

          if ( username_exists( sanitize_user( $username, true )) ) {
            $errors[0] = 'Username <i>'.$username.'</i> is registered.';
          }

        }

        // Email check
        if( empty( $email )) {
          $errors[1] = 'Email is required.';
        } elseif( is_email( $email )) {

          if( email_exists( $email )) {
            $errors[1] = 'This email is registered.';
          }

        } else {
          $errors[1] = 'This email is invaild.';
        }

        // Country check
        if( empty( $country )) {
          $errors[8] = 'Country is required.';
        } elseif( $country == 0 ) {
          $error[8] = 'Country is required';
        }

        // Password check
        $password = "";
        if( empty( $pass1 )) {
          $errors[2] = 'Password is required.';
        }
        if( empty( $pass2 )) {
          $errors[3] = 'Confirm password is required.';
        }
        if( !empty( $pass1 ) && !empty( $pass2 )) {
          // Both passwords entered

          // TODO: Length check

          if( $pass1 != $pass2 ) {
            $errors[2] = 'Passwords do not match.';
          } else {
            // Password matches confirm password
            $password = $pass1;
          }

        }

        $validated_serial = false;

        // Account Number check: check account number with db entry
        if( !empty( $acc )) {

          $old_acc = get_old_user_by_account($acc);

          if( !$old_acc ) {

            $errors[4] = 'This account number is not vaild.';
            if( $debug ) echo '<p>!$old_acc</p>';

          } elseif( $old_acc ) {

            // account no. check passed
            $old_acc = $old_acc[0];

            if( $debug ) {

              echo '<p>$old_acc</p><pre>';
              print_r( $old_acc );
              echo '</pre>';

            }

          }
        }

        // Serial number check
        if( empty( $serial )) {
          $serial_error = 'Serial number is not entered.';
          $errors[7] = 'Serial number is required';
        } else{
          $old_acc = get_old_user_by_serial( $serial );

          // var_dump($old_acc);
          if( $old_acc == false ){
            $errors[7] = 'This serial number is not valid';
          }else{
            $validated_serial = $serial;
          }



          // print_r ( $validated_serial );

        }

        $errors = array_filter($errors);

        if( !empty( $errors )) { //If no errors

          if($debug) {

            echo '<p>$errors</p><pre>';
            print_r($errors);
            echo '</pre>';
            echo '<p>$serial_error: '.$serial_error.'</p>';

          }

        } else {

          if($debug) echo '<p>!$errors</p>';

          // Create the user
          $user_id = wp_create_user ( $username, $password, $email );

          if( $debug ) {

            echo '<p>Trying to create user.</p>';

            if( is_wp_error( $user_id )){
              echo '<p>is error</p><pre>';
              print_r( $user_id );
              echo '</pre>';
            }

            if( !is_wp_error( $user_id )) {
              echo '<p>!is error. $user_id='.$user_id.'</p>';
            }
          }


          //If user created successfully
          if( !is_wp_error( $user_id )){

            // var_dump($user_id);
            genmark_set_user_role_and_detail( $user_id, $validated_serial, $old_acc, $country );
            $data1 = wp_new_user_notification( $user_id );

            // var_dump( $data1 );

            if( $debug ) echo'!is_wp_error';

            // Redirect to Login page
            if(!$debug) {

              $login_ID = 971;
              $url =  get_permalink(apply_filters('wpml_object_id', $login_ID, 'page'));

              $action = 'created';

              if( empty( $validated_serial )) {
                $action = 'pending';
              }

              // var_dump($url);exit;
              $url = add_query_arg( array(
                'id' => $username,
                'a' => $action,
              ), $url);

              wp_redirect( $url );
              exit;
              wp_redirect( get_home_url() . "/login?id=$username&a=created" ); exit;
            }

          }

        }
        ?>


  <?php if ( $debug ): echo '<p>end php</p>'; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

<?php endif; ?>


<?php
/**
 * Checks whether the given account number exists.
 *
 * @param string $acc Account Numeber
 * @return null | OBJECT old_user  This old user account with associated company name, region etc.
 */
  function get_old_user_by_account(  ){
    global $wpdb;
    $results = $wpdb->get_results( "SELECT * FROM `wp_crc_oldaccounts`", OBJECT );
    return $results; //$results will be empty is there is a database error
  }

  function get_old_user_by_serial( $serial_input ){
    global $wpdb;
    $results = $wpdb->get_results( "SELECT * FROM `wp_crc_oldaccounts`", OBJECT );
    foreach( $results as $account ){
      $serials = unserialize( $account->serials );
      if ( is_array($serials) ) { // nothing went wrong

        foreach( $serials as $serial ){
          // var_dump( $serial );
          if($serial == $serial_input){

            return $account;

          }
        }//end foreach

      }
    }
    return false; //$results will be empty is there is a database error
  }


/**
 * Serial number check - $validated_serial will be set if is vaild.
 *
 * @param string $acc Account Numeber
 * @return null | OBJECT old_user  This old user account with associated company name, region etc.
 */
  // function genmark_serial_check( $old_acc, $serial_input ){

  //   $validated_serial = "";

  //   if ( $serial_input && $old_acc != 0 ){ // Do the serial number check only if the user entered a serial number.

  //       $serials = unserialize( $account->serials );
  //       // var_dump( $serials );
  //       if ( is_array($serials) ) { // nothing went wrong

  //         foreach( $serials as $serial ){
  //           // var_dump( $serial );
  //           if($serial == $serial_input){
  //             $validated_serial = $serial;
  //             break;
  //           }
  //         }//end foreach

  //       }

  //   }
  //   // print_r( $validated_serial );
  //   return $validated_serial;
  // }


/**
 * Determine whether the entered serial number is valid and assign role (Customer | Customer Pending)
 * to the user correspondingly. It also stores the additional user's information (Serial number,
 * old account ID, region).
 *
 * @param Int $user_id. The ID of the user just created
 * @param string $serial_input. The input serial number
 * @param Object $old_acc. Retrieved from wp_crc_oldaccounts
 */
  function genmark_set_user_role_and_detail( $user_id, $validated_serial, $old_acc, $country ){

    global $debug;
    $user = new WP_User ( $user_id );

    if( $validated_serial ){
      if( $debug ) echo '<p>validated_serial: '.$validated_serial.'</p>';
    } else {
      if( $debug ) echo '<p>!validated_serial: '.$validated_serial.'</p>';
    }

    // var_dump($user);

    $user->set_role ('customer');

    // Reformat $user_id for ACF
    $user_id = 'user_'.$user_id;

    $serial_field_key = 'field_55c40845bbfb4';
    $value = $validated_serial;
    update_field($serial_field_key, $value, $user_id);

    $company_field_key = 'field_55c4083abbfb3';
    $value = $old_acc->name;
    update_field($company_field_key, $value, $user_id);

    // $acc_field_key = 'field_55c40855bbfb6';
    // $value = $old_acc->account;
    // update_field($acc_field_key, $value, $user_id);
    //
    if( $country == 'US'){
      $value = 'us';
    }else{
      $value = 'intl';
    }
    $region_field_key = 'field_55c543ea3efc3';
    update_field($region_field_key, $value, $user_id);

    $welcome_field_key = 'field_55c52ea39019d';
    update_field($welcome_field_key, true, $user_id);

  }
?>
