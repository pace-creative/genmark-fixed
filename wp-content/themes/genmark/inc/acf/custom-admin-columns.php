<?php
  // http://www.elliotcondon.com/advanced-custom-fields-admin-custom-columns/
  /*-------------------------------------------------------------------------------
    Custom Publications Columns
  -------------------------------------------------------------------------------*/
  function the_publications_columns( $columns ) {
    $columns = array(
      'cb' => '<input type="checkbox" />',
      'title' => 'Title',
      'publication-disease_state' => 'Disease State',
      // 'publication-date' => 'Date',
      'date' => 'Date',
    );
    return $columns;
  }

  function the_custom_publications_columns( $column ) {

    if( $column == 'publication-date' ) {

      $date_string = get_field( $column );
      $date = $date_string ? date_create($date_string) : false;
      echo ucwords( get_post_status() ) . '<br>';
      echo $date ? date_format($date, 'Y-m-d') : '';

    } elseif( $column == 'publication-disease_state') {

      $disease_state_field = get_field_object( $column );
      echo isset($disease_state_field['choices'][ get_field($column) ]) ? $disease_state_field['choices'][get_field($column)] : '';

    } else {

      echo get_field( $column );

    }
  }
  add_action("manage_publications_posts_custom_column", "the_custom_publications_columns");
  add_filter("manage_edit-publications_columns", "the_publications_columns");

  /*-------------------------------------------------------------------------------
    Sortable Publications Columns
  -------------------------------------------------------------------------------*/
  function the_publications_column_register_sortable( $columns ) {
    // $columns['publication-disease_state'] = 'publication-disease_state';
    return $columns;
  }
  add_filter("manage_edit-publications_sortable_columns", "the_publications_column_register_sortable" );

?>