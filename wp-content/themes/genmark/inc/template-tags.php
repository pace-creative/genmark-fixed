<?php

function get_the_colophon() {

  $colophon = '
    <a href="'. get_home_url() . '/terms/">Terms</a> &copy; Copyright ' . date('Y') . ' GenMark Diagnostics, Inc. All rights reserved.
  ';

  return $colophon;

}

function the_colophon() {
  echo get_the_colophon();
}