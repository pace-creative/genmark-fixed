<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <?php
        $hide_title = get_field('hide_title');
      ?>

      <?php if( !$hide_title ): ?>
        <h1 class="page-title"><?php the_title(); ?></h1>
      <?php endif; ?>

      <?php the_content(); ?>


      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>
