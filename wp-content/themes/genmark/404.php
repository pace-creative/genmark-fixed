<?php

  $s = $_SERVER['REQUEST_URI'];

  // check for path
  $path = str_replace('index.html','',$s);
  $post = get_page_by_path($path);

  if( !empty( $post )) {
    // pvar_dump(get_the_permalink($post->ID));
    // exit;
    wp_redirect( get_the_permalink($post->ID) );
    exit;
  }

?>
<?php get_header(); ?>

<div class="container-fluid">
  <div class="row breadcrumbs hidden-xs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul class="breadcrumbs__list">
            <li class="breadcrumbs__item">404</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<section>
  <div class="container">
    <div class="row">

      <div class="col-xs-12 col-md-9">
        <h1>404</h1>
        <h2>We’re sorry - something has gone wrong on our end.</h2>
        <p>The page you are looking for appears to be moved, deleted or does not exist. You can either go back or head to the homepage.</p>
      </div>

      <div class="col-xs-12 col-md-9">
        <label for="p404-search" class="sr-only">Search GenMark</label>
        <form action="<?php echo get_home_url(); ?>" method="get">
          <input type="text" name="s" class="p404-search form-control" placeholder="Search GenMark" />
          <input class="p404-search--__submit" type="image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/btn-search.png" alt="Search">
        </form>
      </div>

      <div class="col-xs-12 col-md-9">
        <?php

          $s = str_replace('/', ' ', $s);
          $s = trim(str_replace('-', ' ', $s));
          $s = preg_replace("/\.(html|htm|php|asp|aspx)$/","",$s);
          $s = preg_replace("/[^a-z ]/","",$s);
          $s = str_replace('index', '', $s );
          $s = str_replace('int', '', $s );

          $args = array(
            's' => $s,
            'post_type' => array(
              'page',
              'Webinars',
              'Publications',
              // 'Conferences',
              'people',
            ),
            'posts_per_page' => 5,
          );

          $the_query = new WP_Query( $args );
          relevanssi_do_query($the_query);

          if($the_query->have_posts()):
            ?>
            <h2>Related Pages</h2>
            <?php
            while( $the_query->have_posts()):
              $the_query->the_post();
              ?>
                <h3><a href="<?php echo get_the_permalink( get_the_ID() ); ?>"><?php the_title(); ?></a></h3>
                <?php the_excerpt(); ?>

              <?php
            endwhile;
          endif;
          ?>
      </div>



    </div>
  </div>
</section>


<?php get_footer(); ?>
