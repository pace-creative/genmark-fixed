<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <div class="row">

        <div class="col-xs-12">
          <form action="" class="form-inline">
            <div class="form-group">
              <label for="id2">Year: </label>
              <select class="form-control">
                <option value="">All Years</option>
              </select>
            </div>
            <div class="form-group">
              <input class="btn btn-default reversed" type="submit" value="Search">&nbsp;
              <a href="#" class="btn btn-default">Reset</a>
            </div>
          </form>
        </div>

      </div>

      <br>

      <table class="item-list" summary="Sortable Table (Click a column header to sort)">
        <thead>
          <tr>
            <th><a class="sortable desc" href="#">Date <abbr style="border-style: none;" class="sort-icon" title="(sorted descending)"><span class="caret"></span></abbr></a></th>
            <th><a class="sortable" href="#">Title <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
            <th>Download</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <td>
              <strong>Apr 29, 2015</strong>
            </td>
            <td>
              <a href="#">GenMark Diagnostics to Present at Bank of America Merrill Lynch 2015 Health Care Conference</a>
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              <strong>Apr 16, 2015</strong>
            </td>
            <td>
              <a href="#">GenMark Announces Positive Preliminary Q1 Financial Results and ePlex Update</a>
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              <strong>Feb 24, 2015</strong>
            </td>
            <td>
              <a href="#">GenMark Reports Fourth Quarter and Full Year 2014 Results</a>
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              <strong>Feb 23, 2015</strong>
            </td>
            <td>
              <a href="#">GenMark Announces Positive Preliminary Q1 Financial Results and ePlex Update</a>
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              <strong>Jan 12, 2015</strong>
            </td>
            <td>
              <a href="#">GenMark Announces Completion of ePlex Development and Preliminary Q4 Financial Results</a>
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

        </tbody>
      </table>

      <ul class="pagination">
        <li class="pagination__item"><a href="#" class="btn btn-default">1</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">2</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">3</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">4</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">></a></li>
      </ul>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>