<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <h2>Step 1 of 3: Select Options</h2>
      <p>
        You may automatically receive GenMark Dx financial information by email. Please enter your preferences for email notifications below and click "Continue" to enter your contact information on the next page. If you have already signed up and would like to review your subscription, <a href="#">click here</a>.
      </p>

      <h3>Press Releases</h3>
      <div class="checkbox checkbox--nolabel">
        <input type="checkbox"> General Releases
      </div>

      <h3>SEC Filings</h3>
      <div class="checkbox checkbox--nolabel">
        <input type="checkbox"> All SEC Filings
      </div>
      <div class="checkbox checkbox--nolabel checkbox--indent">
        <input type="checkbox"> Insider Transactions
      </div>
      <div class="checkbox checkbox--nolabel checkbox--indent">
        <input type="checkbox"> Quarterly and Annual Reports
      </div>

      <h3>Webcasts and Events</h3>
      <div class="checkbox checkbox--nolabel">
        <input type="checkbox"> All Events and Webcasts
      </div>
      <div class="checkbox checkbox--nolabel checkbox--indent">
        <input type="checkbox"> Remind me <select name="" id=""><option>choose a time</option></select> in advance of any event
      </div>

      <h3>Stock Information</h3>
      <div class="checkbox checkbox--nolabel">
        <input type="checkbox"> Quite By Email (<em>Send me a stock update at the end of the trading day</em>)
      </div>
      <div class="checkbox checkbox--nolabel">
        <input type="checkbox"> Weekly Stock Summary (<em>Send me a stock update at the end of the week</em>)
      </div>
      <div class="checkbox checkbox--nolabel">
        <input type="checkbox"> Stock Threshold Alert (<em>Alert me when the stock crosses the following price thresholds</em>)
        <br>
        Current Stock Price is $9.890
        <br>
        Low $ <input type="text" size="4"> &nbsp; High $ <input type="text" size="4">
      </div>
      <div class="checkbox checkbox--nolabel">
        <input type="checkbox"> Percent Change Alert (<em>Alert me when the stock crosses the following price thresholds</em>)
        <br>
        Alert me when the stock changes more than <input type="text" size="4"> % in one day.
      </div>

      <input type="submit" class="btn btn-default" value="continue">

      <p>
        <a href="#"><em>Unsubscribe from Investor Relations email alerts.</em></a>
      </p>
      <p>
        <em>Our email alerts provide automated opt-out methods as well as complete contact information.</em>
      </p>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>