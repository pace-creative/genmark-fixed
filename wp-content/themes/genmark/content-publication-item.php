<?php
  global $disease_state_choices;

  $title = get_the_title();
  $date = get_field('publication-date') ? DateTime::createFromFormat('d/m/Y', get_field('publication-date')) : false;
  $authors = get_field('publication-authors');
  $link = get_field('publication-link');
  $disease_state = get_field('publication-disease_state');
  $type = get_field('content_type');

?>

<tr>
  <td width="15%">
    <p class="upper-blue"><?php if($date) echo $date->format('M, Y'); else echo 'n.d.'; ?></p>
  </td>
  <td width="20%">
    <p>
      <?php

        if( !is_array( $disease_state )) {
          $disease_state = array($disease_state);
        }

        foreach( $disease_state as $key => $state ) {

          if( $key > 0 ) {
            echo ', ';
          }

          echo $disease_state_choices ? $disease_state_choices[ $state ] : $state;

        }

      ?>
    </p>
  </td>
  <td>
    <p><?php echo $title; ?></p>
    <p>
      <a class="btn btn-default" target="_blank" href="<?php echo $link; ?>">View</a>
    </p>
  </td>
  <td>
    <p><?php echo $authors; ?></p>
  </td>
  <td width="15%">
    <p>
      <span class="visible-xs">Type: </span><?php echo $type; ?>
    </p>
  </td>
</tr>
