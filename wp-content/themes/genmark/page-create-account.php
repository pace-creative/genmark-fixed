<?php
  require_once 'inc/create-account-process.php';
  get_header();

 //error_template_header
  $eth = '<div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>';
  //error_template_footer
  $etf = '</div>';
?>

<div class="container">
  <div class="row">

    <?php get_sidebar(); ?>

    <div class="col-xs-12 col-md-9">

      <h1>Create an Account</h1>

      <?php the_content(); ?>

      <?php if ( !empty($errors) ): ?>

        <div class="alert alert-info" role="alert">
          <p style="margin-top: 0;">
            <strong>Having trouble creating an account?</strong>
            <br>
            Please contact Customer Service if you continue experience registration issues at +1 800-373-6767 or <a href="mailto:CustomerService@genmarkdx.com?subject=Customer%20Resource%20Center%20Account%20Request">CustomerService@genmarkdx.com</a>
          </p>
        </div>

      <?php endif; ?>

      <p>
        Fields marked with <span class="required" aria-required="true">*</span> are required.
      </p>

      <form action="<?php echo get_permalink(); ?>" method="post">

        <div class="form-group">
          <label for="create_acc_username" class="upper-blue">USERNAME<span class="required" aria-required="true">*</span>:</label>
          <input name="username" value="<?php if(isset($_POST['username'])) echo $_POST['username']; ?>" id="create_acc_username" class="form-control" value="" size="20" type="text">
          <?php if( !empty($errors) && !empty($errors[0]) ) echo $eth . $errors[0] . $etf; ?>
        </div>

        <div class="form-group">
          <label for="create_acc_email" class="upper-blue">EMAIL<span class="required" aria-required="true">*</span>:</label>
          <input name="email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>" id="create_acc_email" class="form-control" value="" size="20" type="text">
          <?php if( !empty($errors) && !empty($errors[1]) ) echo $eth . $errors[1] . $etf; ?>
        </div>

        <div class="form-group">
          <label for="create_acc_country" class="upper-blue">COUNTRY<span class="required" aria-required="true">*</span>:</label>
            <select name='country' id='create_acc_country' class="form-control" value="">
              <option value="0">Select Country</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AF' ) echo 'selected = "selected"'; ?> value="AF">Afghanistan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AX' ) echo 'selected = "selected"'; ?> value="AX">Åland Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AL' ) echo 'selected = "selected"'; ?> value="AL">Albania</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'DZ' ) echo 'selected = "selected"'; ?> value="DZ">Algeria</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AS' ) echo 'selected = "selected"'; ?> value="AS">American Samoa</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AD' ) echo 'selected = "selected"'; ?> value="AD">Andorra</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AO' ) echo 'selected = "selected"'; ?> value="AO">Angola</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AI' ) echo 'selected = "selected"'; ?> value="AI">Anguilla</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AQ' ) echo 'selected = "selected"'; ?> value="AQ">Antarctica</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AG' ) echo 'selected = "selected"'; ?> value="AG">Antigua and Barbuda</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AR' ) echo 'selected = "selected"'; ?> value="AR">Argentina</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AM' ) echo 'selected = "selected"'; ?> value="AM">Armenia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AW' ) echo 'selected = "selected"'; ?> value="AW">Aruba</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AU' ) echo 'selected = "selected"'; ?> value="AU">Australia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AT' ) echo 'selected = "selected"'; ?> value="AT">Austria</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AZ' ) echo 'selected = "selected"'; ?> value="AZ">Azerbaijan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BS' ) echo 'selected = "selected"'; ?> value="BS">Bahamas</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BH' ) echo 'selected = "selected"'; ?> value="BH">Bahrain</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BD' ) echo 'selected = "selected"'; ?> value="BD">Bangladesh</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BB' ) echo 'selected = "selected"'; ?> value="BB">Barbados</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BY' ) echo 'selected = "selected"'; ?> value="BY">Belarus</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BE' ) echo 'selected = "selected"'; ?> value="BE">Belgium</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BZ' ) echo 'selected = "selected"'; ?> value="BZ">Belize</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BJ' ) echo 'selected = "selected"'; ?> value="BJ">Benin</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BM' ) echo 'selected = "selected"'; ?> value="BM">Bermuda</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BT' ) echo 'selected = "selected"'; ?> value="BT">Bhutan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BO' ) echo 'selected = "selected"'; ?> value="BO">Bolivia, Plurinational State of</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BQ' ) echo 'selected = "selected"'; ?> value="BQ">Bonaire, Sint Eustatius and Saba</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BA' ) echo 'selected = "selected"'; ?> value="BA">Bosnia and Herzegovina</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BW' ) echo 'selected = "selected"'; ?> value="BW">Botswana</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BV' ) echo 'selected = "selected"'; ?> value="BV">Bouvet Island</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BR' ) echo 'selected = "selected"'; ?> value="BR">Brazil</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'IO' ) echo 'selected = "selected"'; ?> value="IO">British Indian Ocean Territory</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BN' ) echo 'selected = "selected"'; ?> value="BN">Brunei Darussalam</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BG' ) echo 'selected = "selected"'; ?> value="BG">Bulgaria</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BF' ) echo 'selected = "selected"'; ?> value="BF">Burkina Faso</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BI' ) echo 'selected = "selected"'; ?> value="BI">Burundi</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KH' ) echo 'selected = "selected"'; ?> value="KH">Cambodia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CM' ) echo 'selected = "selected"'; ?> value="CM">Cameroon</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CA' ) echo 'selected = "selected"'; ?> value="CA">Canada</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CV' ) echo 'selected = "selected"'; ?> value="CV">Cape Verde</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KY' ) echo 'selected = "selected"'; ?> value="KY">Cayman Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CF' ) echo 'selected = "selected"'; ?> value="CF">Central African Republic</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TD' ) echo 'selected = "selected"'; ?> value="TD">Chad</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CL' ) echo 'selected = "selected"'; ?> value="CL">Chile</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CN' ) echo 'selected = "selected"'; ?> value="CN">China</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CX' ) echo 'selected = "selected"'; ?> value="CX">Christmas Island</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CC' ) echo 'selected = "selected"'; ?> value="CC">Cocos (Keeling) Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CO' ) echo 'selected = "selected"'; ?> value="CO">Colombia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KM' ) echo 'selected = "selected"'; ?> value="KM">Comoros</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CG' ) echo 'selected = "selected"'; ?> value="CG">Congo</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CD' ) echo 'selected = "selected"'; ?> value="CD">Congo, the Democratic Republic of the</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CK' ) echo 'selected = "selected"'; ?> value="CK">Cook Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CR' ) echo 'selected = "selected"'; ?> value="CR">Costa Rica</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CI' ) echo 'selected = "selected"'; ?> value="CI">Côte d'Ivoire</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'HR' ) echo 'selected = "selected"'; ?> value="HR">Croatia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CU' ) echo 'selected = "selected"'; ?> value="CU">Cuba</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CW' ) echo 'selected = "selected"'; ?> value="CW">Curaçao</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CY' ) echo 'selected = "selected"'; ?> value="CY">Cyprus</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CZ' ) echo 'selected = "selected"'; ?> value="CZ">Czech Republic</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'DK' ) echo 'selected = "selected"'; ?> value="DK">Denmark</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'DJ' ) echo 'selected = "selected"'; ?> value="DJ">Djibouti</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'DM' ) echo 'selected = "selected"'; ?> value="DM">Dominica</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'DO' ) echo 'selected = "selected"'; ?> value="DO">Dominican Republic</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'EC' ) echo 'selected = "selected"'; ?> value="EC">Ecuador</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'EG' ) echo 'selected = "selected"'; ?> value="EG">Egypt</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SV' ) echo 'selected = "selected"'; ?> value="SV">El Salvador</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GQ' ) echo 'selected = "selected"'; ?> value="GQ">Equatorial Guinea</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ER' ) echo 'selected = "selected"'; ?> value="ER">Eritrea</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'EE' ) echo 'selected = "selected"'; ?> value="EE">Estonia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ET' ) echo 'selected = "selected"'; ?> value="ET">Ethiopia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'FK' ) echo 'selected = "selected"'; ?> value="FK">Falkland Islands (Malvinas)</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'FO' ) echo 'selected = "selected"'; ?> value="FO">Faroe Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'FJ' ) echo 'selected = "selected"'; ?> value="FJ">Fiji</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'FI' ) echo 'selected = "selected"'; ?> value="FI">Finland</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'FR' ) echo 'selected = "selected"'; ?> value="FR">France</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GF' ) echo 'selected = "selected"'; ?> value="GF">French Guiana</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PF' ) echo 'selected = "selected"'; ?> value="PF">French Polynesia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TF' ) echo 'selected = "selected"'; ?> value="TF">French Southern Territories</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GA' ) echo 'selected = "selected"'; ?> value="GA">Gabon</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GM' ) echo 'selected = "selected"'; ?> value="GM">Gambia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GE' ) echo 'selected = "selected"'; ?> value="GE">Georgia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'DE' ) echo 'selected = "selected"'; ?> value="DE">Germany</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GH' ) echo 'selected = "selected"'; ?> value="GH">Ghana</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GI' ) echo 'selected = "selected"'; ?> value="GI">Gibraltar</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GR' ) echo 'selected = "selected"'; ?> value="GR">Greece</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GL' ) echo 'selected = "selected"'; ?> value="GL">Greenland</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GD' ) echo 'selected = "selected"'; ?> value="GD">Grenada</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GP' ) echo 'selected = "selected"'; ?> value="GP">Guadeloupe</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GU' ) echo 'selected = "selected"'; ?> value="GU">Guam</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GT' ) echo 'selected = "selected"'; ?> value="GT">Guatemala</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GG' ) echo 'selected = "selected"'; ?> value="GG">Guernsey</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GN' ) echo 'selected = "selected"'; ?> value="GN">Guinea</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GW' ) echo 'selected = "selected"'; ?> value="GW">Guinea-Bissau</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GY' ) echo 'selected = "selected"'; ?> value="GY">Guyana</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'HT' ) echo 'selected = "selected"'; ?> value="HT">Haiti</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'HM' ) echo 'selected = "selected"'; ?> value="HM">Heard Island and McDonald Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'VA' ) echo 'selected = "selected"'; ?> value="VA">Holy See (Vatican City State)</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'HN' ) echo 'selected = "selected"'; ?> value="HN">Honduras</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'HK' ) echo 'selected = "selected"'; ?> value="HK">Hong Kong</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'HU' ) echo 'selected = "selected"'; ?> value="HU">Hungary</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'IS' ) echo 'selected = "selected"'; ?> value="IS">Iceland</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'IN' ) echo 'selected = "selected"'; ?> value="IN">India</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ID' ) echo 'selected = "selected"'; ?> value="ID">Indonesia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'IR' ) echo 'selected = "selected"'; ?> value="IR">Iran, Islamic Republic of</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'IQ' ) echo 'selected = "selected"'; ?> value="IQ">Iraq</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'IE' ) echo 'selected = "selected"'; ?> value="IE">Ireland</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'IM' ) echo 'selected = "selected"'; ?> value="IM">Isle of Man</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'IL' ) echo 'selected = "selected"'; ?> value="IL">Israel</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'IT' ) echo 'selected = "selected"'; ?> value="IT">Italy</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'JM' ) echo 'selected = "selected"'; ?> value="JM">Jamaica</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'JP' ) echo 'selected = "selected"'; ?> value="JP">Japan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'JE' ) echo 'selected = "selected"'; ?> value="JE">Jersey</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'JO' ) echo 'selected = "selected"'; ?> value="JO">Jordan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KZ' ) echo 'selected = "selected"'; ?> value="KZ">Kazakhstan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KE' ) echo 'selected = "selected"'; ?> value="KE">Kenya</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KI' ) echo 'selected = "selected"'; ?> value="KI">Kiribati</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KP' ) echo 'selected = "selected"'; ?> value="KP">Korea, Democratic People's Republic of</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KR' ) echo 'selected = "selected"'; ?> value="KR">Korea, Republic of</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KW' ) echo 'selected = "selected"'; ?> value="KW">Kuwait</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KG' ) echo 'selected = "selected"'; ?> value="KG">Kyrgyzstan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LA' ) echo 'selected = "selected"'; ?> value="LA">Lao People's Democratic Republic</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LV' ) echo 'selected = "selected"'; ?> value="LV">Latvia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LB' ) echo 'selected = "selected"'; ?> value="LB">Lebanon</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LS' ) echo 'selected = "selected"'; ?> value="LS">Lesotho</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LR' ) echo 'selected = "selected"'; ?> value="LR">Liberia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LY' ) echo 'selected = "selected"'; ?> value="LY">Libya</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LI' ) echo 'selected = "selected"'; ?> value="LI">Liechtenstein</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LT' ) echo 'selected = "selected"'; ?> value="LT">Lithuania</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LU' ) echo 'selected = "selected"'; ?> value="LU">Luxembourg</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MO' ) echo 'selected = "selected"'; ?> value="MO">Macao</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MK' ) echo 'selected = "selected"'; ?> value="MK">Macedonia, the former Yugoslav Republic of</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MG' ) echo 'selected = "selected"'; ?> value="MG">Madagascar</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MW' ) echo 'selected = "selected"'; ?> value="MW">Malawi</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MY' ) echo 'selected = "selected"'; ?> value="MY">Malaysia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MV' ) echo 'selected = "selected"'; ?> value="MV">Maldives</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ML' ) echo 'selected = "selected"'; ?> value="ML">Mali</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MT' ) echo 'selected = "selected"'; ?> value="MT">Malta</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MH' ) echo 'selected = "selected"'; ?> value="MH">Marshall Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MQ' ) echo 'selected = "selected"'; ?> value="MQ">Martinique</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MR' ) echo 'selected = "selected"'; ?> value="MR">Mauritania</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MU' ) echo 'selected = "selected"'; ?> value="MU">Mauritius</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'YT' ) echo 'selected = "selected"'; ?> value="YT">Mayotte</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MX' ) echo 'selected = "selected"'; ?> value="MX">Mexico</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'FM' ) echo 'selected = "selected"'; ?> value="FM">Micronesia, Federated States of</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MD' ) echo 'selected = "selected"'; ?> value="MD">Moldova, Republic of</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MC' ) echo 'selected = "selected"'; ?> value="MC">Monaco</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MN' ) echo 'selected = "selected"'; ?> value="MN">Mongolia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ME' ) echo 'selected = "selected"'; ?> value="ME">Montenegro</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MS' ) echo 'selected = "selected"'; ?> value="MS">Montserrat</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MA' ) echo 'selected = "selected"'; ?> value="MA">Morocco</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MZ' ) echo 'selected = "selected"'; ?> value="MZ">Mozambique</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MM' ) echo 'selected = "selected"'; ?> value="MM">Myanmar</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NA' ) echo 'selected = "selected"'; ?> value="NA">Namibia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NR' ) echo 'selected = "selected"'; ?> value="NR">Nauru</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NP' ) echo 'selected = "selected"'; ?> value="NP">Nepal</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NL' ) echo 'selected = "selected"'; ?> value="NL">Netherlands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NC' ) echo 'selected = "selected"'; ?> value="NC">New Caledonia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NZ' ) echo 'selected = "selected"'; ?> value="NZ">New Zealand</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NI' ) echo 'selected = "selected"'; ?> value="NI">Nicaragua</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NE' ) echo 'selected = "selected"'; ?> value="NE">Niger</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NG' ) echo 'selected = "selected"'; ?> value="NG">Nigeria</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NU' ) echo 'selected = "selected"'; ?> value="NU">Niue</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NF' ) echo 'selected = "selected"'; ?> value="NF">Norfolk Island</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MP' ) echo 'selected = "selected"'; ?> value="MP">Northern Mariana Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'NO' ) echo 'selected = "selected"'; ?> value="NO">Norway</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'OM' ) echo 'selected = "selected"'; ?> value="OM">Oman</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PK' ) echo 'selected = "selected"'; ?> value="PK">Pakistan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PW' ) echo 'selected = "selected"'; ?> value="PW">Palau</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PS' ) echo 'selected = "selected"'; ?> value="PS">Palestinian Territory, Occupied</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PA' ) echo 'selected = "selected"'; ?> value="PA">Panama</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PG' ) echo 'selected = "selected"'; ?> value="PG">Papua New Guinea</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PY' ) echo 'selected = "selected"'; ?> value="PY">Paraguay</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PE' ) echo 'selected = "selected"'; ?> value="PE">Peru</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PH' ) echo 'selected = "selected"'; ?> value="PH">Philippines</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PN' ) echo 'selected = "selected"'; ?> value="PN">Pitcairn</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PL' ) echo 'selected = "selected"'; ?> value="PL">Poland</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PT' ) echo 'selected = "selected"'; ?> value="PT">Portugal</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PR' ) echo 'selected = "selected"'; ?> value="PR">Puerto Rico</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'QA' ) echo 'selected = "selected"'; ?> value="QA">Qatar</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'RE' ) echo 'selected = "selected"'; ?> value="RE">Réunion</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'RO' ) echo 'selected = "selected"'; ?> value="RO">Romania</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'RU' ) echo 'selected = "selected"'; ?> value="RU">Russian Federation</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'RW' ) echo 'selected = "selected"'; ?> value="RW">Rwanda</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'BL' ) echo 'selected = "selected"'; ?> value="BL">Saint Barthélemy</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SH' ) echo 'selected = "selected"'; ?> value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'KN' ) echo 'selected = "selected"'; ?> value="KN">Saint Kitts and Nevis</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LC' ) echo 'selected = "selected"'; ?> value="LC">Saint Lucia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'MF' ) echo 'selected = "selected"'; ?> value="MF">Saint Martin (French part)</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'PM' ) echo 'selected = "selected"'; ?> value="PM">Saint Pierre and Miquelon</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'VC' ) echo 'selected = "selected"'; ?> value="VC">Saint Vincent and the Grenadines</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'WS' ) echo 'selected = "selected"'; ?> value="WS">Samoa</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SM' ) echo 'selected = "selected"'; ?> value="SM">San Marino</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ST' ) echo 'selected = "selected"'; ?> value="ST">Sao Tome and Principe</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SA' ) echo 'selected = "selected"'; ?> value="SA">Saudi Arabia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SN' ) echo 'selected = "selected"'; ?> value="SN">Senegal</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'RS' ) echo 'selected = "selected"'; ?> value="RS">Serbia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SC' ) echo 'selected = "selected"'; ?> value="SC">Seychelles</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SL' ) echo 'selected = "selected"'; ?> value="SL">Sierra Leone</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SG' ) echo 'selected = "selected"'; ?> value="SG">Singapore</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SX' ) echo 'selected = "selected"'; ?> value="SX">Sint Maarten (Dutch part)</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SK' ) echo 'selected = "selected"'; ?> value="SK">Slovakia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SI' ) echo 'selected = "selected"'; ?> value="SI">Slovenia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SB' ) echo 'selected = "selected"'; ?> value="SB">Solomon Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SO' ) echo 'selected = "selected"'; ?> value="SO">Somalia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ZA' ) echo 'selected = "selected"'; ?> value="ZA">South Africa</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GS' ) echo 'selected = "selected"'; ?> value="GS">South Georgia and the South Sandwich Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SS' ) echo 'selected = "selected"'; ?> value="SS">South Sudan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ES' ) echo 'selected = "selected"'; ?> value="ES">Spain</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'LK' ) echo 'selected = "selected"'; ?> value="LK">Sri Lanka</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SD' ) echo 'selected = "selected"'; ?> value="SD">Sudan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SR' ) echo 'selected = "selected"'; ?> value="SR">Suriname</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SJ' ) echo 'selected = "selected"'; ?> value="SJ">Svalbard and Jan Mayen</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SZ' ) echo 'selected = "selected"'; ?> value="SZ">Swaziland</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SE' ) echo 'selected = "selected"'; ?> value="SE">Sweden</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'CH' ) echo 'selected = "selected"'; ?> value="CH">Switzerland</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'SY' ) echo 'selected = "selected"'; ?> value="SY">Syrian Arab Republic</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TW' ) echo 'selected = "selected"'; ?> value="TW">Taiwan, Province of China</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TJ' ) echo 'selected = "selected"'; ?> value="TJ">Tajikistan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TZ' ) echo 'selected = "selected"'; ?> value="TZ">Tanzania, United Republic of</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TH' ) echo 'selected = "selected"'; ?> value="TH">Thailand</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TL' ) echo 'selected = "selected"'; ?> value="TL">Timor-Leste</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TG' ) echo 'selected = "selected"'; ?> value="TG">Togo</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TK' ) echo 'selected = "selected"'; ?> value="TK">Tokelau</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TO' ) echo 'selected = "selected"'; ?> value="TO">Tonga</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TT' ) echo 'selected = "selected"'; ?> value="TT">Trinidad and Tobago</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TN' ) echo 'selected = "selected"'; ?> value="TN">Tunisia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TR' ) echo 'selected = "selected"'; ?> value="TR">Turkey</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TM' ) echo 'selected = "selected"'; ?> value="TM">Turkmenistan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TC' ) echo 'selected = "selected"'; ?> value="TC">Turks and Caicos Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'TV' ) echo 'selected = "selected"'; ?> value="TV">Tuvalu</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'UG' ) echo 'selected = "selected"'; ?> value="UG">Uganda</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'UA' ) echo 'selected = "selected"'; ?> value="UA">Ukraine</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'AE' ) echo 'selected = "selected"'; ?> value="AE">United Arab Emirates</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'GB' ) echo 'selected = "selected"'; ?> value="GB">United Kingdom</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'US' ) echo 'selected = "selected"'; ?> value="US">United States</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'UM' ) echo 'selected = "selected"'; ?> value="UM">United States Minor Outlying Islands</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'UY' ) echo 'selected = "selected"'; ?> value="UY">Uruguay</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'UZ' ) echo 'selected = "selected"'; ?> value="UZ">Uzbekistan</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'VU' ) echo 'selected = "selected"'; ?> value="VU">Vanuatu</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'VE' ) echo 'selected = "selected"'; ?> value="VE">Venezuela, Bolivarian Republic of</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'VN' ) echo 'selected = "selected"'; ?> value="VN">Vietnam</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'VG' ) echo 'selected = "selected"'; ?> value="VG">Virgin Islands, British</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'VI' ) echo 'selected = "selected"'; ?> value="VI">Virgin Islands, U.S.</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'WF' ) echo 'selected = "selected"'; ?> value="WF">Wallis and Futuna</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'EH' ) echo 'selected = "selected"'; ?> value="EH">Western Sahara</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'YE' ) echo 'selected = "selected"'; ?> value="YE">Yemen</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ZM' ) echo 'selected = "selected"'; ?> value="ZM">Zambia</option>
              <option <?php if(isset($_POST['country']) && $_POST['country'] == 'ZW' ) echo 'selected = "selected"'; ?> value="ZW">Zimbabwe</option>
            </select>
          <?php if( !empty($errors) && !empty($errors[8]) ) echo $eth . $errors[8] . $etf; ?>
        </div>


        <div class="form-group">
          <label for="create_acc_password" class="upper-blue">PASSWORD<span class="required" aria-required="true">*</span>:</label>
          <input name="pass1" value="" id="create_acc_password" class="form-control" value="" size="20" type="password">
          <?php if( !empty($errors) && !empty($errors[2]) ) echo $eth . $errors[2] . $etf; ?>
        </div>

        <div class="form-group">
          <label for="create_acc_password_2" class="upper-blue">CONFIRM PASSWORD<span class="required" aria-required="true">*</span>:</label>
          <input name="pass2" value="" id="create_acc_password_2" class="form-control" value="" size="20" type="password">
          <?php if( !empty($errors) && !empty($errors[3]) ) echo $eth . $errors[3] . $etf; ?>
        </div>

        <div class="form-group">
          <label for="create_acc_serial_number" class="upper-blue">SERIAL NUMBER<span class="required" aria-required="true">*</span>:</label>
          <input name="serial" value="<?php if(isset($_POST['serial'])) echo $_POST['serial']; ?>" id="create_acc_serial_number" class="form-control" value="" size="20" type="text">
         <?php if( !empty($errors) && !empty($errors[7]) ) echo $eth . $errors[7] . $etf; ?>
        </div>

        <p><a href="https://www.genmarkdx.com/support/contact/"> Contact customer service</a> if you are having trouble locating your instrument serial number.</p>

        <input name="wp-submit" id="wp-submit--popover" class="btn btn-default" value="<?php echo strtoupper( __('Create Account','genmark')); ?>" type="submit">
        <input name="redirect_to" value="http://localhost/genmark/customer-resource-center/" type="hidden">

      </form>

      <?php
        if ( WP_ENV == 'production' ) {
          ?>
            <script type="text/javascript">llfrmid=22309</script>
            <script type="text/javascript" src="https://formalyzer.com/formalyze_init.js"></script>
            <script type="text/javascript" src="https://formalyzer.com/formalyze_call_secure.js"></script>
          <?php
        }
      ?>

      <div id="serialKeyModal" class="modal--serial-key modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header clearfix">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close ×</span></button>
            </div>

            <?php /*
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            */ ?>
            <div class="modal-body">
              <p><span class="upper-blue">Why do we ask for your instrument serial number?</span>
              <br>By entering your serial number, you will be granted immediate access to the Customer Resource Center.  If you do not have a serial number, you can still create an account, however your access will need to be verified by the Customer Service Team before access can be granted.</p>
            </div>

          </div>

        </div>
      </div>


    </div>
  </div>
</div>


<?php get_footer(); ?>
