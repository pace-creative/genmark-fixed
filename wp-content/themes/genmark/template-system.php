<?php /* Template Name: System */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <?php
        $video = get_field('video');
        $intro_text = get_field('intro_text');
        $pdf = get_field('product_literature');
        $thumb = false;

        if( has_post_thumbnail() ) {
          $thumb = get_the_post_thumbnail();
        }
      ?>

      <?php if( !empty( $video ) || !empty( $pdf ) || $intro_text || $thumb ): ?>
        <div class="media system-header">
          <?php if( $thumb ): ?>
          <div class="media-left">
            <?php echo $thumb; ?>
          </div>
          <?php endif; ?>

          <div class="media-body">

            <?php if( $intro_text ): ?>
              <p class="system-header__text">
                <?php echo $intro_text; ?>
              </p>
            <?php endif; ?>

            <?php if( !empty( $video ) || !empty( $pdf )): ?>

              <div id="video-modal-<?php the_ID(); ?>" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-video" role="document">
                  <div class="modal-content">
                    <div class="modal-header clearfix">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="pauseModalVideo('system_video_<?php the_ID(); ?>')"><span aria-hidden="true">Close Video &times;</span></button>
                    </div>
                    <div class="modal-body clearfix">
                      <video id="system_video_<?php the_ID(); ?>" class="video-js vjs-default-skin vjs-big-play-centered"
                        controls preload="auto" width="100%" height="450"
                        data-setup='{}'>
                        <source src="<?php echo $video['url']; ?>" type='video/mp4' />
                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                      </video>
                    </div>
                  </div>
                </div>
              </div>

              <p>

                <?php if( !empty( $video )): ?>
                  <a href="#" target="_blank" data-toggle="modal" data-target="#video-modal-<?php the_ID(); ?>" class="btn btn-default fa-icon fa-icon--play">Watch the ePlex video</a>
                <?php endif; ?>

                <?php if( !empty( $pdf )): ?>
                  <a href="<?php echo $pdf['url']; ?>" target="_blank" class="btn btn-default fa-icon fa-icon--pdf">Product Literature</a>
                <?php endif; ?>

              </p>
            <?php endif; ?>

          </div>
        </div>
      <?php endif; ?>


      <?php the_content(); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>