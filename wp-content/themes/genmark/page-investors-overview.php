<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <div class="row stock-info">
        <div class="col-xs-12">
          <h2 class="stock-info__title">GenMark Diagnostics (NASDAQ: GNMK)
          <br><small class="stock-info__date">2:55 PM ET on May 5, 2015</small></h2>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-4">

          <div class="stock-info__value">$9.78</div>
          <div class="stock-info__change up">
            <span>+$0.41</span>&nbsp;&nbsp;
            <span>+4.024%</span>
          </div>

        </div>
        <div class="col-xs-12 col-sm-4">

          <table class="table table-striped table--stock-info">
            <tr>
              <th>Previous Close</th>
              <td>10.19</td>
            </tr>
            <tr>
              <th>Open</th>
              <td>10.12</td>
            </tr>
            <tr>
              <th>Volume</th>
              <td>132,500</td>
            </tr>
            <tr>
              <th>Exchange</th>
              <td>NASDAQ</td>
            </tr>
          </table>

        </div>
        <div class="col-xs-12 col-sm-4">

          <table class="table table-striped table--stock-info">
            <tr>
              <th>Day High</th>
              <td>10.19</td>
            </tr>
            <tr>
              <th>Day Low</th>
              <td>9.75</td>
            </tr>
            <tr>
              <th>52 Week High</th>
              <td>14.40</td>
            </tr>
            <tr>
              <th>52 Week Low</th>
              <td>8.48</td>
            </tr>
          </table>

        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <a href="#" class="btn btn-default">More Stock Information</a>
          <a href="#" class="btn btn-default fa-icon fa-icon--pdf">FAQs</a>
        </div>
      </div>


      <div class="row">

        <div class="col-xs-12 col-sm-4 over-box">
          <h3 class="over-box__heading">Reports and Filings</h3>
          <div class="over-box__content">
            <p>
              <a href="#" class="btn btn-default fa-icon fa-icon--external">2014 Proxy Report</a>
            </p>
            <p>
              <a href="#" class="btn btn-default fa-icon fa-icon--external">2013 Annual Report</a>
            </p>
          </div>
          <a class="over-box__read-more" href="#">View More</a>
        </div>

        <div class="col-xs-12 col-sm-4 over-box">
          <h3 class="over-box__heading">Press Releases</h3>
          <div class="over-box__content">

            <div class="over-box__item">
              <span class="over-box__date">May 5, 2015</span>
              <h4 class="over-box__title"><a href="#">GenMark Diagnostics Reports Q1 Financial Results</a></h4>
            </div>

          </div>
          <a class="over-box__read-more" href="#">View More</a>
        </div>

        <div class="col-xs-12 col-sm-4 over-box">
          <h3 class="over-box__heading">Presentations</h3>
          <div class="over-box__content">

            <div class="over-box__item">
              <span class="over-box__date">May 5, 2015 - 3:00PM</span>
              <h4 class="over-box__title"><a href="#">Bank of America Merrill Lynch 2015 Health Care Conference</a></h4>
            </div>

          </div>
          <a class="over-box__read-more" href="#">View More</a>
        </div>

      </div>

      <div class="row">

        <div class="col-xs-12 col-sm-4 over-box">
          <h3 class="over-box__heading">Events</h3>
          <div class="over-box__content">

            <div class="over-box__item">
              <span class="over-box__date">May 5, 2015 - 3:00PM</span>
              <h4 class="over-box__title"><a href="#">Bank of America Merrill Lynch 2015 Health Care Conference</a></h4>
            </div>

          </div>
          <a class="over-box__read-more" href="#">View More</a>
        </div>

        <div class="col-xs-12 col-sm-4 over-box">
          <h3 class="over-box__heading">Corporate Governance</h3>
          <div class="over-box__content">

            <p>
              <a href="#" class="btn btn-default fa-icon fa-icon--pdf">Insider Trading Policy</a>
            </p>
            <p>
              <a href="#" class="btn btn-default fa-icon fa-icon--pdf">Audit Committee Charter</a>
            </p>

          </div>
          <a class="over-box__read-more" href="#">View More</a>
        </div>

        <div class="col-xs-12 col-sm-4 over-box">
          <h3 class="over-box__heading">Email Alerts</h3>

          <div class="over-box__content">
            <p>
              Automatically receive GenMark Dx financial information by email.
            </p>
          </div>
          <a class="over-box__read-more" href="#">Learn More</a>
        </div>

      </div>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>