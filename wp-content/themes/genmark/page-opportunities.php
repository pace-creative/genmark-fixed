<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <p align="center">
        <a href="adhocjobsearch.asp">Job Search</a>&nbsp;&nbsp;
        <a href="process_jobsearch.asp">View All Jobs</a>&nbsp;&nbsp;
        <a href="membership.asp">Create an Account</a>&nbsp;&nbsp;
        <a href="memberlogin.asp">Candidate Login</a>&nbsp;&nbsp;
      </p>

      <!-- <div class="table-responsive"> -->
        <table class="item-list" summary="Sortable Table (Click a column header to sort)">
          <thead>
            <tr>
              <th><a class="sortable desc" href="#">Job Title <abbr style="border-style: none;" class="sort-icon" title="(sorted descending)"><span class="caret"></span></abbr></a></th>
              <th><a class="sortable" href="#">Location <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
              <th>Links</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <h2>Controls Engineer</h2>
                <p>Company Mission GenMark Dx’s mission is to become the market leader in providing high value, simple to perform, clinically relevant multiplexed molecular tests to aid in the diagnosis of disease and the selection and dosing of therapies.</p>
              </td>
              <td width="15%">
                <p>Carlsbad, CA</p>
              </td>
              <td>
                <a class="btn btn-default" href="#">View More</a>
              </td>
            </tr>
            <tr>
              <td>
                <h2>Document Control Specialist</h2>
                <p>Company Mission GenMark Dx’s mission is to become the market leader in providing high value, simple to perform, clinically relevant multiplexed molecular tests to aid in the diagnosis of disease and the selection and dosing of therapies.</p>
              </td>
              <td>
                <p>Carlsbad, CA</p>
              </td>
              <td>
                <a class="btn btn-default" href="#">View More</a>
              </td>
            </tr>
            <tr>
              <td>
                <h2>Electrical Engineer</h2>
                <p>Company Mission GenMark Dx’s mission is to become the market leader in providing high value, simple to perform, clinically relevant multiplexed molecular tests to aid in the diagnosis of disease and the selection and dosing of therapies.</p>
              </td>
              <td>
                <p>Carlsbad, CA</p>
              </td>
              <td>
                <a class="btn btn-default" href="#">View More</a>
              </td>
            </tr>
            <tr>
              <td>
                <h2>Electromechanical Technician</h2>
                <p>Company Mission GenMark Dx’s mission is to become the market leader in providing high value, simple to perform, clinically relevant multiplexed molecular tests to aid in the diagnosis of disease and the selection and dosing of therapies.</p>
              </td>
              <td>
                <p>Carlsbad, CA</p>
              </td>
              <td>
                <a class="btn btn-default" href="#">View More</a>
              </td>
            </tr>
          </tbody>
        </table>
      <!-- </div> -->

      <p>
        <span class="note">For more information, please contact <a href="mailto:careers@genmarkdx.com">careers@genmarkdx.com</a></span>
      </p>

      <ul class="pagination">
        <li class="pagination__item"><a href="#" class="btn btn-default">1</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">2</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">3</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">4</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">></a></li>
      </ul>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>