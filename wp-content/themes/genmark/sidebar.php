<div class="col-xs-3 hidden-xs hidden-sm">
  <aside id="sidebar">
    <?php
      // find top parent
      global $post;
      $cur = $post;

      while( $cur->post_parent != 0 ) {

        $cur = get_post( $cur->post_parent );

      }

      // get children
      $args = array(
        'post_parent' => $cur->ID,
        'post_type' => 'page',
        'numberposts' => -1,
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC',
      );

      $children = get_children_recursive( $args );

    ?>

    <?php if( !empty( $children )): ?>
    <?php echo children2list( $children, array('maxdepth' => 3) ); ?>
    <?php endif; ?>

    <?php
      $boxes = get_field( 'sidebar_boxes' );

      if( !empty( $boxes )):
      ?>

        <?php if( in_array( 'stock', $boxes )): ?>
        <div class="cta cta--stock">
          <h2 class="cta__title">Stock Quote</h2>
          <table>
            <tr>
              <td width="70%">Price:</td>
              <td>10.19</td>
            </tr>
            <tr>
              <td>Change:</td>
              <td><span class="up">+0.08</span></td>
            </tr>
            <tr>
              <td>Day High:</td>
              <td>10.45</td>
            </tr>
            <tr>
              <td>Day Low:</td>
              <td>10.00</td>
            </tr>
            <tr>
              <td>Volume:</td>
              <td>289,600</td>
            </tr>
            <tr>
              <td>NASDAQ</td>
              <td><strong>GNMK</strong></td>
            </tr>
          </table>
          <p>
            4:00 PM ET on May 4, 2015
          </p>
          <p class="small">
            Delayed at least 20 minutes. Provided by eSignal.
          </p>
          <p class="text-center">
            <a href="#" class="btn btn-default"><?php echo strtoupper( __('View More','genmark')); ?></a>
          </p>
        </div>
        <?php endif; ?>

        <?php if( in_array( 'tech-support', $boxes )): ?>
        <div class="cta">
          <h2 class="cta__title-small">
            <span class="glyphicon glyphicon-earphone cta__icon" aria-hidden="true"></span>
            Technical Support
          </h2>
          <p class="text-left">
            <small>
              <?php if( ICL_LANGUAGE_CODE == 'int' ) { ?>
                GenMark Technical Support is available Monday-Friday 8h-18 CET
              <?php } else { ?>
                GenMark Technical Support is available 24 hrs a day, 7 days a week.
              <?php } ?>
            </small></p>
            <?php if( ICL_LANGUAGE_CODE == 'int' ) { ?>

                <table cellpadding="0" cellspacing="0" class="nopad">
                  <tr>
                    <td style="text-align: left; padding: 0; font-size: 13px;">Austria</td>
                    <td style="text-align: left; padding: 0; font-size: 13px;">+43 1 253 1796</td>
                  <tr>
                    <td style="text-align: left; padding: 0; font-size: 13px;">Belgium</td>
                    <td style="text-align: left; padding: 0; font-size: 13px;">+32 2 793 0361</td>
                  <tr>
                    <td style="text-align: left; padding: 0; font-size: 13px;">France</td>
                    <td style="text-align: left; padding: 0; font-size: 13px;">+33 1 8565 0898</td>
                  <tr>
                    <td style="text-align: left; padding: 0; font-size: 13px;">Germany</td>
                    <td style="text-align: left; padding: 0; font-size: 13px;">+49 89 2620 7487</td>
                  <tr>
                    <td style="text-align: left; padding: 0; font-size: 13px;">Netherlands</td>
                    <td style="text-align: left; padding: 0; font-size: 13px;">+31 71 799 9100</td>
                  <tr>
                    <td style="text-align: left; padding: 0; font-size: 13px;">Switzerland</td>
                    <td style="text-align: left; padding: 0; font-size: 13px;">+41 43 215 5811</td>
                  <tr>
                    <td style="text-align: left; padding: 0; font-size: 13px;">United Kingdom</td>
                    <td style="text-align: left; padding: 0; font-size: 13px;">+44 20 3790 7352</td>
                </table>

              <!-- <p class="cta__phone" style="font-size: 13px; text-align: left;">
              </p> -->
            <?php } else { ?>
              <p class="cta__phone">
                <span class="glyphicon glyphicon-earphone cta__icon" aria-hidden="true"></span>
                <?php _e('1.800.373.6767','genmark'); ?>
              </p>
            <?php } ?>
          <a href="mailto:<?php _e('TechnicalSupport@genmarkdx.com','genmark'); ?>" class="btn btn-default"><?php echo strtoupper( __('Email Technical Support','genmark')); ?></a>
        </div>
        <?php endif; ?>

        <?php if( in_array( 'questions', $boxes )): ?>
        <div class="cta">
          <h2 class="cta__title">Questions?</h2>
          <a href="<?php echo get_home_url(); ?>/support/request-information/" class="btn btn-default"><?php echo strtoupper( __('Request Information','genmark')); ?></a>
        </div>
        <?php endif; ?>

        <?php if( in_array( 'join', $boxes )): ?>
        <div class="cta">
          <h2 class="cta__title">Join Our Team</h2>
          <a href="<?php echo get_home_url(); ?>/careers/opportunities/" class="btn btn-default"><?php echo strtoupper( __('View Opportunities','genmark')); ?></a>
        </div>
        <?php endif; ?>

      <?php endif; ?>
  </aside>
</div>
