<?php
  if( have_posts() ) {
    the_post();
  }
  global $post;
  $post->post_parent = apply_filters( 'wpml_object_id', 78, 'page' ); // Set the parent to the people page manually

?>
<?php get_header(); ?>

<?php
  $desig = get_field('designation');
  $title = get_field('title');
?>
<div class="page-section page-section--wide">
  <div class="container">

    <div class="row">

      <?php get_sidebar(); ?>

      <div class="col-xs-12 col-md-9">


        <div class="page-section__content">


          <h1><?php the_title(); ?></h1>
          <h4 class="media-heading upper-blue">
            <?php if( !empty( $desig )): ?>, <?php echo $desig; ?>
            <?php endif; ?>
          </h4>
          <h4 class="media-subheading team-member__title"><i><?php echo $title; ?></i></h4>


          <hr>

          <?php the_content(); ?>

        </div><!-- .page-section__content -->

      </div><!-- .col-xs-12 -->

    </div><!-- .row -->

  </div><!-- .container -->
</div><!-- .page-section -->

<?php get_footer(); ?>



   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->