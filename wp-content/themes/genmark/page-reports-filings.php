<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <h2>SEC Filings</h2>

      <div class="row">

        <div class="col-xs-12">
          <form action="" class="form-inline">
            <div class="form-group">
              <label for="id1">View: </label>
              <select class="form-control">
                <option value="">All Filings</option>
              </select>
            </div>
            <div class="form-group">
              <label for="id2">Year: </label>
              <select class="form-control">
                <option value="">All Years</option>
              </select>
            </div>
            <div class="form-group">
              <input class="btn btn-default" type="submit" value="Reset">
            </div>
          </form>
        </div>

      </div>

      <br>

      <table class="item-list" summary="Sortable Table (Click a column header to sort)">
        <thead>
          <tr>
            <th><a class="sortable desc" href="#">Date Filed <abbr style="border-style: none;" class="sort-icon" title="(sorted descending)"><span class="caret"></span></abbr></a></th>
            <th><a class="sortable" href="#">Description <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
            <th><a class="sortable" href="#">Filing <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
            <th>Download</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <td>
              <strong>Apr 16</strong>
            </td>
            <td>
              Definitive Additional Proxy Materials
            </td>
            <td>
              DEFA14A
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              <strong>Apr 16</strong>
            </td>
            <td>
              Definitive Proxy Statement
            </td>
            <td>
              DEF 14A
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              <strong>Apr 16</strong>
            </td>
            <td>
              Current Report
            </td>
            <td>
              8-K
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              <strong>Apr 10</strong>
            </td>
            <td>
              Massarany, Hany
            </td>
            <td>
              4
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              <strong>Apr 2</strong>
            </td>
            <td>
              Current Report
            </td>
            <td>
              8-K
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

        </tbody>
      </table>

      <ul class="pagination">
        <li class="pagination__item"><a href="#" class="btn btn-default">1</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">2</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">3</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">4</a></li>
        <li class="pagination__item"><a href="#" class="btn btn-default">></a></li>
      </ul>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>