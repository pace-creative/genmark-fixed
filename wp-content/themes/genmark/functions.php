<?php

/**
 * Includes
**/
include 'inc/template-tags.php';
include 'inc/nav-walker.php';
require_once 'inc/geoplugin.class.php';
require_once 'crc/crc.php';
include 'import/functions.php';
include 'inc/acf/functions.php';

$geoplugin = new geoPlugin();

define('DEFAULT_PREF_LANG_TIMEOUT', time()+(3600*24*5) ); // 5 days

// theme functionality
register_nav_menus( array(
  'main_nav'=>'Primary Navigation',
));
add_theme_support( 'post-thumbnails' );

if (!current_user_can('edit_posts')) {
//
  add_filter('show_admin_bar', '__return_false'); //was commented out
}

add_image_size( 'gallery-thumb', 74, 43, true );

// var dump with auto <pre>
function pvar_dump($a) { echo '<pre>'; var_dump($a); echo '</pre>'; }

function pace_add_query_vars($aVars) {

  //for sorting
  $aVars[] = "o";  //order
  $aVars[] = "ob"; //order_by

  //for login
  $aVars[] = "id"; //username
  $aVars[] = "a";  //action
  $aVars[] = "sn";  //serial-number

  // publications
  $aVars[] = 'disease-state';

  return $aVars;
}
add_filter('query_vars', 'pace_add_query_vars');

function pace_add_rewrite_rules($aRules) {
  $aNewRules = array(
    // 'investors/presentations/([^/]+)/?$' => 'index.php?pagename=investors/presentations&curyear=$matches[1]',
   );
  $aRules = $aNewRules + $aRules;
  return $aRules;
}
add_filter('rewrite_rules_array', 'pace_add_rewrite_rules');


// init hook
add_action( 'init', 'pace_init' );
function pace_init() {

  add_image_size( 'portrait-small', 150, 150, true );

  add_editor_style();
  add_editor_style('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
  add_editor_style('https://fontastic.s3.amazonaws.com/bbzHmTRRsE2R6YqX9nSWTi/icons.css');

  //custom post type - PEOPLE
  // add post type
  $labels = array(
    'name'               => _x( 'People', 'post type general name', 'pace' ),
    'singular_name'      => _x( 'Person', 'post type singular name', 'pace' ),
    'menu_name'          => _x( 'People', 'admin menu', 'pace' ),
    'name_admin_bar'     => _x( 'Person', 'add new on admin bar', 'pace' ),
    'add_new'            => _x( 'Add New', 'Person', 'pace' ),
    'add_new_item'       => __( 'Add New Person', 'pace' ),
    'new_item'           => __( 'New Person', 'pace' ),
    'edit_item'          => __( 'Edit Person', 'pace' ),
    'view_item'          => __( 'View Person', 'pace' ),
    'all_items'          => __( 'All People', 'pace' ),
    'search_items'       => __( 'Search People', 'pace' ),
    'parent_item_colon'  => __( 'Parent Person:', 'pace' ),
    'not_found'          => __( 'No properties found.', 'pace' ),
    'not_found_in_trash' => __( 'No properties found in Trash.', 'pace' )
  );

  $args = array(
    'labels' => $labels,
    'supports' => array(
      'title',
      'editor',
      'thumbnail',
    ),
    'public' => true,
    'has_archive' => true,
    'menu_icon' => 'dashicons-businessman',
    'menu_position' => 50,
    );

  register_post_type( 'people', $args );
  add_post_type_support( 'people', 'page-attributes' );
  //end custom post type - PEOPLE

  //custom taxonomy - PERSON
  $labels = array(
    'name'              => _x( 'Person Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Person Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Person Categories' ),
    'all_items'         => __( 'All Person Categories' ),
    'parent_item'       => __( 'Parent Person Category' ),
    'parent_item_colon' => __( 'Parent Person Category:' ),
    'edit_item'         => __( 'Edit Person Category' ),
    'update_item'       => __( 'Update Person Category' ),
    'add_new_item'      => __( 'Add New Person Category' ),
    'new_item_name'     => __( 'New Person Category Name' ),
    'menu_name'         => __( 'Person Category' ),
  );

  register_taxonomy(
    'person_category',
    'people',
    array(
      'labels' => $labels,
      'hierarchical' => true,
    )
  );
  //end custom taxonomy - PERSON

  //custom post type - CONFERENCE
  // add post type
  $labels = array(
    'name'               => _x( 'Conferences', 'post type general name', 'pace' ),
    'singular_name'      => _x( 'Conference', 'post type singular name', 'pace' ),
    'menu_name'          => _x( 'Conferences', 'admin menu', 'pace' ),
    'name_admin_bar'     => _x( 'Conference', 'add new on admin bar', 'pace' ),
    'add_new'            => _x( 'Add New', 'Conference', 'pace' ),
    'add_new_item'       => __( 'Add New Conference', 'pace' ),
    'new_item'           => __( 'New Conference', 'pace' ),
    'edit_item'          => __( 'Edit Conference', 'pace' ),
    'view_item'          => __( 'View Conference', 'pace' ),
    'all_items'          => __( 'All Conferences', 'pace' ),
    'search_items'       => __( 'Search Conferences', 'pace' ),
    'parent_item_colon'  => __( 'Parent Conference:', 'pace' ),
    'not_found'          => __( 'No properties found.', 'pace' ),
    'not_found_in_trash' => __( 'No properties found in Trash.', 'pace' )
  );

  $args = array(
    'labels' => $labels,
    'supports' => array(
      'title',
      //'editor',
      //'thumbnail',
    ),
    'public' => true,
    'has_archive' => true,
    'menu_icon' => 'dashicons-groups',
    'menu_position' => 50,
    );

  register_post_type( 'Conferences', $args );
  add_post_type_support( 'Conferences', 'page-attributes' );
  //end custom post type - CONFERENCE

  //custom post type - PUBLICATION
  // add post type
  $labels = array(
    'name'               => _x( 'Publications', 'post type general name', 'pace' ),
    'singular_name'      => _x( 'Publication', 'post type singular name', 'pace' ),
    'menu_name'          => _x( 'Publications', 'admin menu', 'pace' ),
    'name_admin_bar'     => _x( 'Publication', 'add new on admin bar', 'pace' ),
    'add_new'            => _x( 'Add New', 'Publication', 'pace' ),
    'add_new_item'       => __( 'Add New Publication', 'pace' ),
    'new_item'           => __( 'New Publication', 'pace' ),
    'edit_item'          => __( 'Edit Publication', 'pace' ),
    'view_item'          => __( 'View Publication', 'pace' ),
    'all_items'          => __( 'All Publications', 'pace' ),
    'search_items'       => __( 'Search Publications', 'pace' ),
    'parent_item_colon'  => __( 'Parent Publication:', 'pace' ),
    'not_found'          => __( 'No properties found.', 'pace' ),
    'not_found_in_trash' => __( 'No properties found in Trash.', 'pace' )
  );

  $args = array(
    'labels' => $labels,
    'supports' => array( 'title' ),
    'public' => true,
    'has_archive' => true,
    'menu_icon' => 'dashicons-media-text',
    'menu_position' => 50,
    );

  register_post_type( 'Publications', $args );
  add_post_type_support( 'Publications', 'page-attributes' );
  //end custom post type - PUBLICATION

  //custom post type - WEBINAR
  // add post type
  $labels = array(
    'name'               => _x( 'Webinars', 'post type general name', 'pace' ),
    'singular_name'      => _x( 'Webinar', 'post type singular name', 'pace' ),
    'menu_name'          => _x( 'Webinars', 'admin menu', 'pace' ),
    'name_admin_bar'     => _x( 'Webinar', 'add new on admin bar', 'pace' ),
    'add_new'            => _x( 'Add New', 'Webinar', 'pace' ),
    'add_new_item'       => __( 'Add New Webinar', 'pace' ),
    'new_item'           => __( 'New Webinar', 'pace' ),
    'edit_item'          => __( 'Edit Webinar', 'pace' ),
    'view_item'          => __( 'View Webinar', 'pace' ),
    'all_items'          => __( 'All Webinars', 'pace' ),
    'search_items'       => __( 'Search Webinars', 'pace' ),
    'parent_item_colon'  => __( 'Parent Webinar:', 'pace' ),
    'not_found'          => __( 'No properties found.', 'pace' ),
    'not_found_in_trash' => __( 'No properties found in Trash.', 'pace' )
  );

  $args = array(
    'labels' => $labels,
    'supports' => array(
      'title',
      'editor'
    ),
    'public' => true,
    'has_archive' => true,
    'menu_icon' => 'dashicons-video-alt2',
    'menu_position' => 50,
    );

  register_post_type( 'Webinars', $args );
  flush_rewrite_rules();

  add_post_type_support( 'Webinars', 'page-attributes' );
  //end custom post type - WEBINAR


  //custom post type - Customer Spotlight
  // add post type
  $labels = array(
    'name'               => _x( 'Customer Spotlights', 'post type general name', 'pace' ),
    'singular_name'      => _x( 'Spotlight', 'post type singular name', 'pace' ),
    'menu_name'          => _x( 'Customer Spotlights', 'admin menu', 'pace' ),
    'name_admin_bar'     => _x( 'Spotlight', 'add new on admin bar', 'pace' ),
    'add_new'            => _x( 'Add New', 'Spotlight', 'pace' ),
    'add_new_item'       => __( 'Add New Spotlight', 'pace' ),
    'new_item'           => __( 'New Spotlight', 'pace' ),
    'edit_item'          => __( 'Edit Spotlight', 'pace' ),
    'view_item'          => __( 'View Spotlight', 'pace' ),
    'all_items'          => __( 'All Customer Spotlights', 'pace' ),
    'search_items'       => __( 'Search Customer Spotlights', 'pace' ),
    'parent_item_colon'  => __( 'Parent Spotlight:', 'pace' ),
    'not_found'          => __( 'No properties found.', 'pace' ),
    'not_found_in_trash' => __( 'No properties found in Trash.', 'pace' )
  );

  $args = array(
    'labels' => $labels,
    'supports' => array(
      'title',
    ),
    'public' => true,
    'has_archive' => true,
    'menu_icon' => 'dashicons-video-alt2',
    'menu_position' => 50,
    );

  register_post_type( 'customer-spotlight', $args );
  flush_rewrite_rules();

  add_post_type_support( 'customer-spotlight', 'page-attributes' );
  //end custom post type - Customer Spotlight


 //custom post type - Customer Resources
  // add post type
  $labels = array(
    'name'               => _x( 'Customer Resources', 'post type general name', 'pace' ),
    'singular_name'      => _x( 'Customer Resource', 'post type singular name', 'pace' ),
    'menu_name'          => _x( 'Customer Resources', 'admin menu', 'pace' ),
    'name_admin_bar'     => _x( 'Customer Resource', 'add new on admin bar', 'pace' ),
    'add_new'            => _x( 'Add New', 'Customer Resource', 'pace' ),
    'add_new_item'       => __( 'Add New Customer Resource', 'pace' ),
    'new_item'           => __( 'New Customer Resource', 'pace' ),
    'edit_item'          => __( 'Edit Customer Resource', 'pace' ),
    'view_item'          => __( 'View Customer Resource', 'pace' ),
    'all_items'          => __( 'All Customer Resources', 'pace' ),
    'search_items'       => __( 'Search Customer Resources', 'pace' ),
    'parent_item_colon'  => __( 'Parent Customer Resource:', 'pace' ),
    'not_found'          => __( 'No properties found.', 'pace' ),
    'not_found_in_trash' => __( 'No properties found in Trash.', 'pace' )
  );

  $args = array(
    'labels' => $labels,
    'supports' => array(
      'title',
      //'editor',
      //'thumbnail',
    ),
    'public' => true,
    'has_archive' => true,
    'menu_icon' => 'dashicons-portfolio',
    'menu_position' => 55,
    );

  register_post_type( 'customer_resources', $args );
  add_post_type_support( 'customer_resources', 'page-attributes' );
  //end custom post type - Customer Resources

  //custom taxonomy - Customer Resource
  $labels = array(
    'name'              => _x( 'Customer Resource Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Customer Resource Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Customer Resource Categories' ),
    'all_items'         => __( 'All Customer Resource Categories' ),
    'parent_item'       => __( 'Parent Customer Resource Category' ),
    'parent_item_colon' => __( 'Parent Customer Resource Category:' ),
    'edit_item'         => __( 'Edit Customer Resource Category' ),
    'update_item'       => __( 'Update Customer Resource Category' ),
    'add_new_item'      => __( 'Add New Customer Resource Category' ),
    'new_item_name'     => __( 'New Customer Resource Category Name' ),
    'menu_name'         => __( 'Category' ),
  );

  register_taxonomy(
    'customer_resource_category',
    'customer_resources',
    array(
      'labels' => $labels,
      'hierarchical' => true,
    )
  );
  //end custom taxonomy - Customer Resource

  //custom taxonomy - Customer Resource
  $labels = array(
    'name'              => _x( 'Products', 'taxonomy general name' ),
    'singular_name'     => _x( 'Product', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Products' ),
    'all_items'         => __( 'All Products' ),
    'parent_item'       => __( 'Parent Product' ),
    'parent_item_colon' => __( 'Parent Product:' ),
    'edit_item'         => __( 'Edit Product' ),
    'update_item'       => __( 'Update Product' ),
    'add_new_item'      => __( 'Add New Product' ),
    'new_item_name'     => __( 'New Product Name' ),
    'menu_name'         => __( 'Products' ),
  );

  register_taxonomy(
    'customer_resource_product',
    'customer_resources',
    array(
      'labels' => $labels,
      'hierarchical' => true,
    )
  );
  //end custom taxonomy - Customer Resource

  //custom taxonomy - Customer Resource
  $labels = array(
    'name'              => _x( 'Document Languages', 'taxonomy general name' ),
    'singular_name'     => _x( 'Document Language', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Document Languages' ),
    'all_items'         => __( 'All Document Languages' ),
    'parent_item'       => __( 'Parent Document Language' ),
    'parent_item_colon' => __( 'Parent Document Language:' ),
    'edit_item'         => __( 'Edit Document Language' ),
    'update_item'       => __( 'Update Document Language' ),
    'add_new_item'      => __( 'Add New Document Language' ),
    'new_item_name'     => __( 'New Document Language Name' ),
    'menu_name'         => __( 'Document Languages' ),
  );

  register_taxonomy(
    'customer_resource_language',
    'customer_resources',
    array(
      'labels' => $labels,
      'hierarchical' => true,
    )
  );
  //end custom taxonomy - Customer Resource
}
// admin init hook
add_action( 'admin_init', 'pace_admin_init' );
function pace_admin_init() {

}

function remove_editor() {
  if (isset($_GET['post'])) {
    $id = $_GET['post'];
    $template = get_post_meta($id, '_wp_page_template', true);

    switch( $template ) {

      // only remove editor
      case 'templates/eplex-pipeline-panel.php':

        break;

      // remove editor and thumbnail
      case 'front-page.php':
      case 'templates/customer-resource-center.php':
      case 'templates/contact.php':
      case 'templates/no-content.php':
        remove_post_type_support( 'page', 'editor' );

      // remove only thumbnail
      case 'templates/people-management.php':
      case 'templates/people-board-of-directors.php':
      case 'templates/solution.php':
      case 'templates/publications.php':
      case 'templates/conferences.php':
      case 'templates/webinars.php':
      case 'templates/forgot-password.php':
      case 'templates/request-information.php':
      case 'default':
        remove_post_type_support( 'page', 'thumbnail' );
        break;
    }

  }
}
add_action( 'admin_init', 'remove_editor' );

function search_distinct() {
  return "DISTINCT";
}
add_filter('posts_distinct', 'search_distinct');

/**
 * Shortcodes
 */

// include scripts
function pace_scripts() {

  wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css' );
  wp_enqueue_style( 'theme-print-style', get_stylesheet_directory_uri().'/print.css', array(), false, 'print' );
  wp_enqueue_style( 'videojs-style', '/../bootstrap/css/bootstrap.css', array(), '3.3.2' );
  wp_enqueue_style( 'bootstrap-style', '//vjs.zencdn.net/4.12/video-js.css', array(), '4.12' );
  wp_enqueue_style( 'genmark-icons', 'https://fontastic.s3.amazonaws.com/bbzHmTRRsE2R6YqX9nSWTi/icons.css');
  wp_enqueue_style( 'helvetica-myfonts', get_stylesheet_directory_uri().'/css/helveticaneue.css');
  // wp_enqueue_style( 'lightbox-style', get_stylesheet_directory_uri().'/css/lightbox.css' );
  wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array(), '1.04' );

  wp_enqueue_script( 'prefixFree', get_stylesheet_directory_uri().'/js/prefixfree.min.js',array(),'1.11.0');
  wp_enqueue_script( 'jQuery', get_stylesheet_directory_uri().'/js/jquery.js',array(),'1.11.0');
  wp_enqueue_script( 'jQuery-scripts', get_stylesheet_directory_uri().'/js/jqscript.js',array(),'1');
  wp_enqueue_script( 'snippets', get_stylesheet_directory_uri().'/js/snippets.js',array(),'1');
  wp_enqueue_script( 'dotimeout', get_stylesheet_directory_uri().'/js/jquery.ba-dotimeout.min.js',array(),'1');
  wp_enqueue_script( 'bootstrap-script', '/../bootstrap/js/bootstrap.min.js',array(),'3.3.2',true);
  wp_enqueue_script( 'videojs-script', '//vjs.zencdn.net/4.12/video.js',array(),'4.12',true);
  wp_enqueue_script( 'validate-script', '//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js',array(),'1.14.0',true);
  // wp_enqueue_script( 'lightbox-script', get_stylesheet_directory_uri().'/js/lightbox.min.js',array(),'2.7.1',true);

  $localize_vals = array(
    'WP_HOME' => WP_HOME,
    'DEFAULT_PREF_LANG_TIMEOUT', DEFAULT_PREF_LANG_TIMEOUT,
    'curpage' => get_the_permalink(),
    'time' => time(),
    'stylesheet_url' => get_stylesheet_directory_uri(),
    'ajax_url' => admin_url( 'admin-ajax.php' ),
  );
  wp_localize_script( 'jQuery-scripts', 'php_vals', $localize_vals, 'pace');

}
add_action( 'wp_enqueue_scripts', 'pace_scripts' );

function add_defer_attribute( $tag, $handle ) {
  // add script handles to the array below
  $scripts_to_defer = array('bootstrap-script', 'videojs-script', 'validate-script');

  foreach($scripts_to_defer as $defer_script) {
    if ($defer_script === $handle) {
     return str_replace(' src', ' defer src', $tag);
    }
  }

  return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);

function add_async_attribute($tag, $handle) {
   // add script handles to the array below
   $scripts_to_async = array('prefixFree', 'snippets', 'dotimeout');

   foreach($scripts_to_async as $async_script) {
      if ($async_script === $handle) {
         return str_replace(' src', ' async src', $tag);
      }
   }
   return $tag;
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


//** Neuter Admin Section **//

// Home BOXES control
add_action( 'admin_menu', 'register_pace_admin', 9999 );
function register_pace_admin(){

  // remove items not in use in admin menu
  global $menu;

  // unused sections
  $restricted = array(
    __('menu-comments'),
    __('menu-posts'),
  );

  // restrict by role
  $current_user = wp_get_current_user();

  if(!in_array( 'administrator', $current_user->roles )) {
    // $restricted[] = __('menu-plugins');
    $restricted[] = __('menu-users');
    $restricted[] = __('menu-tools');
    // $restricted[] = __('menu-settings');
    // $restricted[] = __('menu-appearance');
    $restricted[] = __('toplevel_page_wpseo_dashboard');
    $restricted[] = __('toplevel_page_wpcf7');

    remove_submenu_page('index.php', 'index.php?page=relevanssi%2Frelevanssi.php');
    // remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
    // remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
    // remove_submenu_page('edit.php?post_type=page', 'post-new.php?post_type=page');

  }

  // add a separator at the end
  $menu[] = array(
    0 =>  '',
    1 =>  'read',
    2 =>  'separator',
    3 =>  '',
    4 =>  'wp-menu-separator'
  );

  end ($menu);
  while (prev($menu)){
    $value = isset($menu[key($menu)][5]) ? $menu[key($menu)][5] : false;

    if(in_array($value, $restricted)){unset($menu[key($menu)]);}
  }
}

// customize admin bar
add_action( 'wp_before_admin_bar_render', 'modify_admin_bar' );


function modify_admin_bar(){

  global $wp_admin_bar;

  // general restrictions
  $wp_admin_bar->remove_menu('comments');

  // restrict by role
  $current_user = wp_get_current_user();

  if(!in_array( 'administrator', $current_user->roles )) {
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('new-content');
    $wp_admin_bar->remove_menu('wpseo-menu');
  }

}
//** End Neuter Admin Section **//

function the_excerpt_max_charlength($charlength) {
  $excerpt = get_the_excerpt();
  $charlength++;

  if ( mb_strlen( $excerpt ) > $charlength ) {
    $subex = mb_substr( $excerpt, 0, $charlength - 5 );
    $exwords = explode( ' ', $subex );
    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    if ( $excut < 0 ) {
      echo mb_substr( $subex, 0, $excut );
    } else {
      echo $subex;
    }
    echo '[...]';
  } else {
    echo $excerpt;
  }
}

add_filter( 'nav_menu_link_attributes', 'pace_menu_link_attributes', 10, 3 );
function pace_menu_link_attributes( $atts, $item, $args ) {

  if( $item->menu_item_parent == 0 ) {

    $_tmp = explode( ' ', $args->menu_class );

    foreach( $_tmp as &$class ) {

      $class .= '__link';

    }

    // $atts['class'] = $args->menu_class . '__link';
    $atts['class'] = implode( ' ', $_tmp );

  }


  return $atts;
}

add_filter( 'nav_menu_css_class', 'pace_menu_css_class', 10, 3 );
function pace_menu_css_class( $classes, $item, $args ) {

  if( $item->menu_item_parent == 0 ) {

    $_tmp = explode( ' ', $args->menu_class );

    foreach( $_tmp as &$class ) {

      $classes[] =  $class . '__item';

    }

  }

  return $classes;
}

function pace_custom_admin_js() {
    $url = get_bloginfo('template_directory') . '/js/wp-admin.js';
    echo '"<script type="text/javascript" src="'. $url . '"></script>"';
}
add_action('admin_footer', 'pace_custom_admin_js');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {

  // Define the style_formats array
  $style_formats = array(
    // Each array child is a format with it's own settings
    array(
      'title' => 'Button Link - Plain',
      'selector' => 'a',
      'classes' => 'btn btn-default',
    ),
    array(
      'title' => 'Button Link - PDF',
      'selector' => 'a',
      'classes' => 'btn btn-default fa-icon fa-icon--pdf',
    ),

    array(
      'title' => 'Table - Speficiations',
      'selector' => 'table',
      'classes' => 'table table--specs',
    ),

    array(
      'title' => 'Table - Comparisons',
      'selector' => 'table',
      'classes' => 'table table--comparison',
    ),
    array(
      'title' => 'Table - Diagrams',
      'selector' => 'table',
      'classes' => 'table table--diagram',
    ),
    array(
      'title' => 'Table - Responsive',
      'selector' => 'table',
      'classes' => 'table table--responsive',
    ),
    array(
      'title' => 'Table Cell - Comparison Highlight',
      'selector' => '.table--comparison td',
      'classes' => 'highlight',
    ),

    array(
      'title' => 'Bold + Blue',
      'inline' => 'strong',
      'classes' => 'upper-blue',
    ),

    array(
      'title' => 'Question',
      'inline' => 'span',
      'classes' => 'question',
    ),

    array(
      'title' => 'Note',
      'inline' => 'span',
      'classes' => 'note',
    ),

  );
  // Insert the array, JSON ENCODED, into 'style_formats'
  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

/**
 * The site doesn't allow for direct viewing of top-level pages
 */
function genmark_wrangle_pages() {

  // skep preview
  if( is_preview() ) {
    return;
  }

  // only do this on pages
  if( is_page() ) {

    $id = get_the_ID();

    // get parent ID
    $parent_id = wp_get_post_parent_id( $id );
    $template = get_post_meta( $id, '_wp_page_template', true );

    // check if top level, or not a content page
    if( $parent_id == 0 || $template === 'templates/no-content.php' ) {

      // find children
      $args = array(
        'post_parent' => get_the_ID(),
        'post_type' => 'page',
        'numberposts' => 1,
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC'
      );
      $children = get_children( $args );

      if( !empty( $children )) {

        // get child
        $child = array_pop( $children );

        // get child's URL
        $url = get_the_permalink( $child->ID );

        // redirect
        wp_redirect( $url );
        exit;

      }

    }

  }

}
add_action( 'template_redirect', 'genmark_wrangle_pages' );

// deprecated, do nothing
function check_https( $url ) {
  return $url;
}

function genmark_title_asterisk( $title, $id = null, $override = false ) {

  if( is_admin() ) {
    return $title;
  }

  if( $override !== true && get_field( 'add_asterisk_to_title', $id )) {
    $title = $title . '<sup>*</sup>';
  }

  if( $override === true ) {
    $title = str_replace( '<sup>*</sup>', '', $title );
  }

  return $title;

}
add_filter( 'the_title', 'genmark_title_asterisk', 10, 3 );

function check_cache_file($file) {

  // check for existing file
  if( file_exists( $file ) ) {

    // find out how old the file is
    $filetime = filemtime($file);
    $timediff = time() - $filetime;

    // check if file has timed out
    if( $timediff < CACHE_TIMEOUT ) {
      return file_get_contents( $file );
    }

  }

  return false;
}

function checkdir($dir) {
  if( is_dir( $dir ) || mkdir( $dir )) {
    return $dir;
  } else {
    return false;
  }
}

function human_filesize($bytes, $decimals = 2) {
  $sz = 'BKMGTP';
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}

// call get_children recursively
function get_children_recursive( $args ) {

  $children = array();

  $the_query = new WP_Query($args);
  $tmp = $the_query->posts;
  // $tmp = get_children( $args );

  foreach( $tmp as $child ) {
    $args['post_parent'] = $child->ID;
    $child->children = get_children_recursive($args);
    $children[] = $child;
  }

  return $children;

}

// format get_children_recursive output into a list
function children2list( $children, $args = array(), $depth = 0 ) {

  // set up some defaults
  $defaults = array(
    'list_class' => 'subnav',
    'current_id' => false,
    'carets' => true,
    'caret_class' => 'caret subnav__collapse-toggle',
    'maxdepth' => 999,
  );

  $args = wp_parse_args( $args, $defaults );

  if( $depth >= $args['maxdepth'] ) return false;

  $args['item_class'] = $args['list_class'].'__item';
  $args['link_class'] = $args['list_class'].'__link';

  // get ancestor tree
  $ancestors = get_post_ancestors( get_the_ID() );

  $list_class = $args['list_class'];
  if( $args['current_id'] !== false && in_array( $args['current_id'], $ancestors )) {
    $list_class .= ' active';
  }

  // start list output
  $list = '';
  $list .= '<ul class="' . $list_class . ' depth' . $depth . '">';

  // build from children
  foreach( $children as $child ) {


    if( get_the_title($child->ID) == 'Blood Culture Identification (BCID) Panels' && ( ICL_LANGUAGE_CODE == 'int' )) {
      continue;
    }

    $has_children = !empty( $child->children ) && $depth+1 < $args['maxdepth'];
    $item_class = $args['item_class'];
    $link_class = $args['link_class'];
    $caret_class = $args['caret_class'];

    if( $has_children && $depth+1 < $args['maxdepth'] ) {
      $item_class .= ' has_children';
    }
    if( $child->ID == get_the_ID() ) {
      $item_class .= ' active';
      $caret_class .= ' active';
    }
    if( in_array( $child->ID, $ancestors ) ) {
      $item_class .= ' has_active_children';
      $caret_class .= ' active';
    }

    $list .= '<li class="' . $item_class . '">';
      $list .= '<a class="' . $link_class . '" href="' . get_the_permalink( $child->ID ) . '">'.get_the_alt_title($child->ID).'</a>';

      if( $has_children && $args['carets'] === true ) {
        $list .= '<span class="' . $caret_class . '"></span>';
      }

    // if this item has children, recusively call
    if( $has_children ) {
      $args['current_id'] = $child->ID;
      $list .= children2list( $child->children, $args, $depth+1 );
    }

    $list .= '</li>';

  }

  $list .= '</ul>';

  return $list;

}

// shortcut function to check for alternate title if it exists
function get_the_alt_title( $post_id = false, $field_name = 'alt_title' ) {

  $post_id = $post_id ? $post_id : get_the_ID();

  $alt = get_field( $field_name, $post_id );

  $post = get_post( $post_id );

  $title = empty( $alt ) ? $post->post_title : $alt;

  return $title;

}
function the_alt_title( $post_id = false, $field_name = 'alt_title' ) {
  echo get_the_alt_title( $post_id, $field_name );
}

add_action( 'wp_login_failed', 'my_front_end_login_fail' );  // hook failed login

function my_front_end_login_fail( $username ) {

  // get login page
  $login_page = get_page_by_title('login');
  $login_url = get_the_permalink($login_page);

  $referrer = wp_get_referer();  // where did the post submission come from?

  // if there's a valid referrer, and it's not the default log-in screen
  if ( !empty( $referrer ) && !empty($login_url) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
    wp_redirect( add_query_arg('login', 'failed', $login_url) );  // let's append some information (login=failed) to the URL for the theme to use
    exit;
  }
}
/*
  When WordPress fixes get_pages() to allow meta_field/meta_key args, update with a hook into "acf/fields/post_object/query/name=system"
*/
// function genmark_acf_create_field( $field ) {

//   // act on "System Page" field
//   if( $field['_name'] == 'system' ) {

//     pvar_dump($field);

//     if( !isset( $field['choices'] )) {
//       return;
//     }

//     foreach( $field['choices'] as $id => $choice ) {
//       $template = get_post_meta($id, '_wp_page_template', true);

//       if( $template !== 'templates/system.php' ) {
//         // unset( $field['choices'][$id] );
//       }
//     }
//   }

// }
// add_filter( 'acf/create_field', 'genmark_acf_create_field', 5);


function handle_location() {

  // don't do this stuff in the admin
  if( is_admin()
    || is_preview()
    || strstr( $_SERVER['REQUEST_URI'], 'wp-admin' ) !== false
    || strpos( $_SERVER['REQUEST_URI'], '/count' ) === 0
    || $_SERVER['PHP_SELF'] == '/wordpress/wp-login.php'
    || $_SERVER['PHP_SELF'] == '/genmark/wordpress/wp-login.php' ) {
    return;
  }

  // check if the user has been to the site before
  $pref_lang = isset( $_COOKIE['pref_lang'] ) ? $_COOKIE['pref_lang'] : false;
  $last_lang = isset( $_COOKIE['last_lang'] ) ? $_COOKIE['last_lang'] : false;

  if( $pref_lang == 'undefined' ) {
    $pref_lang = false;
  }
  if( $last_lang == 'undefined' ) {
    $last_lang = false;
  }

  // see if that region matches current language
  if( $pref_lang === ICL_LANGUAGE_CODE ) {

    // refresh cookie
    set_pref_language( $pref_lang );

    // don't redirect
    return;

  } elseif ( $pref_lang !== false ) {

    // build current URL
    $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    $url = strtolower($url);
    $url = remove_query_arg('alert', $url);

    // find translated page
    $newurl = get_url_for_language( $url, $pref_lang );
    $newurl = add_query_arg( 'alert', 'regionchanged', $newurl );

    // refresh cookie
    // set_pref_language( $pref_lang );

    // redirect
    // wp_redirect( $newurl );
    // exit;
    return;

  } elseif( $last_lang === ICL_LANGUAGE_CODE ) {

    // do nothing
    return;

  } elseif( $last_lang !== false ) {

    // build current URL
    $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

    // find translated page
    $newurl = get_url_for_language( $url, $last_lang );
    $newurl = add_query_arg( 'alert', 'lastlang', $newurl );

    // refresh cookie
    set_last_lang( $last_lang );

    // redirect
    wp_redirect( $newurl );
    exit;

  }

}
add_action( 'template_redirect', 'handle_location', 9 );

function geo_lang_match() {

  // find geolocated country
  $country = get_geo_language();

  // match country code to possible regions
  $match_lang = country_code_to_lang_code( $country );

  if( $match_lang !== ICL_LANGUAGE_CODE ) {
    return false;
  }

  return true;

}

function ajax_geo_lang_match() {

  $match = geo_lang_match();

  if( $match ) {
    echo 1;
  } else {
    echo 0;
  }

  die;

}

// trigger for both logged in and not
add_action( 'wp_ajax_nopriv_ajax_geo_lang_match', 'ajax_geo_lang_match' );
add_action( 'wp_ajax_ajax_geo_lang_match', 'ajax_geo_lang_match' );

function get_geo_object() {

  global $geoplugin;

  if( $geoplugin->ip === null ) {

    // give a default IP for local testing
    $ip = $_SERVER['REMOTE_ADDR'];

    if( $ip == '127.0.0.1' || $ip == '::1' ) {
      $ip = '96.49.112.77';
    }

    $geoplugin->locate( $ip );

  }

  return $geoplugin;

}

function get_geo_language() {

  $geoplugin = get_geo_object();

  return $geoplugin->countryCode;

}

function set_pref_language( $lang, $expire = false ) {

  // default expiry is in 5 days
  if( empty( $expire )) {
    $expire = DEFAULT_PREF_LANG_TIMEOUT;
  }

  // keep preferred language for 5 days
  setcookie( 'pref_lang', $lang, $expire, '/' );

}

function set_last_lang( $lang = false ) {

  $lang = $lang ? $lang : ICL_LANGUAGE_CODE;

  // "last language" cookie expires with browser
  setcookie( 'last_lang', $lang, 0, '/' );

}

// trigger for both logged in and not
add_action( 'wp_ajax_nopriv_set_last_lang', 'set_last_lang' );
add_action( 'wp_ajax_set_last_lang', 'set_last_lang' );

function country_code_to_lang_code( $code ) {
  return country_code_to_lang( $code );
}

function country_code_to_lang( $code, $format = 'code' ) {

  $langs = array(
    'default' => array(
      'code' => 'int',
      'name' => 'International',
    ),
    'US' => array(
      'code' => 'en',
      'name' => 'United States',
    ),
  );

  if( isset( $langs[ $code ][ $format ] )) {
    $lang = $langs[ $code ][ $format ];
  } else {
    $lang = $langs['default'][ $format ];
  }

  return $lang;

}

// credit: http://www.barrykooij.com/getting-the-url-for-a-translated-page-based-on-the-url-of-a-different-language-using-wpml/
function get_url_for_language( $original_url, $language ) {

  $post_id = url_to_postid( $original_url );
  $lang_post_id = apply_filters( 'wpml_object_id', $post_id , 'page', true, $language );

  $url = "";
  if($lang_post_id != 0) {

    $url = get_permalink( $lang_post_id );

    if( $url === $original_url ) {
      $url = apply_filters('wpml_permalink', $url, $language);
    }

  } else {

    // No page found, it's most likely the homepage
    global $sitepress;
    $url = $sitepress->language_url( $language );

  }

  return $url;

}

function get_current_url_by_language( $lang = false ) {

  if( !$lang ) {
    $lang = ICL_LANGUAGE_CODE;
  }

  if( is_front_page() ) {
    $url = get_home_url();
  } else {
    $url = get_the_permalink();
  }

  return get_url_for_language( $url, $lang );

}


function update_crc_welcome_widget_setting() {

  if ( isset($_REQUEST) ) {
    $bool_val = $_REQUEST['bool_val'];
    $user_id = $_REQUEST['user_id'];
    if ($bool_val == 'true')  update_field('field_55c52ea39019d', true, $user_id);
    if ($bool_val == 'false') update_field('field_55c52ea39019d', false, $user_id);
  }
  die();

}
add_action( 'wp_ajax_update_crc_welcome_widget_setting', 'update_crc_welcome_widget_setting' );
add_action( 'wp_ajax_nopriv_update_crc_welcome_widget_setting', 'update_crc_welcome_widget_setting' );


function update_modal_video() {

  if ( isset($_REQUEST) ) {
    $post_id = $_REQUEST['post_id'];
    $field_name = $_REQUEST['field_name'];

    if( $field_name == 'webinar-video' ) {
      $video = get_field( $field_name, $post_id);
    }

    if( $field_name == 'widget-video' ) {
      $fileName = $post_id;
      $video = array(
        "id" => $fileName,
        "alt" => "",
        "title" => "",
        "caption" => "",
        "description" => "",
        "mime_type" => "video/mp4",
        "url" => get_template_directory_uri().'/vid/'.$fileName.'.mp4',
        "poster" => get_stylesheet_directory_uri().'/img/video-poster.jpg',
      );
    }

    if( $field_name == 'system-video' ) {
      $video_field = get_field( 'video', $post_id );
      $video = array(
        "id" => $fileName,
        "alt" => "",
        "title" => "",
        "caption" => "",
        "description" => "",
        "mime_type" => "video/mp4",
        "url" => $video_field['url'],
        "poster" => get_stylesheet_directory_uri().'/img/video-poster.jpg',
      );
    }
    //print_r($video);
    print json_encode( $video );
  }
  die();

}
add_action( 'wp_ajax_update_modal_video', 'update_modal_video' );
add_action( 'wp_ajax_nopriv_update_modal_video', 'update_modal_video' );

/*
// AJAX example
function example_ajax_request() {

    // The $_REQUEST contains all the data sent via ajax
    if ( isset($_REQUEST) ) {

        $fruit = $_REQUEST['fruit'];

        // Let's take the data that was sent and do something with it
        if ( $fruit == 'Banana' ) {
            $fruit = 'Apple';
        }

        // Now we'll return it to the javascript function
        // Anything outputted will be returned in the response
        echo $fruit;

        // If you're debugging, it might be useful to see what was sent in the $_REQUEST
        // print_r($_REQUEST);

    }

    // Always die in functions echoing ajax content
   die();
}

add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );
add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );
*/

add_filter('get_the_excerpt', 'genmark_search_excerpt');
function genmark_search_excerpt($excerpt) {

  global $post;

  if( is_search() ) {
    $excerpt = $excerpt . ' <a class="morelink" href="'.get_the_permalink($post->ID).'">More &raquo;</a>';
  }

  return $excerpt;

}

/*
 * Pronounceable password generator
 * version 1.0 - made into a function
 * Inspired by a question made by: georgcantor_at_geocities.com
 * in the PHPBuilder discussion forum
 * (c) Jesus M. Castagnetto, 1999
 * GPL'd code, see www.fsf.org for more info
 */


/*
 * function ppassgen()
 * parameters:
 * $words = the name of the file w/ the words (one per line)
 *      or and array of words
 * $min = the minimum number of words per password
 * $max = the maximum number of words per password
 * $cutoff = the minimum number of characters per word
 * $sep = separator for the words in the password
 */

function ppassgen($words= "mywords", $min=2, $max=4, $cutoff=5, $sep= "_") {

  $pass = '';

    if(is_array($words)) {
        /* if we have passed and array of words, use it */
        $word_arr =  "words";
         /*
        while(list($k,$v) = each(${$word_arr})) {
            echo "$k $v<BR>";
        }
        */
    } else {
        /* read the external file into an array */
        $fp = fopen($words, "r");

        if (!fp) {
            echo  "[ERROR}: Could not open file $words<BR>\n";
            exit;
        } else {
            /* assuming words of up to 127 characters */
            $word_arr =  "ext_arr";
            while(!feof($fp)) {
                $tword = trim(fgets($fp,128));

                /* check for minimum length and for exclusion of numbers */
                if ((strlen($tword) >= $cut_off) && !ereg( "[0-9]",$tword)) {
                    $ext_arr[] = strtolower($tword);
                }
            }
            fclose($fp);
        }
    }

    /* size of array of words */
    $size_word_arr = count(${$word_arr});

    /* generate the password */
    srand((double)microtime()*1000000);

    /* or use the Mersenne Twister functions */
    //mt_srand((double)microtime()*1000000);

    /* for a password of a fixed word number, min = max */
    $n_words = ($min == $max)? $min : rand($min,$max);

    /* or use the Mersenne Twister for a better random */
    //$n_words = ($min == $max)? $min : mt_rand($min,$max);

    for ($i=0; $i<$n_words; $i++) {
        $pass .= ${$word_arr}[rand(0,($size_word_arr - 1))] . $sep;
    }
    /* return the password minus the last separator */
    return substr($pass,0,-1*strlen($sep));
}

add_filter('relevanssi_pre_excerpt_content', 'search_add_custom_fields', 10, 3);

function search_add_custom_fields($content, $post, $query) {

  $add = array();

  // get some custom fields to throw into the excerpts
  $add['pub_authors'] = get_field( 'publication-authors', $post->ID );
  $add['web_presenters'] = get_field( 'webinar-presenters', $post->ID );

  $content .= ' ' . implode( ' ', $add );

  return trim( $content );

}

function genmark_upper( $str ) {
  $str = strtoupper( $str );
  $str = str_replace( 'HCVG', 'HCVg', $str );
  $str = str_replace( 'ESENSOR', 'eSENSOR', $str );
  $str = str_replace( 'EPLEX', 'ePLEX', $str );

  return $str;
}

// Change what's hidden by default
add_filter('default_hidden_meta_boxes', 'hide_meta_lock', 10, 2);
function hide_meta_lock($hidden, $screen) {

  // hiden yoast SEO box by default
  $hidden[] = 'wpseo_meta';
  $hidden[] = 'revisionsdiv';
  $hidden[] = 'icl_div_config';

  return $hidden;
}


function genmark_custom_columns( $column, $post_id ) {

  switch ( $column ) {

    case 'region':
      $region = get_field( 'region', $post_id );

      echo strtoupper($region);

      break;
  }

}
add_action( 'manage_posts_custom_column' , 'genmark_custom_columns', 10, 2 );


function add_customer_resources_columns($columns) {
  return array_merge( $columns, array(
    'region' => __('Region','genmark'),
  ));
}
add_filter('manage_customer_resources_posts_columns' , 'add_customer_resources_columns');

function sortable_customer_resources_column( $columns ) {
    $columns['region'] = 'region';

    //To make a column 'un-sortable' remove it from the array
    //unset($columns['date']);

    return $columns;
}
add_filter( 'manage_edit-customer_resources_sortable_columns', 'sortable_customer_resources_column' );

add_action( 'pre_get_posts', 'sort_region_pre_get_posts', 1 );
function sort_region_pre_get_posts( $query ) {

   /**
    * We only want our code to run in the main WP query
    * AND if an orderby query variable is designated.
    */
   if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {

      switch( $orderby ) {

         case 'region':

            // set our query's meta_key, which is used for custom fields
            $query->set( 'meta_key', 'region' );

            /**
             * If your meta value are numbers, change 'meta_value'
             * to 'meta_value_num'.
             */
            $query->set( 'orderby', 'meta_value' );

            break;

      }

   }

}
function modify_post_mime_types($post_mime_types) {
    $post_mime_types['application/pdf'] = array(__('PDF'), __('Manage PDF'), _n_noop('PDF <span class="count">(%s)</span>', 'PDF <span class="count">(%s)</span>'));
    return $post_mime_types;
}
add_filter('post_mime_types', 'modify_post_mime_types');

function genmark_load_theme_textdomain() {
  load_theme_textdomain('genmark', get_template_directory() . '/languages');
}
add_action( 'after_setup_theme', 'genmark_load_theme_textdomain' );

// Remove query string from static files
function remove_cssjs_ver( $src ) {
 if( strpos( $src, '?ver=' ) )
 $src = remove_query_arg( 'ver', $src );
 return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

// Add rel="next" and rel="prev" for SEO
function add_rel_prev_next() {

  if( is_page_template('templates/publications.php') ) {
    $current_page = max( 1, get_query_var('paged') );
    $max_pages = $_GET['max_pages'];

    if ( $current_page == 1 ) {
      $next_page_url = get_the_permalink() . 'page/' . ($current_page + 1);
      echo '<link rel="next" href="' . $next_page_url . '" />';
      echo "\n";
    } elseif ( $current_page > 1 && $current_page < $max_pages ) {
      $prev_page_url = get_the_permalink() . 'page/' . ($current_page - 1);
      echo '<link rel="prev" href="' . $prev_page_url . '" />';
      echo "\n";
      $next_page_url = get_the_permalink() . 'page/' . ($current_page + 1);
      echo '<link rel="next" href="' . $next_page_url . '" />';
      echo "\n";
    } elseif ( $current_page == $max_pages ) {
      $prev_page_url = get_the_permalink() . 'page/' . ($current_page - 1);
      echo '<link rel="prev" href="' . $prev_page_url . '" />';
      echo "\n";
    }
  }
}
add_action('wp_head', 'add_rel_prev_next', 3);

function genm_software_features_shortcode() {
  ob_start();
  get_template_part('content', 'software-features');
  return ob_get_clean();
}
add_shortcode( 'software_features', 'genm_software_features_shortcode' );


function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

// Function for PDF Download within form on BCID Testing Page
function add_this_script_footer() { ?>
  <script type="text/javascript">
  document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ('5656' == event.detail.contactFormId ) {
      var inputs = event.detail.inputs;
      var url = inputs[6].value;
      window.location.href=url;
      setTimeout(function() { location.reload(); }, 3000);
    }
  }, false );
  </script>
<?php } add_action('wp_footer', 'add_this_script_footer');



function category_html($cr_cat, $level = 0) {
  global $search_prod, $crc_lang, $search, $meta_query;

  $found_results = 0;
  $row_index = 0;

  if ( get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id) != 'default' ):
    $icon = '<img src="'. get_template_directory_uri() .'/img/icon-'. get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id) .'.png" alt="'. get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id). ' file icon">';
    //$i_class = "fa fa-file-" . get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id) . "-o";
        else:
    $icon = '<i class="fa fa-file-o"></i>';
    //$i_class = "fa fa-file-o";
        endif;

      $tax_query = array(
        array(
          'taxonomy' => 'customer_resource_category',
          'field' => 'slug',
          'terms' => $cr_cat->slug,
        ),
      );

      if( $search_prod !== 'all' ) {

        $tax_query['relation'] = 'AND';

        $tax_query[] = array(
          'taxonomy' => 'customer_resource_product',
          'field' => 'slug',
          'terms' => $search_prod,
        );

      }

      if( $crc_lang !== 'all' ) {

        $tax_query['relation'] = 'AND';

        $tax_query[] = array(
          'taxonomy' => 'customer_resource_language',
          'field' => 'slug',
          'terms' => $crc_lang,
        );

      }

      $args = array(
        'post_type' => 'customer_resources',
        'orderby' => 'title',
        'order' => 'ASC',
        'posts_per_page' => -1,
        's' => $search,
        'tax_query' => $tax_query,
        'meta_query' => $meta_query,
      );

      $query = new WP_Query($args);

      if( !empty( $args['s'] )) {
        relevanssi_do_query($query);
      }

      if( $query->have_posts() ):
        $found_results++;
        ?>



    <div style="padding-left: 15px;" class="results-container">
      <h2 class="results-heading" id="crc-cat-heading-<?php echo $cr_cat->term_id; ?>"><?php echo $cr_cat->name; ?></h2>
    <div class="results">


    <?php

      // pvar_dump($cr_cat);
    $termID = $cr_cat->term_id; // Parent A ID
    $args = array(
      'taxonomy' => 'customer_resource_category',
      'parent' => $termID,
    );

    $termchildren = get_terms( $args );
    // pvar_dump($termchildren);

    $childLevel = $level + 1;

    foreach( $termchildren as $category ) {
      category_html($category, $childLevel);
    }

        ?>
        <?php if(empty($termchildren)) : ?>


      <?php
        while( $query->have_posts() ):
          $query->the_post();
        ?>


            <?php $file_url = get_field('cr-file') ? get_field('cr-file')['url'] : '#'; ?>
            <?php $video_url = get_field('video_url') ?>

            <?php
              $title = get_the_title();

              if( is_preview() && get_the_ID() === $real_id ) {

                $title = $real_title;

              }

              $title = genmark_upper( $title );

              $file_format = get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id);


            ?>

            <?php if ( $file_format !== 'video' ) : ?>
            <a class="crc-download" data-name="<?php echo $title; ?>" target="_blank" href="<?php echo $file_url; ?>">
              <p class="upper-blue">
                <span class="icon pull-left"><?php echo $icon; ?></span>
                <span class="title"<?php if($file_url == '#') echo 'style="color:#aaa !important;"'; ?>><?php echo $title; ?></span>
              </p>
            </a>
          <?php endif; ?>

          <?php if ( $file_format == 'video' ) : ?>
            <a class="crc-download" data-name="<?php echo $title; ?>" target="_blank" href="<?php echo $video_url; ?>">
              <p class="upper-blue">
                <span class="icon pull-left"><?php echo $icon; ?></span>
                <span class="title"><?php echo $title; ?></span>
              </p>
            </a>
          <?php endif; ?>


        <?php
        $row_index++;
        endwhile;
        ?>
      <?php endif; ?>
          </div>
      </div>

<?php

endif;
?>
<?php
wp_reset_query();

}
