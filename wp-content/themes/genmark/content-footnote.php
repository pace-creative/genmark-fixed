<?php
  $footnote = get_field('footnotes');
?>

<?php if( $footnote ): ?>
  <p class="footnote">
    <?php echo $footnote; ?>
  </p>
<?php endif; ?>