<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <p>
        The Board of Directors of GenMark Dx (the "Company") sets high standards for the Company's employees, officers and directors. Implicit in this philosophy is the importance of sound corporate governance. It is the duty of the Board of Directors to serve as a prudent fiduciary for shareholders and to oversee the management of the Company's business. To fulfill its responsibilities and to discharge its duty, the Board of Directors follows the procedures and standards that are set forth in these guidelines. These guidelines are subject to modification from time to time as the Board of Directors deems appropriate in the best interests of the Company or as required by applicable laws and regulations.
      </p>

      <h2>Charters</h2>

      <table class="item-list" summary="Sortable Table (Click a column header to sort)">
        <thead>
          <tr>
            <th><a class="sortable desc" href="#">Title <abbr style="border-style: none;" class="sort-icon" title="(sorted descending)"><span class="caret"></span></abbr></a></th>
            <th>Download</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <td>
              Audit Committee Charter
            </td>
            <td width="15%">
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              Corporate Governance and Nominating Committee Charter
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              Compensation Committee Charter
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

        </tbody>
      </table>

      <h2>Documents</h2>

      <table class="item-list" summary="Sortable Table (Click a column header to sort)">
        <thead>
          <tr>
            <th><a class="sortable desc" href="#">Title <abbr style="border-style: none;" class="sort-icon" title="(sorted descending)"><span class="caret"></span></abbr></a></th>
            <th>Download</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <td>
              Corporate Governance Guidelines
            </td>
            <td width="15%">
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

          <tr>
            <td>
              Insider Trading Policy
            </td>
            <td>
              <a class="btn btn-default fa-icon fa-icon--pdf" href="#">Download PDF</a>
            </td>
          </tr>

        </tbody>
      </table>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>