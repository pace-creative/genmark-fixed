<?php

$date_from = DateTime::createFromFormat('d/m/Y', get_field('conference-date-from'));
$date_to =   DateTime::createFromFormat('d/m/Y', get_field('conference-date-to'));
$title = get_the_title();
$location = get_field('conference-location');

?>

<tr>
  <td width="20%">
    <p class="upper-blue">

      <?php

        if( $date_from ) {
          echo $date_from->format('M j');

          if( $date_to ) {

            if($date_from->format('Y')!=$date_to->format('Y')) {
              echo $date_from->format(', Y');
            }

            echo ' - ';

            if($date_from->format('M')==$date_to->format('M')) {
              echo $date_to->format('j, Y');
            } else  {
              echo $date_to->format('M j, Y');
            }
          }

        }

      ?>

    </p>
  </td>
  <td>
    <p><?php echo $title; ?></p>
  </td>
  <td width="20%">
    <p><?php echo $location; ?></p>
  </td>
</tr>
