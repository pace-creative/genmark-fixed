<?php get_header(); ?>

<div class="container">
  <div class="row">

    <?php get_sidebar(); ?>

    <div class="col-xs-12 col-md-9">

      <?php
        $username = ( get_query_var('id')) ? get_query_var('id') : '';
        $a        = ( get_query_var('a')) ? get_query_var('a') : '';
      ?>

      <?php if( $a == 'created' && $username ): ?>
      <div class="login__widget" style="">
        <p>Your account <i><?php echo $username; ?></i> has been created successfully. You may now log in.</p>
      </div>
      <?php endif; ?>

      <?php if( $a == 'pending' && $username ): ?>
      <div class="login__widget" style="">
        <p>Your account <i><?php echo $username; ?></i> has been created successfully, but we were unable to verify your account number and/or serial number. Our customer service team will be in touch to manually verify your information.</p>
      </div>
      <?php endif; ?>

      <?php if( $a == 'loggedout' && $username ): ?>
      <div class="login__widget" style="background: #0068b0; color:#fff;padding:5px 15px;">
        <p>You have logged out <i><?php echo $username; ?></i>.</p>
      </div>
      <?php endif; ?>

      <h1>Customer Resource Center</h1>

  <?php the_content(); ?>

      <?php
        if( isset($_GET['login']) ) echo '<p style="color:red;">'.$_GET['login'].'</p>';
      ?>

      <form action="<?php echo WP_SITEURL; ?>/wp-login.php" method="post">

        <div class="form-group">
          <label for="user_login--page" class="upper-blue"><?php echo strtoupper( __('Username','genmark')); ?>:</label>
          <input name="log" <?php if($username && $a!='loggedout') echo 'value="'.$username.'"'; ?> id="user_login--page" class="form-control" value="" size="20" type="text">
        </div>

        <div class="form-group">
          <label for="user_pass--page" class="upper-blue"><?php echo strtoupper( __('Password','genmark')); ?>:</label>
          <input name="pwd" id="user_pass--page" class="form-control" value="" size="20" type="password">
        </div>

        <div class="checkbox">
          <label for="rememberme"><input name="rememberme" id="rememberme" value="forever" type="checkbox"> Remember Me</label>
        </div>

        <input name="wp-submit" id="wp-submit--popover" class="btn btn-default" value="<?php echo strtoupper( __('Log In','genmark')); ?>" type="submit">
        <input name="redirect_to" value="<?php echo get_home_url(); ?>/customer-resource-center/" type="hidden">

        <p>
          <a href="<?php echo get_home_url(); ?>/account/forgot-password/">Forgot Password?</a> | <a href="<?php echo get_home_url(); ?>/account/create-account/">Create Account</a>
        </p>

      </form>

    </div>
  </div>
</div>

<?php get_footer(); ?>
