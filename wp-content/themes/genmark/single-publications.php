<?php
  if( have_posts() ) {
    the_post();
  }
  global $post;
  $post->post_parent = apply_filters( 'wpml_object_id', 84, 'page' ); // Set the parent to the Webinars page manually

?>
<?php get_header(); ?>

<?
  $date = get_field('publication-date') ? DateTime::createFromFormat('Ymd', get_field('publication-date')) : false;
  $authors = get_field('publication-authors');
  $link = get_field('publication-link');

?>
<div class="page-section page-section--wide">
  <div class="container">

    <div class="row">

      <?php get_sidebar(); ?>

      <div class="col-xs-12 col-md-9">


        <div class="page-section__content">


          <h1>Publications</h1>
          <h2><?php the_title(); ?></h2>

          <?php if ($date): ?>
          <h5 class="upper-blue"><?php echo $date->format('M, Y'); ?></h5>
          <?php endif; ?>

          <?php if ($authors): ?>
          <p>Authors: <?php echo $authors; ?></p>
          <?php endif; ?>

          <hr>

          <p>
            <?php if( $link ): ?>
              <a href="<?php echo $link; ?>" target="_blank" class="btn btn-default">Read Article</a>
            <?php endif; ?>
          </p>

        </div><!-- .page-section__content -->

      </div><!-- .col-xs-12 -->

    </div><!-- .row -->

  </div><!-- .container -->
</div><!-- .page-section -->

<?php get_footer(); ?>



   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->