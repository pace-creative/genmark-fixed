<?php /* Template Name: Forgot Password */ ?>
<?php

  $account = isset( $_POST['account'] ) ? $_POST['account'] : false;
  $email = isset( $_POST['email'] ) ? $_POST['email'] : false;
  $success = false;

  if( isset( $_POST['wp-submit'] )) {

    $errors = array();
    $user = null;
    $user_account = null;

    // Email check
    if( empty( $email )) {
      $errors['email'] = 'Email is requried.';
    } elseif( is_email( $email ) ) {

      $user = get_user_by( 'email', $email );

      if( empty( $user )) {

        $errors['email'] = 'This email does not match our records.';

      }

    } else {

      $errors['email'] = 'This email is invaild.';

    }

    // if( !empty( $user )) {
    //   $user_account = get_field( 'user-old-account', 'user_' . $user->data->ID );
    // }

    // if( empty( $account )) {

    //   $errors['account'] = 'Account number is required.';

    // } elseif( strtolower($user_account) != strtolower($account) ) {

    //   $errors['email'] = 'This email does not match our records.';
    //   $errors['account'] = 'This account number does not match our records.';

    // }

    if( empty( $errors )) {
      // echo ( 'TEST' );
      // $words = file( get_stylesheet_directory_uri() . '/inc/words.php', FILE_IGNORE_NEW_LINES );
      $pass = randomPassword();

      // var_dump( $pass );

      $title = __('Password Reset Request');

      $message = __('You have requested that the password be reset for the following account:') . "\r\n\r\n";
      $message .= network_home_url( '/' ) . "\r\n\r\n";
      $message .= sprintf(__('Username: %s'), $user->data->user_login) . "\r\n\r\n";
      $message .= sprintf(__('Your new password is: %s'), $pass) . "\r\n\r\n";

      if ( $message && wp_mail($email, $title, $message) ) {

        wp_set_password( $pass, $user->data->ID );

        $success = true;

        $account = '';
        $email = '';

      }else{
        $success = 'Error';
      }

    }

  }

 //error_template_header
  $eth = '<div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>';
  //error_template_footer
  $etf = '</div>';

?>
<?php
  get_header();
?>

<div class="container">
  <div class="row">

    <?php get_sidebar(); ?>

    <div class="col-xs-12 col-md-9">

      <h1><?php the_title(); ?></h1>

      <?php the_content(); ?>

      <?php if( $success === true ): ?>
          <div class="alert alert-success" role="alert">
            <p style="margin-top: 0;">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              Your information has been submitted successfully, and your new password has been sent to your email.
            </p>
          </div>
      <?php endif; ?>


      <form action="<?php echo get_permalink(); ?>" method="post">

        <div class="form-group">
          <label for="email" class="upper-blue">EMAIL<span class="required">*</span>:</label>
          <input name="email" id="email" class="form-control" value="<?php echo $email; ?>" size="20" type="text" aria-required="true">
          <?php if( !empty($errors) && !empty($errors['email']) ) echo $eth . $errors['email'] . $etf; ?>
        </div>

        <input name="wp-submit" class="btn btn-default" value="Get Password" type="submit">

      </form>

    </div>
  </div>
</div>


<?php get_footer(); ?>
