<?php /* Template Name: Publications */ ?>

<?php get_header(); ?>

<div class="container">
  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

        <?php
          $paged = get_query_var('paged', 1);
          $order = get_query_var('o', 'asc');
          $orderby = get_query_var('ob', false);
          $meta_key = '';

          if( $orderby ) {
            if( $orderby != 'title' ) {
              $meta_key = $orderby;
              $orderby = 'meta_value';
            }
          } else {
            // set default to sort by date
            $order = 'desc';
            $orderby = 'meta_value';
            $meta_key = 'publication-date';
          }
          $disease_state = get_query_var('disease-state', false);
        ?>

        <h1 class="page-title"><?php the_title(); ?></h1>

        <?php
          global $disease_state_choices;
          $disease_state_field = get_field_object('field_56df26bb5351a');
          $disease_state_choices = isset($disease_state_field['choices']) ? $disease_state_field['choices'] : array();
        ?>

        <form class="form-inline" role="form" method="GET">
          <input type="hidden" name="paged" value="1">
          <div class="form-group">
            <label class="control-label" for="disease-state">Disease State: </label>
            <select name="disease-state" id="disease-state" class="form-control">
              <option value="0">All</option>
              <?php foreach( $disease_state_choices as $value=>$label ): ?>
                <?php

                  if( $value == 'all' ) {
                    continue;
                  }

                  $args = array(
                    'post_type' => 'publications',
                    'numberposts' => -1,
                    'meta_query' => array(
                      array(
                        'key' => 'publication-disease_state',
                        'value' => '"' . $value . '"',
                        'compare' => 'LIKE'
                      ),
                    )
                  );
                  $publications = get_posts($args);
                  $publications_count = count($publications);
                ?>
                <?php if( $publications_count > 0 ):?>
                  <option value="<?php echo $value; ?>"<?php echo $disease_state==$value ? ' selected' : ''; ?>><?php echo $label; ?></option>
                <?php endif; ?>
              <?php endforeach; ?>
            </select>
            <button type="submit" class="btn btn-default">Filter</button>
          </div>
        </form>

        <br>

          <?php
          $args = array(
            'post_type' => 'publications',
            'orderby' => $orderby,
            'meta_key' => $meta_key,
            'order' => $order,
            'posts_per_page' => 10,
            'paged' => $paged,
          );

          if( $disease_state ) {


            $args['meta_query'] = array();
            $args['meta_query']['relation'] = 'OR';

            $args['meta_query'][] = array(
              'key' => 'publication-disease_state',
              'value' => 'all',
              'compare' => 'LIKE',
            );

            $args['meta_query'][] = array(
              'key' => 'publication-disease_state',
              'value' => $disease_state,
              'compare' => 'LIKE',
            );

          }

          $query = new WP_Query($args);
          if( $query->have_posts() ):
            ?>

        <table class="item-list" summary="Sortable Table (Click a column header to sort)">

            <thead>
              <tr>
                <?php
                  $table_headings = array(
                    'publication-date' => 'Date',
                    'publication-disease_state' => 'Disease State',
                    'title' => 'Title',
                    'publication-authors' => 'Authors',
                    'content_type' => 'Type'
                  );
                  foreach( $table_headings as $key=>$label ):
                    $query_vars = array(
                      'paged' => '1',
                      'ob' => $key
                    );

                    if( $key == 'title' ) {
                      if( $orderby == $key ) {
                        $query_vars['o'] = ($order=='asc') ? 'desc' : 'asc';
                      }
                    } else {
                      if( $meta_key == $key ) {
                        $query_vars['o'] = ($order=='asc') ? 'desc' : 'asc';
                      }
                    }

                    if( $disease_state ) {
                      $query_vars['disease-state'] = $disease_state;
                    }
                    ?>
                    <th>
                      <a class="sortable<?php if($meta_key==$key){ echo ($order=='asc') ? ' asc' : ' desc'; } ?>" href="<?php echo add_query_arg( $query_vars, get_the_permalink() ); ?>">
                        <?php echo $label; ?> <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr>
                      </a>
                    </th>
                  <?php
                  endforeach;
                ?>
              </tr>
            </thead>
            <tbody>

            <?php
            while( $query->have_posts() ):
              $query->the_post();
              echo get_template_part('content','publication-item');
            endwhile;
            ?>
            </tbody>
          </table>

          <?php
          if ( $query->max_num_pages > 1 ) :

            $args = array(
              'format' => 'page/%#%/?max_pages=' . $query->max_num_pages,
              'current' => max( 1, get_query_var('paged') ),
              'prev_next' => false,
              'show_all' => true,
              'total' => $query->max_num_pages,
              'type' => 'array'
            );

            $pages = paginate_links( $args );
            if( is_array( $pages ) ) {
              echo '<ul class="pagination">';
              foreach ( $pages as $page ) {
                echo '<li class="pagination__item">'.str_replace("page-numbers", "btn btn-default", $page).'</li>';
              }
             echo '</ul>';
            }
          endif;
          wp_reset_query();
        endif;
        ?>

   </div><!-- .col-xs-12 -->

 </div><!-- .row -->
</div><!-- .container .content -->

<?php get_footer(); ?>
