<?php /* Template Name: Panel */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <?php
        $system = get_field( 'system' );
        $video = get_field( 'video', $system->ID );
        $pdf = get_field('product_literature');
        $box_title = get_field('resource_box_title');
        $intro_text = get_field('intro_text');
        $is_pipeline = get_field('is_pipeline');
        $thumb = false;

        if( has_post_thumbnail() ) {
          $thumb = get_the_post_thumbnail();
        }
      ?>

      <?php if($intro_text ): ?>
        <p class="intro-text intro-text--panel">
          <?php echo $intro_text; ?>
        </p>
      <?php endif; ?>

      <?php if( !empty( $video )): ?>
        <div id="video-modal-<?php the_ID(); ?>" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-video" role="document">
            <div class="modal-content">
              <div class="modal-header clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="pauseModalVideo('system_video_<?php the_ID(); ?>')"><span aria-hidden="true"><?php _e('Close Video','genmark'); ?> &times;</span></button>
              </div>
              <div class="modal-body clearfix">
                <video id="system_video_<?php the_ID(); ?>" class="video-js vjs-default-skin vjs-big-play-centered"
                  controls preload="auto" width="100%" height="450"
                  poster="<?php echo get_stylesheet_directory_uri(); ?>/img/video-poster.jpg"
                  data-setup='{}'>
                  <source src="<?php echo $video['url']; ?>" type='video/mp4' />
                  <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                </video>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>


      <?php if( $system->post_name == 'xt-8-system' ): ?>
        <p>


          <?php if( !empty( $pdf )): ?>
            <a href="<?php echo $pdf['url']; ?>" target="_blank" class="btn btn-default fa-icon fa-icon--pdf"><?php echo strtoupper( __('Product Literature','genmark')); ?></a>
          <?php endif; ?>

          <?php if( !empty( $video )): ?>
            <a href="#" class="btn btn-default fa-icon fa-icon--play" data-toggle="modal" data-target="#video-modal-<?php the_ID(); ?>"><?php echo strtoupper( __('Watch the XT-8 Video','genmark')); ?></a>
          <?php endif; ?>

        </p>
      <?php endif; ?>


      <?php if( $system->post_name == 'eplex-system' && ( !empty( $video ) || !empty( $thumb ))): ?>
        <div class="media media--stackable">
          <?php if( $thumb ): ?>
          <div class="media-left">
            <?php echo $thumb; ?>
          </div>
          <?php endif; ?>

          <div class="media-body">

            <?php
              $widget_class = 'content-widget content-widget--panelinfo';

            ?>

            <div class="<?php echo $widget_class; ?>">

                  <h3 class="content-widget__title">
                    <?php
                      if( $box_title ) {
                        echo $box_title;
                      } elseif( $is_pipeline ) {

                        if( ICL_LANGUAGE_CODE == 'int' && get_the_title() == 'Blood Culture Identification Panels' ) {
                          _e('Now CE Marked on ePlex','genmark');
                        } else {
                          _e('Coming Soon on ePlex','genmark');
                        }

                      } else {

                        if( ICL_LANGUAGE_CODE == 'en' && get_the_title() == 'Respiratory Pathogen Panel' ) {
                          _e('Now FDA Cleared', 'genmark');
                        }

                      }
                    ?>
                  </h3>

              <a href="<?php echo get_the_permalink( $system->ID ); ?>" class="btn btn-default"><?php echo strtoupper( __('View System','genmark')); ?></a>

              <?php if( !empty( $video ) ): ?>
                <a href="#" target="_blank" data-toggle="modal" data-target="#video-modal-<?php the_ID(); ?>" class="btn btn-default fa-icon fa-icon--play"><?php echo strtoupper( __('Watch the video','genmark')); ?></a>
              <?php endif; ?>

              <?php if( !empty( $pdf )): ?>
                <br />
                <a href="<?php echo $pdf['url']; ?>" target="_blank" class="btn btn-default fa-icon fa-icon--pdf"><?php echo strtoupper( __('Product Literature','genmark')); ?></a>
              <?php endif; ?>

            </div>

          </div>
        </div>
      <?php endif; ?>


      <?php the_content(); ?>

      <div class="media media--stackable media--panel-cta">
        <div class="media-body">
          <p>
            To learn more about the <?php the_title(); ?> or to schedule a demonstration, contact us at <strong><?php _e('1.800.373.6767','genmark'); ?></strong> or email us.
          </p>
        </div>
        <div class="media-right media-middle">
          <a href="mailto:info@genmarkdx.com" class="btn btn-default"><?php echo strtoupper( __('Schedule a demonstration via Email','genmark')); ?></a>
        </div>
      </div>

      <?php the_field('additional_content'); ?>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>
