<?php /* Template Name: Customer Resource Center */ ?>
<?php
if( !is_user_logged_in() ){
  $login_ID = 971;
  wp_redirect( get_permalink(apply_filters('wpml_object_id', $login_ID, 'page')) ); exit;
  //wp_redirect( get_home_url() . "/login/" ); exit;
}

$search = isset( $_GET['search'] ) ? $_GET['search'] : false;
$search_cat = isset( $_GET['select_resource'] ) ? $_GET['select_resource'] : 'all';
$search_prod = isset( $_GET['select_product'] ) ? $_GET['select_product'] : 'all';
$crc_lang = isset( $_GET['crc_language'] ) ? $_GET['crc_language'] : 'all';

// get real title for previews
$real_title = get_the_title();
$real_id = get_the_ID();
$real_region = get_field( 'region' );

?>
<?php get_header();  ?>

<div class="crc container">
  <div class="row">


    <?php
    if( is_user_logged_in() ):
      $current_user = wp_get_current_user();
      /*
      echo '<pre>';
      print_r($current_user);
      echo '</pre>';
      */
    ?>


      <?php if( in_array( 'customer-pending', $current_user->roles )): ?>
        <div class="col-xs-12">
          <h1>Customer Resource Center</h1>
          <h2>Your access will need to be verified by one of our Customer Service Team members before access can be granted.</h2>
        </div>
      <?php endif; //end if role == customer-pending ?>




      <?php if( in_array( 'customer', $current_user->roles)
            || in_array( 'administrator', $current_user->roles )
            || in_array( 'editor', $current_user->roles )): ?>
        <?php $user_region = get_field('region', 'user_'.$current_user->ID); ?>
        <?php

          $meta_query = array();

          if( !is_preview() && $user_region !== 'all' ) {
            $meta_query = array(
              'relation'=>'or',
              array(
                'key' => 'region',
                'value' => 'ALL',
                'compare' => 'IS',
              ),
              array(
                'key' => 'region',
                'value' => $user_region,
                'compare' => 'IS',
              ),
            );
          }

          $args = array(
            'orderby' => 'name',
            'order'   => 'ASC',
            'hide_empty' => 1,
          );

          $cr_categories_all = get_terms('customer_resource_category', $args);
          $cr_categories = [];

          $products_all = get_terms('customer_resource_product', $args);
          $products = $products_all; //[];

          $langs_all = get_terms('customer_resource_language', $args);
          $langs = $langs_all; //[];

          foreach ($cr_categories_all as $cr_cat) {

            if( $search_cat !== 'all' && $search_cat !== $cr_cat->slug ) {
              continue;
            }

            $tax_query = array(
              'relation' => 'AND',
              array(
                'taxonomy' => 'customer_resource_category',
                'field' => 'slug',
                'terms' => $cr_cat->slug,
              ),
            );

            if( $search_prod !== 'all' ) {
              $tax_query[] = array(
                'taxonomy' => 'customer_resource_product',
                'field' => 'slug',
                'terms' => $search_prod,
              );
            }

            if( $crc_lang !== 'all' ) {
              $tax_query[] = array(
                'taxonomy' => 'customer_resource_language',
                'field' => 'slug',
                'terms' => $crc_lang,
              );
            }

            $args = array(
              'post_type' => 'customer_resources',
              'orderby' => 'title',
              'order' => 'ASC',
              'posts_per_page' => -1,
              's' => $search,
              'tax_query' => $tax_query,
              'meta_query' => $meta_query,
            );

            // pvar_dump($args);

            $query = new WP_Query($args);

              // pvar_dump($query);
            if( !empty( $args['s'] )) {
              relevanssi_do_query($query);
            }

            if ( $query->have_posts() ) {


              $cat = $cr_cat;
              $cr_categories[] = $cat;

            } else {
              // no posts found
            }

            wp_reset_query();
          }

          // foreach ($products_all as $cr_prod) {
          //    $args = array(
          //     'post_type' => 'customer_resources',
          //     'orderby' => 'title',
          //     'order' => 'ASC',
          //     'posts_per_page' => -1,
          //     'tax_query' => array(
          //       array(
          //         'taxonomy' => 'customer_resource_product',
          //         'field' => 'slug',
          //         'terms' => $cr_prod->slug,
          //       ),
          //     ),
          //     'meta_query' => $meta_query,
          //   );

          //   $query = new WP_Query($args);

          //   if ( $query->have_posts() ) {

          //     $prod = $cr_prod;
          //     $products[] = $prod;

          //   } else {
          //     // no posts found
          //   }

          //   wp_reset_query();
          // }

          /*
          echo '<pre>';
          print_r($cr_categories);
          echo '</pre>';
          */
          //$cr_categories = $cr_categories_all;
        ?>

        <?php /* get_sidebar(); */ ?>



        <div class="col-xs-12">

        <?php if( !is_preview() ): ?>
          <?php if( get_field( "user-show-welcome-message", 'user_'.$current_user->ID) ): ?>
            <div class="welcome-widget col-xs-12">
              <h1>Welcome!</h1>
              <p>On behalf of everyone at GenMark, we want to say how delighted we are to welcome you as a customer. As a GenMark customer, you will have access to comprehensive resources such as dedicated customer service representatives, expert technical support scientists, and an online Customer Resource Center that contains assay worksheets, package inserts, workflow diagrams, and the latest protocol updates.</p>
              <p><input type="checkbox" id="showWelcomeWidget" name="show-welcome-widget" ><b> CHECK THIS BOX TO PERMANENTLY DISMISS THIS MESSAGE</b></p>
            </div>
          <?php else: ?>
            <h1>Customer Resource Center</h1>
          <?php endif; ?>

          <div class="row crc-filter">

            <div class="col-xs-12 col-md-3">
              <form class="form" action="" method="get">

                <div class="form-group">
                  <label for="search_resources">Search Resources:</label><br>
                  <input name="search" id="search_resources" class="form-control" placeholder="Eg. Assay Manuals" value="<?php echo $search ? $search : ''; ?>" size="20" type="text">
                </div>

                <div class="form-group">
                  <label for="select_product">Product:</label><br>
                  <select class="form-control" id="select_product" name="select_product">
                    <option value="all">All</option>
                    <?php foreach( $products as $cr_prod): ?>
                      <option value="<?php echo $cr_prod->slug; ?>"<?php echo $cr_prod->slug == $search_prod ? ' selected="selected"' : ''; ?>><?php echo $cr_prod->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="select_resource">Resource Type:</label><br>
                  <select class="form-control" id="select_resource" name="select_resource">
                    <option value="all">All</option>
                    <?php foreach( $cr_categories_all as $cr_cat): ?>
                      <option value="<?php echo $cr_cat->slug; ?>"<?php echo $cr_cat->slug == $search_cat ? ' selected="selected"' : ''; ?>><?php echo $cr_cat->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="crc_language">Language:</label><br>
                  <select class="form-control" id="crc_language" name="crc_language">
                    <option value="all">All</option>
                    <?php foreach( $langs_all as $cr_lang): ?>
                      <option value="<?php echo $cr_lang->slug; ?>"<?php echo $cr_lang->slug == $crc_lang ? ' selected="selected"' : ''; ?>><?php echo $cr_lang->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <input class="btn btn-default btn-search" value="<?php echo strtoupper( __('Search','genmark')); ?>" type="submit">

              </form>
            </div>

          <?php endif; ?>
            <div class="col-xs-12 col-md-9">
          <?php if( $search_cat === 'all' && !empty( $cr_categories ) ): ?>

            <h2 style="margin-top: 0;">Select a Resource</h2>

              <div class="content-panel row">
                <?php foreach($cr_categories as $cr_cat): ?>

                  <?php

                    $tax_query = array(
                      array(
                        'taxonomy' => 'customer_resource_category',
                        'field' => 'slug',
                        'terms' => $cr_cat->slug,
                      ),
                    );

                    if( $search_prod !== 'all' ) {
                      $tax_query[] = array(
                        'taxonomy' => 'customer_resource_product',
                        'field' => 'slug',
                        'terms' => $search_prod,
                      );
                    }

                    if( $crc_lang !== 'all' ) {
                      $tax_query[] = array(
                        'taxonomy' => 'customer_resource_language',
                        'field' => 'slug',
                        'terms' => $crc_lang,
                      );
                    }

                    $args = array(
                      'post_type' => 'customer_resources',
                      'orderby' => 'title',
                      'order' => 'ASC',
                      'posts_per_page' => -1,
                      's' => $search,
                      'tax_query' => $tax_query,
                      'meta_query' => $meta_query,
                    );
                    $query = new WP_Query($args);

                    if( !empty( $args['s'] )) {
                      relevanssi_do_query($query);
                    }

                    if( $query->found_posts == 0 ) {
                      continue;
                    }

                  ?>

                <div class="col-xs-6 col-sm-4 fixheight">
                  <a class="crc-cat" data-location="Main" data-name="<?php echo $cr_cat->name; ?>" href="#<?php echo $cr_cat->slug; ?>">
                    <div class="content-panel-item">
                      <div class="content-panel-item__header">
                        <p class="upper-blue">Go to <i class="fa fa-angle-down pull-right"></i></p>
                      </div>
                      <div class="content-panel-item__content">
                        <h3><?php echo $cr_cat->name; ?></h3>
                      </div>
                      <div class="content-panel-item__file">
                        <?php if ( get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id) != 'default' ): ?>
                          <?php /* <i class="fa fa-file<?php echo '-'.get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id); ?>-o"></i> */ ?>
                          <img class="content-panel-item__file-icon" src="<?php echo get_template_directory_uri(); ?>/img/icon-<?php echo get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id); ?>.png" alt="<?php echo  get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id); ?> file icon">
                          <img class="content-panel-item__file-icon--hover" src="<?php echo get_template_directory_uri(); ?>/img/icon-<?php echo get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id); ?>_hover.png" alt="<?php echo  get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id); ?> file icon">
                        <?php else: ?>
                          <i style="font-size: 17px;" class="fa fa-file-o"></i>
                        <?php endif; ?>
                      </div>
                    </div>
                  </a>
                </div>
                <?php endforeach; ?>
              </div>
            </div><!-- /.col-xs-12 col-md-9 -->
          </div><!-- /.row -->
          <?php endif; ?>

          <?php if( empty( $cr_categories )): ?>
            <hr />
            <p>Sorry, no results found for this query.</p>
          <?php endif; ?>
          <hr>

          <?php
            $found_results = 0;
          ?>
          <?php foreach($cr_categories as $cr_cat): ?>

            <?php
              if( $search_cat !== 'all' && $cr_cat->slug !== $search_cat ) {
                continue;
              }
            ?>

            <?php if ( get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id) != 'default' ):
              $icon = '<img src="'. get_template_directory_uri() .'/img/icon-'. get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id) .'.png" alt="'. get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id). ' file icon">';
              //$i_class = "fa fa-file-" . get_field('cr-cat-file-format', $cr_cat->taxonomy . '_' . $cr_cat->term_id) . "-o";
                  else:
              $icon = '<i class="fa fa-file-o"></i>';
              //$i_class = "fa fa-file-o";
                  endif; ?>
              <?php

                $tax_query = array(
                  array(
                    'taxonomy' => 'customer_resource_category',
                    'field' => 'slug',
                    'terms' => $cr_cat->slug,
                  ),
                );

                if( $search_prod !== 'all' ) {

                  $tax_query['relation'] = 'AND';

                  $tax_query[] = array(
                    'taxonomy' => 'customer_resource_product',
                    'field' => 'slug',
                    'terms' => $search_prod,
                  );

                }

                if( $crc_lang !== 'all' ) {

                  $tax_query['relation'] = 'AND';

                  $tax_query[] = array(
                    'taxonomy' => 'customer_resource_language',
                    'field' => 'slug',
                    'terms' => $crc_lang,
                  );

                }

                $args = array(
                  'post_type' => 'customer_resources',
                  'orderby' => 'title',
                  'order' => 'ASC',
                  'posts_per_page' => -1,
                  's' => $search,
                  'tax_query' => $tax_query,
                  'meta_query' => $meta_query,
                );

                $query = new WP_Query($args);

                if( !empty( $args['s'] )) {
                  relevanssi_do_query($query);
                }

                if( $query->have_posts() ):
                  $found_results++;
                  ?>


            <section class="crc-category row" id="<?php echo $cr_cat->slug; ?>" >
              <div class="col-xs-12">
                <h2><?php echo $cr_cat->name; ?><a href="#" class="upper-blue upper-blue--real pull-right">back to top <i class="fa fa-angle-up"></i></a></h2>
              </div>

                <?php
                  while( $query->have_posts() ):
                    $query->the_post();
                  ?>

                    <div class="col-xs-12 col-sm-6">
                      <?php $file_url = get_field('cr-file') ? get_field('cr-file')['url'] : '#'; ?>

                      <?php
                        $title = get_the_title();

                        if( is_preview() && get_the_ID() === $real_id ) {

                          $title = $real_title;

                        }

                        $title = genmark_upper( $title );
                      ?>

                      <a class="crc-download" data-name="<?php echo $title; ?>" target="_blank" href="<?php echo $file_url; ?>">
                        <p class="upper-blue">
                          <span class="icon pull-left"><?php echo $icon; ?></span>
                          <span class="title"<?php if($file_url == '#') echo 'style="color:#aaa !important;"'; ?>><?php echo $title; ?></span>
                        </p>
                      </a>
                    </div>


                  <?php
                  endwhile;
                  ?>

                </section>
                <hr>
                  <?php
                endif;
                ?>
                <?php
                wp_reset_query();
              ?>
          <?php endforeach; ?>

        <?php if( empty( $found_results )): ?>
          <!-- <p>Sorry, no results found for this query.</p> -->
        <?php endif; ?>

        </div>
      <?php endif; //end if role == customer / admin ?>


    <?php endif; //end if is_user_logged_in ?>




    <?php if( !is_user_logged_in() ): ?>
    <div class="col-xs-12">
      <h2>You have to log in to view this page.</h2>
      <p><a href="<?php echo get_home_url(); ?>/create-account/">create account</a> | <a href="<?php echo get_home_url(); ?>/login/">login</a></p>
    </div>
    <?php endif; ?>


  </div>
</div>

<script>
// TODO: move the script to js file
var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";

$( "#showWelcomeWidget" ).click(function() {
  var bool_val = $( "#showWelcomeWidget" ).is(':checked') ? false : true;
  var user_id = 'user_'+ <?php echo $current_user->ID; ?>;
  updateWelcomeWidgetSetting( bool_val , user_id );
});

function updateWelcomeWidgetSetting(bool_val, user_id){

  $.ajax({
    url: ajaxurl,
    data: {
      'action':'update_crc_welcome_widget_setting',
      'bool_val' : bool_val,
      'user_id': user_id
    },
    success:function(data) {  console.log(data);  },
    error: function(errorThrown){  console.log(errorThrown); }
  });

  $( ".welcome-widget" ).after('<h1>Customer Resource Center</h1>');
  $( ".welcome-widget" ).slideUp("normal", function() { $(this).remove(); } );

}
</script>


<?php get_footer(); ?>
