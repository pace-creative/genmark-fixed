<?php /* Template Name: Webinars */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

        <table class="item-list" summary="Sortable Table (Click a column header to sort)">
          
          <?php

          $paged    = ( get_query_var('paged')) ? get_query_var('paged') : 1;
          $orderby  = (!get_query_var('ob') || get_query_var('ob') == 'title') ? 'title' : 'meta_value';
          $meta_key = ( get_query_var('ob') != 'title') ? get_query_var('ob') : '';
          $order    = ( get_query_var('o')) ? get_query_var('o') : 'asc';

          //override sort by title if no query var. set default to sort by date 
          if(!get_query_var('ob')){
            $orderby = 'meta_value';
            $meta_key = 'webinar-date';
            $order = 'desc';
          }

          $args = array(
            'post_type' => 'webinars',
            'orderby' => $orderby, // title | meta_value
            'meta_key' => $meta_key, // conference-location
            'order' => $order,
            'posts_per_page' => 5,
            'paged' => $paged,
          );

          $query = new WP_Query($args);
          if( $query->have_posts() ):
            ?>
            <thead>
              <tr>
                <th><a class="sortable<?php if($meta_key=='webinar-date'){ if($order=='asc') echo' asc'; else echo' desc'; } ?>" href="?paged=1&ob=webinar-date<?php if($meta_key=='webinar-date'){ if($order=='asc') echo '&o=desc'; else echo '&o=asc'; } ?>">Date <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
                <th><a class="sortable<?php if($orderby=='title'){ if($order=='asc') echo' asc'; else echo' desc'; } ?>" href="?paged=1&ob=title<?php if($orderby=='title'){ if($order=='asc') echo '&o=desc'; else echo '&o=asc'; } ?>">Title <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
                <th><a class="sortable<?php if($meta_key=='webinar-presenters'){ if($order=='asc') echo' asc'; else echo' desc'; } ?>" href="?paged=1&ob=webinar-presenters<?php if($meta_key=='webinar-presenters'){ if($order=='asc') echo '&o=desc'; else echo '&o=asc'; } ?>">Presenters <abbr style="border-style: none;" class="sort-icon" title=""><span class="caret"></span></abbr></a></th>
              </tr>
            </thead>
            <tbody>
            <?php
            while( $query->have_posts() ):
              $query->the_post();
              echo get_template_part('content','webinar-item');
            endwhile;
          ?>
          </tbody>
        </table>

          <?php
          if ( $query->max_num_pages > 1 ) : 

            $args = array(
              'format' => '?paged=%#%', //'page/%#%'
              'current' => max( 1, get_query_var('paged') ),
              'prev_next' => false,
              'show_all' => true,
              'total' => $query->max_num_pages,
              'type' => 'array'
            ); 

            $pages = paginate_links( $args );
            if( is_array( $pages ) ) {
              echo '<ul class="pagination">';
              foreach ( $pages as $page ) {
                echo '<li class="pagination__item">'.str_replace("page-numbers", "btn btn-default", $page).'</li>';
              }
             echo '</ul>';
            }
          endif;
          wp_reset_query();
        endif; 
        ?>
        
        <?php echo get_template_part('content','footnote'); ?>
        
   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>