<?php /* Template Name: Request Information */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <?php the_content(); ?>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php
  if ( WP_ENV == 'production' ) {
    ?>
      <script type="text/javascript">llfrmid=22309</script>
      <script type="text/javascript" src="https://formalyzer.com/formalyze_init.js"></script>
      <script type="text/javascript" src="https://formalyzer.com/formalyze_call_secure.js"></script>
    <?php
  }
?>

<?php get_footer(); ?>
