<?php /* Template Name: Contact */ ?>
<?php get_header(); ?>
<?php
  if( ICL_LANGUAGE_CODE == 'int' ) {
    $contact_text_1 = get_field('contact-content-eu');
    $contact_text_2 = get_field('contact-content-us');
    $lat = '47.176349';
    $lng = '8.507733';
  } else {
    $contact_text_1 = get_field('contact-content-us');
    $contact_text_2 = get_field('contact-content-eu');
    $lat = get_field('contact-address')['lat'];
    $lng = get_field('contact-address')['lng'];
  }

?>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript">

  function initialize() {

    lat = 49.282470;
    lng = -123.103423;

    if( arguments.length >= 1 ) {
      lat = arguments[0];
    }
    if( arguments.length >= 2 ) {
      lng = arguments[1];
    }

    genmark = new google.maps.LatLng(lat, lng);
    center = new google.maps.LatLng(lat, lng);

    var stylesArray = [{
      "stylers": [{
        "visibility": "simplified"
      }, {
        "weight": 4.1
      }, {
        "gamma": 1
      }, {
        "lightness": 3
      }, {
        "saturation": -255
      }]
    }];

    var mapOptions = {
      center: center,
      zoom: 11,
      navigationControl: false,
      mapTypeControl: false,
      scrollwheel: false,
      // zoomControl: false,
      streetViewControl: false,
      styles: stylesArray
    };

    var map = new google.maps.Map(document.getElementById('contactMap'),
      mapOptions);

    var marker = new google.maps.Marker({
      position: genmark,
      map: map,
      icon: php_vals.stylesheet_url + '/img/genmark_pin.png'
    });

    /*
    var marker = new google.maps.Marker({
      position: pacebc,
      map: map,
      icon: php_vals.stylesheet_url + '/images/map_marker.png'
    });

    contentString = '<div style="line-height: 1.35; overflow: hidden; white-space: nowrap;"><b>Pace Creative BC</b><br />' +
      '#521, 55 East Cordova Street<br />' +
      'Vancouver, BC, V6A 0A5<br />' +
      '<a class="map-link" href="https://www.google.ca/maps/place/55+E+Cordova+St,+Vancouver,+BC+V6A+0A5" target="_blank">View in Google Maps</a></div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map, marker);
    });
    */
  }

  jQuery(document).ready(function() {
    initialize(<?php echo $lat; ?>,
               <?php echo $lng; ?>);
  });
</script>


<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9 content-contact">

      <?php
        $location = get_field('contact-address');
      ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?php echo $contact_text_1; ?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <div class="embed-responsive embed-responsive-1by1">
            <div id="contactMap" class="embed-responsive-item" ></div>
          </div>
        </div>
      </div>

      <hr>

      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?php echo $contact_text_2; ?>
          <?php echo get_template_part('content','footnote'); ?>
        </div>
      </div>

    </div><!-- .col-xs-12 -->

  </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>
