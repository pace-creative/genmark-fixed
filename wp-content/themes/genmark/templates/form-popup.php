<?php /* Template Name: Form Popup */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <?php the_content(); ?>

      <?php
        if ( WP_ENV == 'production' ) {
          ?>
            <script type="text/javascript">llfrmid=22309</script>
            <script type="text/javascript" src="https://formalyzer.com/formalyze_init.js"></script>
            <script type="text/javascript" src="https://formalyzer.com/formalyze_call_secure.js"></script>
          <?php
        }
      ?>

      <div id="form-pdf-popup" class="modal--req-info modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-header clearfix"><button class="close" type="button" data-dismiss="modal">Close ×</button></div>
            <div class="modal-body">
              <?php

                $text = get_field('popup_text');
                $files = get_field('form_files');

                function get_type_config( $type ) {

                  $return = array();

                  switch( $type ) {

                    case 'ics';
                      $return['icon_class'] = 'fa-calendar';
                      break;

                    case 'pdf';
                    default;
                      $return['icon_class'] = 'fa-file-pdf-o';
                      break;

                  }

                  return $return;

                }

              ?>

              <p><span class="upper-blue"><?php echo $text; ?></span></p>
              <p>
                <?php foreach( $files as $file ): ?>
                  <?php
                    $config = get_type_config( $file['file_type'] );
                    $linktext = $file['link_text'];
                    $download = $file['file'];
                  ?>
                  <i class="fa <?php echo $config['icon_class']; ?>" aria-hidden="true"></i> <a href="<?php echo $download['url']; ?>" target="_blank"><?php echo $linktext; ?></a><br />
                <?php endforeach; ?>
              </p>
            </div>
          </div>
        </div>
      </div>

      <script>
        var wpcf7Elm = document.querySelector( '.wpcf7' );

        if(wpcf7Elm) {
          wpcf7Elm.className += ' haspopup';
        }

        wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
          $('#form-pdf-popup').modal('show');
        }, false );
      </script>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>
