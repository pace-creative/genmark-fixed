<?php /* Template Name: Lab Automation */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <?php the_content(); ?>


      <div id="lab-auto-pdf-popup" class="modal--req-info modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-header clearfix"><button class="close" type="button" data-dismiss="modal">Close ×</button></div>
            <div class="modal-body">
              <p><span class="upper-blue">Your message was sent successfully. Thank you.</span></p>
              <p>
                <?php
                  $file = get_field('file');
                ?>
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <a href="<?php echo $file['url']; ?>" target="_blank">Download PDF</a>
              </p>
            </div>
          </div>
        </div>
      </div>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>
