<?php /* Template Name: Solution */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <?php
        $title = get_the_title();
        $video = get_field('solution-video');
        $pdf = get_field('solution-pdf');
        $footer_widgets = get_field( 'solution-widget' );
      ?>

      <h1 class="page-title"><?php echo $title; ?></h1>

      <?php if( !empty( $pdf ) || !empty( $video )): ?>
      <p>
        <?php if( !empty( $pdf )): ?>
          <a href="<?php echo $pdf['url']; ?>" target="_blank" class="btn btn-default fa-icon fa-icon--pdf"><?php echo genmark_upper( __('Product Literature','genmark')); ?></a>
        <?php endif; ?>

        <?php if( !empty( $video )): ?>
          <a href="#" class="btn btn-default fa-icon fa-icon--play" data-toggle="modal" data-target="#video-modal-<?php the_ID(); ?>"><?php echo genmark_upper( __( 'Watch the ' . (get_field( 'alt_title' ) ? get_field( 'alt_title' ) : get_the_title()) . ' video', 'genmark') ) ; ?></a>
        <?php endif; ?>
      </p>
      <?php endif; ?>

      <?php if( !empty( $video ) ): ?>
      <div id="video-modal-<?php the_ID(); ?>" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-video" role="document">
          <div class="modal-content">
            <div class="modal-header clearfix">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="pauseModalVideo('system_video_<?php the_ID(); ?>')"><span aria-hidden="true"><?php _e('Close Video','genmark'); ?> &times;</span></button>
            </div>
            <div class="modal-body clearfix">
              <video id="system_video_<?php the_ID(); ?>" class="video-js vjs-default-skin vjs-big-play-centered"
                controls preload="auto" width="100%" height="450"
                poster="<?php echo get_stylesheet_directory_uri(); ?>/img/video-poster.jpg"
                data-setup='{}'>
                <source src="<?php echo $video['url']; ?>" type='video/mp4' />
                <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
              </video>
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>


      <?php the_content(); ?>

      <?php
        if( !empty( $footer_widgets )) {

          echo '<br>';

          if( in_array( 'solution-eplex', $footer_widgets )) {

            $eplex_page_id = apply_filters( 'wpml_object_id', 5, 'page' );
            ?>
            <?php /*
            <div class="content-widget">
              <h2 class="content-widget__title"><?php _e('Available on ePlex','genmark'); ?></h2>
              <a href="#" class="btn btn-default"><?php _e('View System','genmark'); ?></a>
              <a href="#" class="btn btn-default fa-icon fa-icon--play"><?php _e('Play Video','genmark'); ?></a>
            </div>
            */ ?>
            <div class="content-widget content-widget--fw media media--stackable">
              <div class="content-widget--fw__img media-left media-middle">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/eplex-125x160.jpg" alt="" class="content-widget__img">
              </div>
              <div class="content-widget--fw__title media-body media-middle">
                <h3><?php _e('Available Only on ePlex','genmark'); ?></h3>
              </div>
              <div class="content-widget--fw__btn-set media-right media-middle">
                <a href="<?php echo get_permalink( $eplex_page_id ); ?>" class="btn btn-default"><?php echo strtoupper( __('View System','genmark')); ?></a>
                <a href="#" id="GenMarkDx-ePlex-Overview" data-system-id="<?php echo $eplex_page_id; ?>" class="btn btn-default launch-system-video widget fa-icon fa-icon--play"><?php echo strtoupper( __('Play Video','genmark')); ?></a>
              </div>
            </div>

            <?php
          }

          if( in_array( 'solution-xt8', $footer_widgets )) {

            $xt8_page_id = apply_filters( 'wpml_object_id', 12, 'page' );
            ?>
            <?php /*
            <div class="content-widget">
              <h2 class="content-widget__title"><?php _e('Available on XT-8','genmark'); ?></h2>
              <a href="#" class="btn btn-default"><?php _e('View System','genmark'); ?></a>
              <a href="#" class="btn btn-default fa-icon fa-icon--play"><?php _e('Play Video','genmark'); ?></a>
            </div>
            */ ?>

            <div class="content-widget content-widget--fw media media--stackable">
              <div class="content-widget--fw__img media-left media-middle">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/xt8-feature-125x106.jpg" alt="" class="content-widget__img">
              </div>
              <div class="content-widget--fw__title media-body media-middle">
                <h3><?php _e('Available on XT-8','genmark'); ?></h3>
              </div>
              <div class="content-widget--fw__btn-set media-right media-middle" >
                <a href="<?php echo get_permalink( $xt8_page_id ); ?>" class="btn btn-default"><?php echo strtoupper( __('View System','genmark')); ?></a>
                <a href="#" id="GenMarkDx-XT8" data-system-id="<?php echo $xt8_page_id; ?>" class="btn btn-default launch-system-video widget fa-icon fa-icon--play"><?php echo strtoupper( __('Play Video','genmark')); ?></a>
              </div>
            </div>
            <?php
          }

        }
      ?>

      <?php echo get_template_part('content','footnote'); ?>


   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>
