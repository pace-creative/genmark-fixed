<?php /* Template Name: ePlex Pipeline Panel */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

  </div><!-- .col-xs-12 -->
</div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>