<?php /* Template Name: Panel Landing Page */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <?php the_content(); ?>

      <?php

        // get page children
        $args = array(
          'post_parent' => get_the_ID(),
          'post_type' => 'page',
          'numberposts' => -1,
          'post_status' => 'publish',
          'orderby' => 'menu_order',
          'order' => 'ASC',
        );

        $the_query = new WP_Query($args);

        if( $the_query->have_posts() ) {
          ?>
          <div class="row">
          <?php
          while( $the_query->have_posts() ) {
            $the_query->the_post();

            // make sure child is using Panel template
            $template = get_post_meta(get_the_ID(), '_wp_page_template', true);

            if( $template != 'templates/panel.php' || get_the_title() == 'Blood Culture Identification Panels' ) {
              continue;
            }

            ?>

            <div class="col-xs-12 col-sm-4">
              <div class="content-widget content-widget--panels fakelink" data-href="<?php the_permalink(); ?>">
                <h2 class="content-widget__title"><?php the_title(); ?></h2>
                <?php
                  if( has_post_thumbnail() ) {
                    $thumb_url = wp_get_attachment_url( get_post_thumbnail_id() );
                    ?>
                    <img class="content-widget__img" src="<?php echo $thumb_url; ?>" alt="">
                    <?php
                  }
                ?>
                <p class="content-widget__text">
                  <?php /* <a href="<?php the_permalink(); ?>"><?php _e('View Panel','genmark'); ?> ></a> */?>
                  <a href="<?php the_permalink(); ?>"><?php _e('View Panel','genmark'); ?></a>
                </p>
              </div>
            </div>

            <?php
          }
          ?>
          </div>
        <?php
        }

        wp_reset_postdata();

      ?>

      <?php the_field('additional_content'); ?>

      <?php echo get_template_part('content','footnote'); ?>

  </div><!-- .col-xs-12 -->
</div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>
