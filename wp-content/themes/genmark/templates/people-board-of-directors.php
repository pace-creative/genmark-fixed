<?php /* Template Name: Board of Directors */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">
      <h1><?php the_title(); ?></h1>

      <?php the_content(); ?>

      <?php
      $tax_query = array(
                    array(
                      'taxonomy' => 'person_category',
                      'field' => 'slug',
                      'terms' => 'board-of-directors',
                    ),
                  );

        // Display Chairman first
        $args = array(
          'post_type' => 'people',
          'orderby' => 'menu_order',
          'order' => 'asc',
          'posts_per_page' => -1,
          'tax_query' => $tax_query,
          'meta_query' => array(
            array(
              'key' => 'title',
              'value' => 'Chairman',
              'compare' => 'LIKE',
            ),
          ),
        );
        $query = new WP_Query($args);
        if( $query->have_posts() ):
          while( $query->have_posts() ):
            $query->the_post();
            echo get_template_part('content','team-item');

          endwhile;
        endif;
        wp_reset_query();
      ?>

      <?php
        // Display the other BOD
        $args = array(
          'post_type' => 'people',
          'orderby' => 'menu_order',
          'order' => 'asc',
          'posts_per_page' => -1,
          'tax_query' => $tax_query,
          'meta_query' => array(
            array(
              'key' => 'title',
              'value' => 'Chairman',
              'compare' => 'NOT LIKE',
            ),
          ),

        );
        $query = new WP_Query($args);
        if( $query->have_posts() ):
          while( $query->have_posts() ):
            $query->the_post();
            echo get_template_part('content','team-item');

          endwhile;
        endif;
        wp_reset_query();
      ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>