<?php /* Template Name: Management */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">
      <h1><?php the_title(); ?></h1>

      <?php the_content(); ?>

      <?php
        $args = array(
          'post_type' => 'people',
          'orderby' => 'menu_order',
          'order' => 'ASC',
          'posts_per_page' => -1,
          'tax_query' => array(
            array(
              'taxonomy' => 'person_category',
              'field' => 'slug',
              'terms' => 'management',
            ),
          ),
        );

        $query = new WP_Query($args);
        if( $query->have_posts() ):
          while( $query->have_posts() ):
            $query->the_post();
            echo get_template_part('content','team-item');

          endwhile;
        endif;
      ?>
   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>