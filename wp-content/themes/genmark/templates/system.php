<?php /* Template Name: System */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <?php
        $video = get_field('video');
        $intro_text = get_field('intro_text');
        $pdf = get_field('product_literature');
        $thumb = false;
        $gallery = get_field('gallery');

        if( has_post_thumbnail() ) {
          $thumb = get_the_post_thumbnail();
        }
      ?>

      <?php if( !empty( $video ) || !empty( $pdf ) || $intro_text || $thumb ): ?>
        <div class="media system-header">
          <?php if( $thumb ): ?>
          <?php /*<div class="media-left">*/ ?>
          <div class="pull-left">

            <?php if( !empty( $gallery )): ?>

              <?php
                $gallery_first = get_field('gallery_first');
                $gallery_first -= 1;

                if( isset( $_GET['gallery'] )) {
                  $gallery_first = $_GET['gallery'];
                }

                if( !$gallery_first || !is_int( $gallery_first ) || $gallery_first < 0 || $gallery_first >= count( $gallery )) {
                  $gallery_first = 0;
                }
              ?>

              <div class="gallery">
                <?php foreach( $gallery as $key => $image ): ?>
                  <img class="gallery-img <?php echo $key === $gallery_first ? 'active' : ''; ?>" id="gallery-img-<?php echo $key; ?>" src="<?php echo $image['url']; ?>" alt="" />
                <?php endforeach; ?>
              </div><!-- /.gallery -->

              <ul class="gallery-thumbs">

                <?php foreach( $gallery as $key => $image ): ?>
                  <li class="gallery-thumbs__item">
                    <img class="gallery-thumbs__thumb <?php echo $key === $gallery_first ? 'active' : ''; ?>" id="gallery-img-<?php echo $key; ?>" src="<?php echo $image['sizes']['gallery-thumb']; ?>" data-item="<?php echo $key; ?>" alt="" />
                  </li>
                <?php endforeach; ?>

              </ul><!-- /.gallery-thumbs -->

            <?php else: ?>
              <?php echo $thumb; ?>
            <?php endif; ?>


          </div>
          <?php endif; ?>

          <div class="media-body">

            <?php if( $intro_text ): ?>
              <div class="system-header__text">

                <?php if( get_the_id() == 5 ): ?>
                  <div class="gallery-text <?php echo 0 === $gallery_first ? 'active' : ''; ?>" id="gallery-text-0">
                    <p><strong>ePlex<sup>®</sup> NP (Near Patient)</strong></p>
                    <ul>
                      <li>Random and continuous access</li>
                      <li>3 test bays</li>
                      <li>36 patient samples per day*</li>
                      <li>Automated QC monitoring</li>
                      <li>Bi-directional LIS connectivity</li>
                      <li>Epidemiology reporting</li>
                      <li>24/7 support and remote access</li>
                    </ul>
                    <p class="small">* Based on 24 hour day</p>
                  </div>

                  <div class="gallery-text <?php echo 1 === $gallery_first ? 'active' : ''; ?>" id="gallery-text-1">
                    <p><strong>ePlex<sup>®</sup> 1 Tower</strong></p>
                    <ul>
                      <li>Random and continuous access</li>
                      <li>6 test bays</li>
                      <li>72 patient samples per day*</li>
                      <li>Automated QC monitoring</li>
                      <li>Bi-directional LIS connectivity</li>
                      <li>Epidemiology reporting</li>
                      <li>24/7 support and remote access</li>
                    </ul>
                    <p class="small">* Based on 24 hour day</p>
                  </div>

                  <div class="gallery-text <?php echo 2 === $gallery_first ? 'active' : ''; ?>" id="gallery-text-2">
                    <p><strong>ePlex<sup>®</sup> 2 Tower</strong></p>
                    <ul>
                      <li>Random and continuous access</li>
                      <li>12 test bays</li>
                      <li>144 patient samples per day*</li>
                      <li>Automated QC monitoring</li>
                      <li>Bi-directional LIS connectivity</li>
                      <li>Epidemiology reporting</li>
                      <li>24/7 support and remote access</li>
                    </ul>
                    <p class="small">* Based on 24 hour day</p>
                  </div>

                  <div class="gallery-text <?php echo 3 === $gallery_first ? 'active' : ''; ?>" id="gallery-text-3">
                    <p><strong>ePlex<sup>®</sup> 3 Tower</strong></p>
                    <ul>
                      <li>Random and continuous access</li>
                      <li>18 test bays</li>
                      <li>216 patient samples per day*</li>
                      <li>Automated QC monitoring</li>
                      <li>Bi-directional LIS connectivity</li>
                      <li>Epidemiology reporting</li>
                      <li>24/7 support and remote access</li>
                    </ul>
                    <p class="small">* Based on 24 hour day</p>
                  </div>

                  <div class="gallery-text <?php echo 4 === $gallery_first ? 'active' : ''; ?>" id="gallery-text-4">
                    <p><strong>ePlex<sup>®</sup> 4 Tower</strong></p>
                    <ul>
                      <li>Random and continuous access</li>
                      <li>24 test bays</li>
                      <li>288 patient samples per day*</li>
                      <li>Automated QC monitoring</li>
                      <li>Bi-directional LIS connectivity</li>
                      <li>Epidemiology reporting</li>
                      <li>24/7 support and remote access</li>
                    </ul>
                    <p class="small">* Based on 24 hour day</p>
                  </div>
                <?php else: ?>
                  <?php echo $intro_text; ?>
                <?php endif; ?>

              </div>
            <?php endif; ?>

            <?php if( !empty( $video ) || !empty( $pdf )): ?>

              <div id="video-modal-<?php the_ID(); ?>" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-video" role="document">
                  <div class="modal-content">
                    <div class="modal-header clearfix">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="pauseModalVideo('system_video_<?php the_ID(); ?>')"><span aria-hidden="true"><?php _e('Close Video','genmark'); ?> &times;</span></button>
                    </div>
                    <div class="modal-body clearfix">
                      <video id="system_video_<?php the_ID(); ?>" class="video-js vjs-default-skin vjs-big-play-centered"
                        controls preload="auto" width="100%" height="450"
                        poster="<?php echo get_stylesheet_directory_uri(); ?>/img/video-poster.jpg"
                        data-setup='{}'>
                        <source src="<?php echo $video['url']; ?>" type='video/mp4' />
                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                      </video>
                    </div>
                  </div>
                </div>
              </div>

              <p class="system-header__btn">

                <?php if( !empty( $video )): ?>
                  <a href="#" target="_blank" data-toggle="modal" data-target="#video-modal-<?php the_ID(); ?>" class="btn btn-default fa-icon fa-icon--play"><?php echo strtoupper( __('Watch the video','genmark')); ?></a>
                <?php endif; ?>

                <?php if( !empty( $pdf )): ?>
                  <a href="<?php echo $pdf['url']; ?>" target="_blank" class="btn btn-default fa-icon fa-icon--pdf"><?php echo strtoupper( __('Product Literature','genmark')); ?></a>
                <?php endif; ?>

              </p>
            <?php endif; ?>

          </div>
        </div>
      <?php endif; ?>


      <?php the_content(); ?>

      <?php

        $footer_widgets = get_field( 'footer_widget' );

        if( !empty( $footer_widgets )) {

          if( in_array( 'eplex', $footer_widgets )) {
            ?>

            <div class="content-widget content-widget--solution">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/eplex-panel-sample.jpg" alt="" class="content-widget__img">
              <h3 class="content-widget__title"><?php _e('Comprehensive ePlex Solutions','genmark'); ?></h3>
              <a href="#" class="btn btn-default"><?php strtoupper(_e('View Panels','genmark')); ?></a>
            </div>

            <?php
          }

          if( in_array( 'xt8', $footer_widgets )) {
            ?>
            <?php /*
            <div class="content-widget content-widget--solution">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/esensor_cartridge.jpg" alt="" class="content-widget__img">
              <h3 class="content-widget__title"><?php _e('Comprehensive XT-8 Solutions','genmark'); ?></h3>
              <a href="#" class="btn btn-default"><?php _e('View Panels','genmark'); ?></a>
            </div>
            */ ?>

            <div class="content-widget content-widget--fw media media--stackable">
              <div class="content-widget--fw__img media-left media-middle">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/esensor_cartridge.jpg" alt="" class="content-widget__img">
              </div>
              <div class="content-widget--fw__title media-body media-middle">
                <h3><?php _e('Comprehensive XT-8 Solutions','genmark'); ?></h3>
              </div>
              <div class="content-widget--fw__btn media-right media-middle">
                <?php $xt8_url = get_permalink( apply_filters('wpml_object_id', 26, 'page')); ?>
                <a href="<?php echo $xt8_url; ?>" class="btn btn-default"><?php strtoupper(_e('View Panels','genmark')); ?></a>
              </div>
            </div>
            <?php
          }

          if( in_array( 'comprehensive', $footer_widgets )) {
            ?>

            <h2><?php _e('Comprehensive Multiplex Panels','genmark'); ?></h2>

            <?php

                // get ePlex's children
                $args = array(
                  'post_parent' => 16,
                  'post_type' => 'page',
                  'numberposts' => -1,
                  'post_status' => array('publish','private'),
                  'orderby' => 'menu_order',
                  'order' => 'ASC',
                );

                $the_query = new WP_Query($args);

                if( $the_query->have_posts() ):

                  while( $the_query->have_posts() ):
                    $the_query->the_post();
                    $template = get_post_meta(get_the_ID(), '_wp_page_template', true);

                    if( $template != 'templates/panel.php' || get_the_title() == 'Blood Culture Identification Panels' ) {
                      continue;
                    }

                    ?>


                    <div class="content-widget content-widget--fw media media--stackable">
                      <div class="content-widget--fw__img media-left media-middle">
                        <?php
                          if( has_post_thumbnail() ) {
                            $thumb_url = wp_get_attachment_url( get_post_thumbnail_id() );
                            the_post_thumbnail('thumb');
                          }
                        ?>
                      </div>
                      <div class="content-widget--fw__title media-body media-middle">
                        <h3><?php
                          $title = get_the_title();
                          $title = str_replace('*', '', $title);

                          echo $title;
                        ?></h3>
                        <?php
                        $excerpt = get_field('exceprt');
                        if( !empty($excerpt) ): ?>
                          <p style="margin: .5em 0;">
                            <?php echo $excerpt; ?>
                          </p>
                        <?php endif; ?>
                      </div>
                      <div class="content-widget--fw__btn media-right media-middle">
                        <a href="<?php echo the_permalink(); ?>" class="btn btn-default"><?php echo strtoupper( __('View Panel','genmark') ); ?></a>
                      </div>
                    </div>
                    <?php
                    endwhile;
                  endif;
                ?>

          <?php
          }
          if( in_array( 'in-development', $footer_widgets )) {
          ?>


          <div class="content-widget content-widget--development">
            <div class="row">
              <div class="col-xs-12">
                <h3 class="pull-left"><?php _e('ePlex Panels in Development','genmark'); ?></h3>
                <p class="content-widget--development-btn">
                  <a href="<?php echo get_permalink( apply_filters('wpml_object_id', 24, 'page') ); ?>" class="btn btn-default"><?php echo __('VIEW ePLEX PIPELINE','genmark'); ?></a>
                </p>
              </div>



              <?php
                // get eplex-pipeline's children
                $args = array(
                  'post_parent' => 24,
                  'post_type' => 'page',
                  'numberposts' => -1,
                  'post_status' => array('publish', 'private'),
                  'orderby' => 'menu_order',
                  'order' => 'ASC',
                );

                $the_query = new WP_Query($args);

                if( $the_query->have_posts() ) {

                  while( $the_query->have_posts() ) {
                    $the_query->the_post();

                    if(get_field('hide_from_pipeline_page')) {
                      continue;
                    }

                    $status = get_post_status();

                    ?>

                    <div class="col-xs-12 col-sm-6">

                      <div class="pull-left">
                        <?php
                          if( has_post_thumbnail() ) {
                            $thumb_url = wp_get_attachment_url( get_post_thumbnail_id() );
                          }
                          $title = str_replace( 'Private: ', '', get_the_alt_title() );
                          $title = genmark_upper( $title );

                          $intro = get_field('exceprt');
                          $text = get_the_content();

                          if( !empty( $intro )) {
                            $text = $intro;
                          }

                          $text = strip_tags( $text );

                        ?>
                        <img class="content-widget__img" src="<?php echo $thumb_url; ?>" alt="content-widget__img" width="75">
                      </div>

                      <div class="media-body row">
                        <p class="content-widget--development-content">
                          <span class="upper-blue"><?php echo $title; ?></span><br>
                          <?php echo $text; ?>
                          <?php
                            if( $status == 'publish' ): ?>
                              <br>
                              <a href="<?php the_permalink(); ?>"><?php _e('View Panel','genmark'); ?></a>
                            <?php
                            endif;
                          ?>
                        </p>
                      </div>

                    </div>
                    <?php
                  }
                }
                wp_reset_postdata();
              ?>

            <?php /*
              <div class="col-xs-12 col-sm-6">
                <div class="pull-left">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cns_featured_t-75x64.jpg" alt="" class="content-widget__img">
                </div>
                <div class="media-body row">
                  <p class="content-widget--development-content">
                    <span class="upper-blue">CENTRAL NERVOUS SYSTEM PANEL (CNS)</span><br>
                    Bacterial, viral, and fungal targets associated with central nervous system infections from a cerebrospinal fluid sample.
                  </p>
                </div>
              </div>

              <div class="col-xs-12 col-sm-6">
                <div class="pull-left">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/fp_featured_t-75x64.jpg" alt="" class="content-widget__img">
                </div>
                <div class="media-body">
                  <p class="content-widget--development-content">
                    <span class="upper-blue">FUNGAL PATHOGEN PANEL (FP)</span><br>
                    Fungal targets associated with bloodstream infections from positive blood culture.
                  </p>
                </div>
              </div>

              <div class="col-xs-12 col-sm-6">
                <div class="pull-left">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/gi_featured_t-75x64.jpg" alt="" class="content-widget__img">
                </div>
                <div class="media-body">
                  <p class="content-widget--development-content">
                    <span class="upper-blue">GASTROINTESTINAL PATHOGEN PANEL (GI)</span><br>
                    Bacterial, viral, and parasitic targets associated with gastrointestinal infections from a stool sample.
                  </p>
                </div>
              </div>

              <div class="col-xs-12 col-sm-6">
                <div class="pull-left">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/hcvg_featured_t-75x64.jpg" alt="" class="content-widget__img">
                </div>
                <div class="media-body">
                  <p class="content-widget--development-content">
                    <span class="upper-blue">HCV GENOTYPING PANEL (HCVG)</span><br>
                    Typing and subtyping of HCV 1a, 1b, 2a/c, 2b, 3, 4, 5, and 6 from plasma or serum.
                  </p>
                </div>
              </div>
            */ ?>
            </div>
          </div>
          <?php
          }
        }
      ?>

      <?php the_field('additional_content'); ?>

      <div class="media media--stackable media--panel-cta">
        <div class="media-body">
          <p>
            To learn more about the <?php the_title(); ?> or to schedule a demonstration, contact us at <strong><?php _e('1.800.373.6767','genmark'); ?></strong> or email us.
          </p>
        </div>
        <div class="media-right media-middle">
          <a href="mailto:info@genmarkdx.com" class="btn btn-default"><?php echo strtoupper( __('Schedule a demonstration via Email','genmark')); ?></a>
        </div>
      </div>

      <?php echo get_template_part('content','footnote'); ?>

   </div><!-- .col-xs-12 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>
