<?php /* Template Name: Landing Page BCID Testing */ ?>
<?php
$hero_img = get_field('hero_image');
$hero_header = get_field('hero_header');
$hero_text = get_field('hero_text');
$article_header = get_field('article_header');
$article_text = get_field('article_text');
?>

<?php get_header(); ?>

<section class="landing-pg landing-pg__hero">
  <div class="landing-pg__hero-blue-top"></div>
  <div class="container">
    <div class="row">
      <div class="col-sx-12 landing-pg__logo">
        <a href="<?php echo get_home_url(); ?>" class="landing-pg__hero-logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/genmark-logo.png" alt=""></a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <img class="landing-pg__hero-img" src="<?php echo $hero_img; ?>" alt="">
        <div class="landing-pg__hero-img--fade"></div>
      </div>
      <div class="col-md-7 landing-pg__hero-copy">
        <h1 class="landing-pg__hero-header"><?php echo $hero_header; ?></h1>
        <span class="landing-pg__hero-text"><?php echo $hero_text; ?></span>
      </div>
    </div>
  </div>
</section>

<section class="landing-pg landing-pg__article">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <h2 class="landing-pg__article-header"><?php echo $article_header; ?></h2>
        <span class="landing-pg__article-text"><?php echo $article_text; ?></span>
      </div>
      <div class="col-sm-5 col-sm-offset-1 landing-pg__form">
        <?php echo do_shortcode('[contact-form-7 id="5656" title="Landing Page BCID Testing"]'); ?>
      </div>
    </div>
  </div>
</section>

<section class="landing-pg">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <a href="<?php echo get_home_url(); ?>" class=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/genmark-logo.png" alt=""></a>
        <span class="landing-pg__copyright">Terms &copy; Copyright <?php echo date('Y'); ?> GenMark Diagnostics, Inc. All rights reserved.</span>
      </div>
      <div class="col-xs-12">
        <a href="http://creativepace.com/" class="created-by__btn">
          <img class="created-by__btn__icon--hover" src="https://genmark-new.test/wp-content/themes/genmark/img/footer-site-by-pace-creative_hover.png" alt=""><img class="created-by__btn__icon" src="https://genmark-new.test/wp-content/themes/genmark/img/footer-site-by-pace-creative.png" alt=""><br>
        </a>
      </div>
    </div>
  </div>
</section>


<?php get_footer(); ?>
