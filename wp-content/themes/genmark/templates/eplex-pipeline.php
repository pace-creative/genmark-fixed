<?php /* Template Name: ePlex Pipeline */ ?>
<?php get_header(); ?>

<div class="container">

  <div class="row">

    <?php get_sidebar(); ?>


    <div class="col-xs-12 col-md-9">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <?php the_content(); ?>

      <?php

        // get page children
        $args = array(
          'post_parent' => get_the_ID(),
          'post_type' => 'page',
          'numberposts' => -1,
          'post_status' => array('publish','private'),
          'orderby' => 'menu_order',
          'order' => 'ASC',
        );

        $the_query = new WP_Query($args);

        if( $the_query->have_posts() ) {
          ?>
          <div class="row">
          <?php

          while( $the_query->have_posts() ) {
            $the_query->the_post();

            if(get_field('hide_from_pipeline_page')) {
              continue;
            }

            $status = get_post_status();

            ?>

            <div class="col-xs-12 col-sm-6">
              <div class="content-widget content-widget--pipeline <?php echo $status == 'publish' ? 'fakelink' : ''; ?>" <?php if( $status == 'publish' ) { ?>data-href="<?php the_permalink(); ?>"<?php } ?>>
                <?php
                  if( has_post_thumbnail() ) {
                    $thumb_url = wp_get_attachment_url( get_post_thumbnail_id() );
                  }

                  $title = str_replace( 'Private: ', '', get_the_title() );
                  $title = genmark_upper( $title );

                  $intro = get_field('exceprt');
                  $text = get_the_content();

                  if( !empty( $intro )) {
                    $text = $intro;
                  }

                  $text = strip_tags( $text );

                ?>
                <img class="content-widget__img" src="<?php echo $thumb_url; ?>" alt="">
                <p class="content-widget__text">
                  <strong class="upper-blue"><?php echo $title; ?></strong><br>
                  <?php echo $text; ?>
                  <?php
                    if( $status == 'publish' ): ?>
                      <br>
                      <a href="<?php the_permalink(); ?>"><?php _e('View Panel','genmark'); ?></a>
                    <?php
                    endif;
                  ?>
                </p>
              </div>
            </div>

            <?php
          }
          ?>
          </div>
        <?php
        }

        wp_reset_postdata();

      ?>

      <?php the_field('additional_content'); ?>

      <?php echo get_template_part('content','footnote'); ?>

  </div><!-- .col-xs-12 -->
</div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>
