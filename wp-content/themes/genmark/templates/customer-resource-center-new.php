<?php /* Template Name: New Customer Resource Center */ ?>
<?php
if( !is_user_logged_in() ){
  $login_ID = 971;
  wp_redirect( get_permalink(apply_filters('wpml_object_id', $login_ID, 'page')) ); exit;
  //wp_redirect( get_home_url() . "/login/" ); exit;
}

global $search_prod, $crc_lang, $search, $meta_query;

$search = isset( $_GET['search'] ) ? $_GET['search'] : false;
$search_cat = isset( $_GET['select_resource'] ) ? $_GET['select_resource'] : 'all';
$search_prod = isset( $_GET['select_product'] ) ? $_GET['select_product'] : 'all';
$crc_lang = isset( $_GET['crc_language'] ) ? $_GET['crc_language'] : 'all';

// get real title for previews
$real_title = get_the_title();
$real_id = get_the_ID();
$real_region = get_field( 'region' );

?>
<?php get_header();  ?>

<div class="crc container">
  <div class="row">


    <?php
    if( is_user_logged_in() ):
      $current_user = wp_get_current_user();
      /*
      echo '<pre>';
      print_r($current_user);
      echo '</pre>';
      */
    ?>


      <?php if( in_array( 'customer-pending', $current_user->roles )): ?>
        <div class="col-xs-12">
          <h1>Customer Resource Center</h1>
          <h2>Your access will need to be verified by one of our Customer Service Team members before access can be granted.</h2>
        </div>
      <?php endif; //end if role == customer-pending ?>




      <?php if( in_array( 'customer', $current_user->roles)
            || in_array( 'distributor', $current_user->roles )
            || in_array( 'administrator', $current_user->roles )
            || in_array( 'editor', $current_user->roles )): ?>
        <?php $user_region = get_field('region', 'user_'.$current_user->ID); ?>
        <?php

          $meta_query = array();

          if( !is_preview() && $user_region !== 'all' ) {
            $meta_query = array(
              'relation'=>'or',
              array(
                'key' => 'region',
                'value' => 'ALL',
                'compare' => 'IS',
              ),
              array(
                'key' => 'region',
                'value' => $user_region,
                'compare' => 'IS',
              ),
            );
          }

          $args = array(
            'taxonomy' => 'customer_resource_category',
            'orderby' => 'name',
            'order'   => 'ASC',
            'hide_empty' => 1,
            'parent' => 0,
          );

          $cr_categories_all = get_terms($args);
          $cr_categories = [];

          $products_all = get_terms('customer_resource_product', $args);
          $products = $products_all; //[];

          $langs_all = get_terms('customer_resource_language', $args);
          $langs = $langs_all; //[];

          foreach ($cr_categories_all as $cr_cat) {

            if( $search_cat !== 'all' && $search_cat !== $cr_cat->slug ) {
              continue;
            }

            $tax_query = array(
              'relation' => 'AND',
              array(
                'taxonomy' => 'customer_resource_category',
                'field' => 'slug',
                'terms' => $cr_cat->slug,
              ),
            );

            if( $search_prod !== 'all' ) {
              $tax_query[] = array(
                'taxonomy' => 'customer_resource_product',
                'field' => 'slug',
                'terms' => $search_prod,
              );
            }

            if( $crc_lang !== 'all' ) {
              $tax_query[] = array(
                'taxonomy' => 'customer_resource_language',
                'field' => 'slug',
                'terms' => $crc_lang,
              );
            }

            $args = array(
              'post_type' => 'customer_resources',
              'orderby' => 'title',
              'order' => 'ASC',
              'posts_per_page' => -1,
              's' => $search,
              'tax_query' => $tax_query,
              'meta_query' => $meta_query,
            );

            // pvar_dump($args);

            $query = new WP_Query($args);

              // pvar_dump($query);
            if( !empty( $args['s'] )) {
              relevanssi_do_query($query);
            }

            if ( $query->have_posts() ) {


              $cat = $cr_cat;
              $cr_categories[] = $cat;

            } else {
              // no posts found
            }

            wp_reset_query();
          }

          // foreach ($products_all as $cr_prod) {
          //    $args = array(
          //     'post_type' => 'customer_resources',
          //     'orderby' => 'title',
          //     'order' => 'ASC',
          //     'posts_per_page' => -1,
          //     'tax_query' => array(
          //       array(
          //         'taxonomy' => 'customer_resource_product',
          //         'field' => 'slug',
          //         'terms' => $cr_prod->slug,
          //       ),
          //     ),
          //     'meta_query' => $meta_query,
          //   );

          //   $query = new WP_Query($args);

          //   if ( $query->have_posts() ) {

          //     $prod = $cr_prod;
          //     $products[] = $prod;

          //   } else {
          //     // no posts found
          //   }

          //   wp_reset_query();
          // }

          /*
          echo '<pre>';
          print_r($cr_categories);
          echo '</pre>';
          */
          //$cr_categories = $cr_categories_all;
        ?>

        <?php /* get_sidebar(); */ ?>

        <!-- Check who is signed in and assign role which will be used to
              grant access to see certain categories(documents) -->
        <?php $user = wp_get_current_user();?>
        <?php  if ( in_array( 'distributor', (array) $user->roles ) ) { ?>
          <?php $user_role = 'distributor' ?>
        <?php } else { ?>
          <?php $user_role = 'general' ?>
        <?php } ?>

        <div class="col-xs-12">

        <?php if( !is_preview() ): ?>
          <?php if( get_field( "user-show-welcome-message", 'user_'.$current_user->ID) ): ?>
            <?php if ( $user_role == 'distributor' ) : ?>
              <!-- <h1>Welcome!</h1>
              <p>On behalf of everyone at GenMark, we want to say how delighted we are to welcome you as a customer. As a GenMark customer, you will have access to comprehensive resources such as dedicated customer service representatives, expert technical support scientists, and an online Customer Resource Center that contains assay worksheets, package inserts, workflow diagrams, and the latest protocol updates.</p>
              <p><input type="checkbox" id="showWelcomeWidget" name="show-welcome-widget" ><b> CHECK THIS BOX TO PERMANENTLY DISMISS THIS MESSAGE</b></p>
            </div> -->
            <?php else : ?>
              <div class="welcome-widget col-xs-12">
                <h1>Welcome!</h1>
                <p>On behalf of everyone at GenMark, we want to say how delighted we are to welcome you as a customer. As a GenMark customer, you will have access to comprehensive resources such as dedicated customer service representatives, expert technical support scientists, and an online Customer Resource Center that contains assay worksheets, package inserts, workflow diagrams, and the latest protocol updates.</p>
                <p><input type="checkbox" id="showWelcomeWidget" name="show-welcome-widget" ><b> CHECK THIS BOX TO PERMANENTLY DISMISS THIS MESSAGE</b></p>
              </div>
            <?php endif; ?>
          <?php else: ?>
            <h1>Customer Resource Center</h1>
          <?php endif; ?>

          <div class="row crc-filter">

            <div class="col-xs-12 col-md-3 resources-nav">
              <?php if( !empty( $cr_categories ) ): ?>

            <h2>Resources</h2>

              <div class="content-panel row">
                <?php $i = 0; ?>
                <?php foreach($cr_categories as $key => $cr_cat): ?>

                  <?php
                    // Check if Role of Logged in User matches the ACF Checkbox Field
                    // in each Category, if so, show them
                    $taxonomy = 'customer_resource_category';
                    $term_id = $cr_cat->term_id;
                    $user_type = get_field_object('user_type', $taxonomy . '_' . $term_id);
                    $user_type_values = $user_type['value'];
                  ?>

                  <?php if( $user_type_values ): ?>
                    <?php foreach( $user_type_values as $user_type_value ): ?>
                      <?php if($user_type_value['value'] == $user_role ): ?>
                        <?php

                          $tax_query = array(
                            array(
                              'taxonomy' => 'customer_resource_category',
                              'field' => 'slug',
                              'terms' => $cr_cat->slug,
                            ),
                          );

                          if( $search_prod !== 'all' ) {
                            $tax_query[] = array(
                              'taxonomy' => 'customer_resource_product',
                              'field' => 'slug',
                              'terms' => $search_prod,
                            );
                          }

                          if( $crc_lang !== 'all' ) {
                            $tax_query[] = array(
                              'taxonomy' => 'customer_resource_language',
                              'field' => 'slug',
                              'terms' => $crc_lang,
                            );
                          }

                          $args = array(
                            'post_type' => 'customer_resources',
                            'orderby' => 'title',
                            'order' => 'ASC',
                            'posts_per_page' => -1,
                            's' => $search,
                            'tax_query' => $tax_query,
                            'meta_query' => $meta_query,
                          );
                          $query = new WP_Query($args);

                          if( !empty( $args['s'] )) {
                            relevanssi_do_query($query);
                          }

                          if( $query->found_posts == 0 ) {
                            continue;
                          }

                        ?>
                        <?php $i++ ?>
                      <div class="col-xs-12 content-panel__row">
                        <a class="crc-cat" data-location="Main" data-name="<?php echo $cr_cat->name; ?>" data-href="#<?php echo $cr_cat->slug; ?>">
                          <div class="content-panel-item">
                            <div class="content-panel-item__content">
                              <h3 id="<?php echo $i ?>"><?php echo $cr_cat->name; ?></h3>
                            </div>

                          </div>
                        </a>
                      </div>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  <?php endif; ?>


                <?php endforeach; ?>
              </div>
          <?php endif; ?>
            </div>

          <?php endif; ?>
            <div class="col-xs-12 col-md-9">
              <div class='col-xs-12 heading__container'>
                <h2><i class="fa fa-search" aria-hidden="true"></i> Search</h2>
              </div>
              <div class='col-xs-12'>
                <div class='row search-resources__bottom-container'>
                  <form class="form" action="" method="get">

                    <div class='col-xs-12 col-md-4'>
                      <div class="form-group">
                        <label for="search_resources">Search Resources:</label><br>
                        <input name="search" id="search_resources" class="form-control" placeholder="Eg. Assay Manuals" value="<?php echo $search ? $search : ''; ?>" size="20" type="text">
                      </div>
                    </div>
                    <div class='col-xs-12 col-md-2'>
                      <div class="form-group">
                        <label for="select_product">Product:</label><br>
                        <select class="form-control" id="select_product" name="select_product">
                          <option value="all">All</option>
                          <?php foreach( $products as $cr_prod): ?>
                            <option value="<?php echo $cr_prod->slug; ?>"<?php echo $cr_prod->slug == $search_prod ? ' selected="selected"' : ''; ?>><?php echo $cr_prod->name; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class='col-xs-12 col-md-2'>
                       <div class="form-group">
                        <label for="select_resource">Resource Type:</label><br>
                        <select class="form-control" id="select_resource" name="select_resource">
                          <option value="all">All</option>
                          <?php foreach( $cr_categories_all as $cr_cat): ?>
                            <?php
                              // Check if Role of Logged in User matches the ACF Checkbox Field
                              // in each Category, if so, show them
                              $taxonomy = 'customer_resource_category';
                              $term_id = $cr_cat->term_id;
                              $user_type = get_field_object('user_type', $taxonomy . '_' . $term_id);
                              $user_type_values = $user_type['value'];
                            ?>
                            <?php if( $user_type_values ): ?>
                            <?php foreach( $user_type_values as $user_type_value ): ?>
                              <?php if($user_type_value['value'] == $user_role ): ?>
                                <option value="<?php echo $cr_cat->slug; ?>"<?php echo $cr_cat->slug == $search_cat ? ' selected="selected"' : ''; ?>><?php echo $cr_cat->name; ?></option>
                              <?php endif; ?>
                            <?php endforeach; ?>
                          <?php endif; ?>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class='col-xs-12 col-md-2'>
                      <div class="form-group">
                        <label for="crc_language">Language:</label><br>
                        <select class="form-control" id="crc_language" name="crc_language">
                          <option value="all">All</option>
                          <?php foreach( $langs_all as $cr_lang): ?>
                            <option value="<?php echo $cr_lang->slug; ?>"<?php echo $cr_lang->slug == $crc_lang ? ' selected="selected"' : ''; ?>><?php echo $cr_lang->name; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class='col-xs-12 col-md-2'>
                      <input class="btn btn-default btn-search" value="<?php echo strtoupper( __('Search','genmark')); ?>" type="submit">
                    </div>
                </form>
               </div>
              </div>
              <div class='resources-result'>
                <!-- <?php if( empty( $cr_categories )): ?>
            <hr />
            <p>Sorry, no results found for this query.</p>
          <?php endif; ?>
          <hr> -->

          <?php
            $found_results = 0;
            $row_index = 0;
          ?>
          <?php foreach($cr_categories as $key => $cr_cat): ?>
            <?php
              // Check if Role of Logged in User matches the ACF Checkbox Field
              // in each Category, if so, show them
              $taxonomy = 'customer_resource_category';
              $term_id = $cr_cat->term_id;
              $user_type = get_field_object('user_type', $taxonomy . '_' . $term_id);
              $user_type_values = $user_type['value'];
            ?>
            <?php if( $user_type_values ): ?>
            <?php foreach( $user_type_values as $user_type_value ): ?>
              <?php if($user_type_value['value'] == $user_role ): ?>
                <?php
                  if( $search_cat !== 'all' && $cr_cat->slug !== $search_cat ) {
                    continue;
                  }

                  $count = $cr_cat->count;

                  $termID = $cr_cat->term_id; // Parent A ID
                  $args = array(
                    'taxonomy' => 'customer_resource_category',
                    'child_of' => $termID,
                  );

                  $termchildren = get_terms( $args );

                  foreach( $termchildren as $child_cat ) {
                    $count += $child_cat->count;
                  }
                ?>

                <section class="crc-category row" id="<?php echo $cr_cat->slug; ?>" >
                  <div class='col-xs-12'>
                    <p class='resources-count'>
                      <?php
                        echo $count;
                        if( $count > 1 ){
                          echo ' Results';
                        }else{
                          echo ' Result';
                        }
                      ?>
                    </p>
                  </div>
                  <div class="col-xs-12">
                      <?php category_html( $cr_cat); ?>
                  </div>
                  </section>
                <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>





          <?php endforeach; ?>

          <?php  if( $count < 1 ): ?>
            <div class='resources-no-result' style="padding-top: 200px;">
            <hr>
            <p>Sorry, no results found for this query.</p>
            <hr>
          <?php endif; ?>
              </div>
            </div>





        </div>
      <?php endif; //end if role == customer / admin ?>


    <?php endif; //end if is_user_logged_in ?>




    <?php if( !is_user_logged_in() ): ?>
    <div class="col-xs-12">
      <h2>You have to log in to view this page.</h2>
      <p><a href="<?php echo get_home_url(); ?>/create-account/">create account</a> | <a href="<?php echo get_home_url(); ?>/login/">login</a></p>
    </div>
    <?php endif; ?>


  </div>
</div>

<script>
// TODO: move the script to js file
var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";

$( "#showWelcomeWidget" ).click(function() {
  var bool_val = $( "#showWelcomeWidget" ).is(':checked') ? false : true;
  var user_id = 'user_'+ <?php echo $current_user->ID; ?>;
  updateWelcomeWidgetSetting( bool_val , user_id );
});

function updateWelcomeWidgetSetting(bool_val, user_id){

  $.ajax({
    url: ajaxurl,
    data: {
      'action':'update_crc_welcome_widget_setting',
      'bool_val' : bool_val,
      'user_id': user_id
    },
    success:function(data) {  console.log(data);  },
    error: function(errorThrown){  console.log(errorThrown); }
  });

  $( ".welcome-widget" ).after('<h1>Customer Resource Center</h1>');
  $( ".welcome-widget" ).slideUp("normal", function() { $(this).remove(); } );

}
</script>


<?php get_footer(); ?>
