<?php

$name = get_the_title();
$text = get_the_content();
$desig = get_field('designation');
$title = get_field('title');
$thumb = has_post_thumbnail() ? get_the_post_thumbnail(NULL,'portrait-small') : false;

global $wp_query;

?>
<div class="media media--stackable media--team-member">
  <?php if( $thumb && $wp_query->queried_object->post_name != 'board-of-directors' ): ?>
  <div class="media-left">
    <?php echo $thumb; ?>
  </div>
  <?php endif; ?>
  <div class="media-body media-body-team-member">
    <div class="media-heading team-member__heading">
      <h4 class="media-heading upper-blue team-member__name">
        <?php echo $name; ?><?php if( !empty( $desig )): ?>, <?php echo $desig; ?>
        <?php endif; ?>
      </h4>
      <h4 class="media-subheading team-member__title"><i><?php echo $title; ?></i></h4>
      <p class="team-member__text">
        <?php echo $text; ?>
      </p>
    </div>
  </div>
</div>
