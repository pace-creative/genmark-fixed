<?php /* Template Name: Front Page */ ?>
<?php get_header(); ?>

<div class="container">
  <div class="row">
    <?php
      $eplex_page_id = apply_filters( 'wpml_object_id', 5, 'page' );
      $xt8_page_id = apply_filters( 'wpml_object_id', 12, 'page' );

      $eplex_title = get_field('eplex_title');
      $eplex_blurb = get_field('eplex_blurb');

      // $xt_8_title = get_field('xt-8_title');
      // $xt_8_blurb = get_field('xt-8_blurb');

      $featured_panel = get_field('featured_panel');
      // var_dump($featured_panel);

    ?>

    <div class="col-xs-12 col-md-6 home-cta">

      <h2 class="home-cta__title"><?php echo $eplex_title; ?></h2>

      <div class="media">
        <div class="pull-left img-responsive<?php /*media-left*/ ?>"><img class="home-cta__img" src="<?php echo get_stylesheet_directory_uri(); ?>/img/eplex-home.jpg" alt=""></div>
        <div class="media-body">
          <p><?php echo $eplex_blurb; ?></p>
          <?php /* <a href="<?php echo get_the_permalink( $eplex_page_id ); ?>" class="btn btn-default"><?php echo genmark_upper( __('View ePlex System','genmark')); ?></a> */ ?>
          <a href="<?php echo get_the_permalink( $eplex_page_id ); ?>">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/view-eplex.png" alt="View ePlex System" />
          </a>
        </div>
      </div>

    </div>

    <div class="col-xs-12 col-md-6 home-cta">

      <h2 class="home-cta__title"><?php _e('Featured Panel','genmark'); ?></h2>

      <div class="media">
        <div class="pull-left img-responsive<?php /*media-left*/ ?>">

        <?php

          if( has_post_thumbnail( $featured_panel ) ) {
            echo get_the_post_thumbnail( $featured_panel, null, array('class' => 'home-cta__img') );
          }

        ?>
        <!-- <img class="home-cta__img" src="<?php echo get_stylesheet_directory_uri(); ?>/img/xt8-home.jpg" alt=""> -->

        </div>
        <?php $alt_intro_txt = get_field('alternative_intro_text', $featured_panel); ?>
        <div class="media-body">
          <?php if($alt_intro_txt) : ?>
          <p><?php echo $alt_intro_txt; ?></p>
          <?php else : ?>
          <p><?php echo get_field('intro_text', $featured_panel); ?></p>
          <?php endif; ?>
          <?php /* <a href="<?php echo get_the_permalink( $featured_panel ); ?>" class="btn btn-default"><?php echo genmark_upper( __('View Panel','genmark')); ?></a> */ ?>
          <a href="<?php echo get_the_permalink( $featured_panel ); ?>">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/view-panel.png" alt="View Panel" />
          </a>
        </div>
      </div>

    </div>

  </div>
</div>

<div class="container-fluid" style="background: #f8f8f9;">
  <div class="container">
    <div class="row">

      <?php
        $careers_page_id = apply_filters( 'wpml_object_id', 68, 'page' );
        $news_page_id = apply_filters( 'wpml_object_id', 241, 'page' );
        $technology_page_id = apply_filters( 'wpml_object_id', 42, 'page' );

        $box_1_title = get_field('box_1_title');
        $box_1_blurb = get_field('box_1_blurb');
        $box_1_button_text = get_field('box_1_button_text');
        $box_1_button_url = get_field('box_1_button_url');
        $box_1_alt_style = get_field('box_1_alt_style');

        $box_2_title = get_field('box_2_title');
        $box_2_blurb = get_field('box_2_blurb');
        $box_2_button_text = get_field('box_2_button_text');
        $box_2_button_url = get_field('box_2_button_url');
        $box_2_alt_style = get_field('box_2_alt_style');

        $box_3_title = get_field('box_3_title');
        $box_3_blurb = get_field('box_3_blurb');
        $box_3_button_text = get_field('box_3_button_text');
        $box_3_button_url = get_field('box_3_button_url');
        $box_3_alt_style = get_field('box_3_alt_style');
      ?>

      <div class="col-xs-12 col-md-4 home-cta home-cta--small <?php echo !empty($box_1_alt_style) && $box_1_alt_style === '1' ? 'home-cta--alt' : ''; ?>">

        <h2 class="home-cta__title"><?php echo $box_1_title; ?></h2>
        <div class="normalize-height"><?php echo $box_1_blurb; ?></div><!-- /.normalize-height -->
        <a href="<?php echo $box_1_button_url; ?>" class="btn btn-default"><?php echo $box_1_button_text; ?></a>

      </div>

      <div class="col-xs-12 col-md-4 home-cta home-cta--small <?php echo !empty($box_2_alt_style) && $box_2_alt_style === '1' ? 'home-cta--alt' : ''; ?>">

        <h2 class="home-cta__title"><?php echo $box_2_title; ?></h2>
        <div class="normalize-height"><?php echo $box_2_blurb; ?></div><!-- /.normalize-height -->
        <a href="<?php echo $box_2_button_url; ?>" class="btn btn-default"><?php echo $box_2_button_text; ?></a>

      </div>

      <div class="col-xs-12 col-md-4 home-cta home-cta--small <?php echo !empty($box_3_alt_style) && $box_3_alt_style === '1' ? 'home-cta--alt' : ''; ?>">

        <h2 class="home-cta__title"><?php echo $box_3_title; ?></h2>
        <div class="normalize-height"><?php echo $box_3_blurb; ?></div><!-- /.normalize-height -->
        <a href="<?php echo $box_3_button_url; ?>" class="btn btn-default"><?php echo $box_3_button_text; ?></a>

      </div>

    </div>
  </div>
</div>

<?php get_footer(); ?>
